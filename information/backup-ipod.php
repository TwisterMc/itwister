<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Apple iPod Backup - Hard Drive Backup - iPod Hard Drive Backup</title>
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>iPod  Hard Drive Backup: An Overview</h3>
      <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3532224706";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<p>Apple created the amazing iPod and it seems like every other person has one.&nbsp; However, is your iPod backed up? What if your poor iPod crashed to the ground or had a system glitch?&nbsp; Then what?&nbsp; You've got gigs worth of music and if the unthinkable would happen, you'd have no music.&nbsp; Thankfully there is ways to <b>back up your iPod</b>!</p>
<p><b>Getting music off the iPod:</b> </p>
<p>First thing you'll probably need to do is figure out how to get music off the iPod. Apple didn't create the iPod with the intension of taking it back off.&nbsp; To get rid of a song you'd delete it.&nbsp; So you need an unofficial iPod extractor application.&nbsp; Checkout <a href="http://www.versiontracker.com" target="_blank">VersionTracker</a> or <a href="http://www.macupdate.com" target="_blank">MacUpdate</a> for a little application that specializes in taking the music off your iPod and putting it on your hard drive.&nbsp; </p>
<p><b>Backup your iPod to CDs:</b></p>
<p>Simple enough, burn your favorite songs onto CDs.&nbsp; Then you'll always have your songs on a CD somewhere than you could always re-import.</p>
<p><b>Backup your iPod to Mp3 CDs:</b></p>
<p>Mp3 CDs are great because they'll hold hundreds of mp3 files.&nbsp; Thus is a great backup feature for most since lots of people have CD burners in their computers and it's quite easy. Just use iTunes to burn your own mp3 cds. </p>
<p><b>Backup your iPod to DVD: </b></p>
<p>You think a regular mp3 CD can hold a lot, checkout how many songs you can fit on a DVD!&nbsp; This is a great way to back up your music and keep most, if not all, of it in one place.&nbsp; iTunes actually has a 'backup to dvd' option built in if I remember right.&nbsp; So give that a shot and see how good it does, or feel free to use your own CD/DVD burning software.</p>
<p><b>Backup your iPod to a Hard Drive:</b></p>
<p>Just extract the songs (as stated above) and save the files on your hard drive or a different hard drive.&nbsp; There should be plenty of space of you go out to the computer store and get an external hard drive.&nbsp; You can get probably 50 gigs for less than $200 if you look around. </p>
<p><b>Backup your files:</b></p>
<p>Ok so you got your music backed up, don't forget the files if you use your iPod as a hard drive!</p>
<script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3532224706";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
      <p>
      <?php include("../menu.php"); ?></p>
    </td>
  </tr>
</table>
</body>
</html>
