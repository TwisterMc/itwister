<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>How to back up your harddrive - Hard Drive Backup - File Back Up</title>
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Hard Drive Backup - File Backup: An Overview</h3>
      <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="7789747172";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<p>Of all the things you should know about computers, how to perform regular backups is one of the most important. If, for any reason your computer freaks out, you could loose everything.&nbsp; From emails to address and digital pictures.&nbsp; That's why it's important to always backup the important stuff. From a CD full of pictures to a complete <b>hard drive backup</b>.&nbsp; You never know when the worst could happen. </p>
<p><b>What does "performing a backup" mean? </b></p>
<p>Performing a backup means making a spare copy of some or all of the data on your hard drive. The spare copy is not stored on your hard drive. It is stored on other things that are designed primarily to hold backup copies. </p>
<p><b>How important is performing a backup? </b></p>
<p> I can't think of anything more important than backing up your most important data. If you have valuable and important information on your computer, but don't have copies of that data, then you run a very high risk of losing that data forever. </p>
<p><b>What does "restoring a backup" mean? </b></p>
<p> Restoring a backup means taking the spare copy and putting all (or some) of it back on your hard drive. Restoring a backup is usually done when the original data is lost, damaged, or accidentally altered. Quite often, restoring a backup is done after a hard drive crash. </p>
<p><b>What exactly does the term "crash" mean? </b></p>
<p> Many things can happen to your computer that result in the loss or damage of data. For example, power surges or lightning strikes can seriously affect your computer.
<p> Or, your computer's hard drive can suffer a mechanical failure that results in the loss or damage of data. </p>
<p> If you experience problems serious enough that you lose valuable data, or can't get your computer to run properly, this is called a crash. Quite often, the terms crash and hard drive crash are interchangeable. </p>
<p> There are ways to prevent serious data loss through crashes. Using an uninterrupted power supply is a superb way of preventing hard drive crashes. </p>
<p> If you don't perform regular backups of your most valuable data, and you suffer a crash, you may have lost that data forever. </p>
<p><b>What is a Full System Backup?</b> </p>
<p> A Full System Backup is a complete, total backup of everything that is currently on your computer. This includes Windows, all of your computer programs, the system registry, and all of the files you've created yourself. In addition, any customized settings and configurations you created will be preserved. </p>
<p> Several programs will create full system backups for you.
<p><b>Do I have to backup everything on my computer?</b>
<p> No. It isn't absolutely necessary to backup everything on your computer, but it's more convenient. You should always backup everything you yourself created, and you should update your backups every time you modify something.
<p> For example, if you have a spreadsheet file with your checkbook information in it, you should back it up as soon as you add or change information in it.
<p> However, as I stated above, since a full system backup will backup your configuration and custom settings, it's probably easier just to backup everything. You can always make separate backups of the files you created yourself.
<p> If you don't backup everything on your computer, then you'll have to reinstall all of your software, from their original master disks.
<p><b>What does the terms "original installation set" and "master disks" mean?</b>
<p> When you buy a computer program, it usually comes on a CD-ROM. In order to install the program, you use the  CD-ROM. These are called master disks. They are the original disks you use to install software.&nbsp; There is no need to backup this stuff, except for the preferences, as the main program is still safely on the disk. <p> Also, when you download a program from the Internet, it probably comes in one file. Perhaps a few files. You would use the file(s) to install the program you downloaded. They are the original installation files, or original installation set.
<p> The terms original installation set and master disks basically mean the same thing. They are the disks you use to install a computer program on your computer for the first time. </p>
<p> If you restore a program you've backed up, you are not installing it from scratch using the master disks/original master set. Restoring a program from a backup is not considered to be installing it from scratch. </p>
<p> Many computer experts recommend installing a program from scratch, using the master disks, instead of restoring it from a backup, in the event your computer has crashed. In my opinion, I believe it depends entirely on the program. </p>
<p><b>What about software that comes pre-installed on a computer? How do I backup them?</b> </p>
<p> When you buy a new computer, it normally comes with Windows or Mac operating software, as well as various other programs.&nbsp; These files shouldn't need to be backed up because you can always re-install off the actually CDs that your computer came with. </p>
<p> Sometimes, the computer company gives you a CD or that are called Restore disks. They are supposed to put back everything on your computer in case of a problem. </p>
<p> The best thing to do, when buying a new computer, is find out what programs are pre-installed, and whether or not you get master disks, or specially-made Restore disks.  I would strongly recommend you use a backup software utility of your own and make a full system backup, just in case, unless the computer comes with master disks. </p>
<p><b>I want to backup certain things, but I don't know what files I should backup. How do I find out what files I need to backup?</b> </p>
<p> This depends entirely on what, exactly, you want to backup. For example, if you want to save your e-mail program's address list, it most likely has a utility that lets you do that. Outlook Express, for example, lets you export your address book into a plain file that you can back it up. </p>
<p> If you have folders filled with e-mail and want to save them all, that can be a complicated procedure. There are programs that will do this for you. The Backup Software page will list some of them. </p>
<p> If you can't figure out how to save your the settings or configurations of certain things, then my advice is to check a Usenet newsgroup dedicated to that program, and see if someone can help. </p>
<p> Doing a full system backup, of course, solves these problems. </p>
<p>On a Mac it's always a good idea to take your entire 'home' folder as it contains most of your mail, preferences, and other important documents. But don't forget about your files and folders outside the 'home' folder.</p>
<p><b>How do I perform a backup?&nbsp; To what? </b></p>
<p>In order to perform a backup of your data, you need a device designed for data storage, media to actually hold the backup data, and backup software. </p>
<p> The best media for backing up your computer would be CDs, DVDs and external hard drives.&nbsp;&nbsp; Just burn or copy your files onto one of these devices to back it up. </p>
<p>Most CDs can hold about  650 MB of data. Some CD-R discs have slightly higher capacities. </p>
<p>Different CD burners write at different speeds.&nbsp; They will look something like this: 2X / 4X / 32X. All numbers refer to how fast the drive compared to the speed at which a CD player reads a music CD (which is considered 1 times or "1X"). </p>
<p>The first number in the sequence above is how fast the drive "writes" to a blank from a source CD. The second X number is how fast the drive can copy data from the hard drive to a CD. The last number is the "read" speed. That's is how fast the drive reads data from a CD. </p>
<p>Most CD burners come with software that will do data backups as well as audio CD creation. </p>
<p>A CD burner is not the most efficient way to back up a drive. If you had a 10 GB hard drive, you'd need 16 CDs to do a full backup. </p>
<p>There is another less hardware intensive backup solution. Skip backing up to CD and backup to the web. Just get an account with XDrive. They give you space and software and you click and all your data is stored on their secure server off on the web. The cool thing about this is that you can get it from anywhere you happen to be as long as you can connect to the web. They offer a free trial that includes 500 MB of storage.</p>
<p>The best idea would be to back up to DVD or to an external hard drive as these can hold as much, if not more, than your original hard drive. </p>

<script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="7789747172";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
      <p>
      <?php include("../menu.php"); ?></p>
    </td>
  </tr>
</table>
</body>
</html>
