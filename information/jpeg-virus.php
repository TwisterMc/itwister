<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Microsoft JPEG Virus : JPG Worm : JPEG of Death : Image Virus</title>
<meta name="description" content="Information on the new JPEG virus that affects Microsoft IE.">
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Microsoft JPEG Virus : JPG Worm : JPEG of Death</h3>
      <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3532224706";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
      <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
      <p>A Trojan horse that works through a flaw in Microsoft Windows and <b>JPEG</b> images could cause your computer to crash. <b>JPEG of Death</b> as come call it, uses a hidden image to contain the <b>virus</b> and when your browser loads the image, it loads the virus also. At this point, it's not technically a virus since it has no way to spread, but it'll only be a matter of time before figures out how to. This affects Windows computers running Windows XP, Windows Server 2003, Office XP, Office 2003, Internet Explorer 6 Service Pack 1, Project, Visio, Picture It and Digital Image Pro. The main issue is in Internet Explorer and how it handles JPEG files. Simply stopping the use of IE doesn't exactly help as other applications, such as Outlook, use Internet Explorer to render the JPEGS in HTML emails. However, it's good to note that using an alternative browser is a start to protecting yourself from this virus. Microsoft claims that this is not an issue yet and has no plans to protect it's users until it becomes a bigger issue.</p>
      <p><i>----- More information from a CNET article -----</i></p>
      <p><b>Security researchers say JPEG virus imminent</b><br>
        By <a href="mailto:rob.lemos@cnet.com"> Robert Lemos</a> CNET News.com September 28, 2004, 1:02 PM PT<br>
        URL: <a href="http://news.zdnet.com/2100-1009-5387380.html"> http://news.zdnet.com/2100-1009-5387380.html </a></p>
      <p>
        <!-- eds: original posted Tue Sep 28 13:02:00 PDT 2004 -->
        <b>A Trojan horse that exploits a recent critical flaw in Microsoft Windows' handling of JPEG images has been posted to several newsgroups, but it has no way to spread, security experts said Tuesday.</b> </p>
      <p> Though the code only threatens visitors to the newsgroups where the malicious programs--hidden in images--are posted, antivirus experts continue to warn that it's a short step from such code to an effective computer virus. </p>
      <p>"We are getting closer and closer to an exploit that could be turned into a worm," said Oliver Friedrichs, senior manager with security-software maker Symantec's incident response group. </p>
      <p>The posting of the code hidden in a JPEG graphic is the latest in a series of events that security experts have widely predicted: <a href="/2100-1009_22-5366314.html?tag=nl" title="Major graphics flaw threatens Windows PCs -- Tuesday, Sep 14, 2004">A serious flaw</a> in the widespread Microsoft Windows operating system and software was found; code that showed how to take advantage of the flaw <a href="/2100-1009_22-5378260.html?tag=nl" title="Code to exploit Windows graphics flaw now public -- Wednesday, Sep 22, 2004">has been published</a>; and a tool to automatically create malicious JPEG images is continually being refined, Friedrichs said. </p>
      <p> The latest code, found Tuesday by online newsgroup access provider Easynews, actually requires the victim to download the false image and view it in Windows Explorer in order for his or her system to be infected, Friedrichs said. That should severely limit the number of computers that are compromised by the program. </p>
      <p> Microsoft also pooh-poohed any danger represented by the program. </p>
      <p>"Microsoft does not consider this a high risk to customers given the amount of user action required to execute the attack and is not currently aware of any significant customer impact," the software giant said in a statement. "We will continue to investigate the situation and provide customers with additional resources and guidance as necessary." </p>
      <p>Easynews announced that a program that scans images posted to Internet newsgroups had registered several hits, finding false JPEG images embedded with malicious code. </p>
      <p> Mike Minor, <a href="http://dw.com.com/redir?destUrl=http%3A%2F%2Fwww.easynews.com%2F&amp;siteId=22&amp;oId=2102-1009-5387380&amp;ontId=1009&amp;lop=nl_ex">Easynews</a>' chief technology officer, said he had been monitoring the Usenet feed for 36 hours before discovering an infected image. "We couldn't find any other trace of any other posts from that IP address," Minor said. Easynews has not spotted any infected JPEGs since the two it identified late Sunday. </p>
      <p>The code, which Easynews called a virus, does not have any mechanism to spread, antivirus-software company F-Secure said in its Weblog. </p>
      <p>"These JPEGs did not replicate, so this is not a virus," the company said. "Apparently they tried to use these JPEGs to download Trojan (horse programs) to vulnerable computers, but the download sites should be down by now." </p>
      <p>The code posted to Easynews, which Symantec has dubbed Trojan.Moo, was apparently created with the automated tool released by several hackers. The tool, known as the JPEG of Death creation kit, is constantly being updated by its creators and will likely be able to generate viruses soon, said antivirus experts. </p>
      <p>"I think because the source code for the kit was released, we will see people that take that source code and create new versions," said Craig Schmugar, virus research manager for security software maker McAfee. </p>
      <p> Both McAfee and Symantec have generic detection in their antivirus software for images that contain malicious code. </p>
      <p>The JPEG flaw affects various versions of at least a dozen Microsoft software applications and operating systems, including Windows XP, Windows Server 2003, Office XP, Office 2003, Internet Explorer 6 Service Pack 1, Project, Visio, Picture It and Digital Image Pro. The software giant has a full list of affected applications in <a href="http://dw.com.com/redir?destUrl=http%3A%2F%2Fwww.microsoft.com%2Fsecurity%2Fbulletins%2F200409_jpeg.mspx&amp;siteId=22&amp;oId=2102-1009-5387380&amp;ontId=1009&amp;lop=nl_ex">the advisory on its Web site</a>. Windows XP Service Pack 2, which is <a href="http://dw.com.com/redir?destUrl=http%3A%2F%2Fnews.com.com%2FMicrosoft%2Bsends%2Bsecurity%2Bupdate%2Bto%2Bhome%2BPCs%2F2100-1002_3-5316838.html%3Ftag%3Dnl&amp;siteId=22&amp;oId=2102-1009-5387380&amp;ontId=1009&amp;lop=nl_ex">still being distributed to many customers' computers</a>, is not vulnerable to the flaw. </p>
      <p> <i>CNET News.com's Declan McCullagh contributed to this report.</i> </p>
      <p><i>----- end CNET article -----</i></p>
      <p> </p>
   
<img src="http://dw.com.com/clear/c.gif?ts=1096549383&amp;edId=2&amp;prtnr=CNET%20Networks,%20Inc.&amp;oid=2102-1009_22-5387380&amp;ptId=2102&amp;onId=1009&amp;sId=22&amp;asId=5387380&amp;astId=1&amp;xref=http://news.zdnet.com/2100-1009_22-5387380.html" alt="" border="0" height="1" width="1">
<script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3532224706";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<p>
  <?php include("../menu.php"); ?>
</p>
</td>
</tr>
</table>
</body>
</html>
