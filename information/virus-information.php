<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Virus Information - What are they? - How to Avoid and Protect Yourself From Viruses</title>
<meta name="keywords" content="virus, anti, antivirus, protect, outlook, microsoft, mac, macafee, nortn, bug, worm, pc, data, backup">
<meta name="description" content="To protect yourself from todays viruses you must first educate yourself on what a virus is and how to protect yourself.">
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Virus Information - What are they and how to protect yourself from viruses.</h3>
     <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="7483891760";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<p>SPECIAL: <a href="jpeg-virus.php">JPEG of DEATH</a> Virus Information</p>
        <p><b>What are computer viruses and how can I avoid them? </b></p>
      
      <p>Computer viruses, or more generally a class of software known as malicious code, are programs written with an intent to do harm or to play some sort of prank on an unsuspecting computer user. </p>
      <p>Viruses are designed to cause harm to your data or files, or simply to be a nuisance. Malicious code can perform a number of harmful acts, depending on the particular virus; at one extreme, some viruses just delete documents and files, while more pernicious viruses or Trojan horse applications can actually try to use your computer as a base of operations for an attack on another computer or Web site (a denial of service event). </p>
      <p>In discussing viruses, there are generally two factors that, taken together, determine how dangerous a virus is: </p>
      <ul>
        <li>Method of Transmission: Viruses vary in the methods by which they spread to other computers. The very first computer viruses spread only by floppy diskette. By the late 1990s, viruses evolved to transmit themselves via e-mail; when a system gets an e-mail-borne virus, the virus typically targets all the names found in the infected system's address book and Inbox. Newer viruses also try to attack shared files on networks and Web servers. The faster a virus can spread, the more dangerous it is. </li>
        <li>Payload: Besides the speed with which a virus spread, a virus's threat level is measured by the harm it can do. A virus's payload is the 'weapon' the virus carries and the method by which havoc is caused to an individual. Some viruses are designed largely as pranks, and may only display a boastful message. Other viruses try to delete files on your system, either attacking all your files or targeting documents like word processor or music files. Viruses with more potent payloads are considered more dangerous. </li>
      </ul>
      <p>Computer viruses are everywhere, and everyone who uses one -- particularly when connected to a high-speed Internet connection -- should protect themselves from the risk of data loss from viruses. </p>
      <p><b>To protect your computer against viruses: </b></p>
      <ul>
        <li>Run effective anti-virus software. Look for anti-virus software from major vendors like McAfee or Symantec, or packages that receive good reviews from computer magazines and Web sites. Harvard provides site-licensed copies of anti-virus software for Windows (McAfee VirusScan) and Macintosh (Virex) for download at <a href="http://www.fas.harvard.edu/computing/download">http://www.fas.harvard.edu/computing/download</a>. </li>
        <li>Keep your anti-virus software updated. Remember that new viruses come out all the time and anti-virus software can only protect you if the software has been updated to know about the newest threats. Anti-virus applications contain definitions (sometimes called DAT files or updates) of all known viruses; if a new virus is not in this library, your anti-virus software does no good. You should update your virus definitions at least once per week if not more often. Harvard's McAfee VirusScan updates itself against our server once per day. </li>
        <li>Avoid Microsoft Outlook/Outlook Express. Almost all modern viruses spread via Microsoft Outlook or Outlook Express. Because these e-mail clients are so closely linked to Windows and Internet Explorer, a virus targeting these programs can quickly and easily affect many areas of your system. Other e-mail programs are much safer; for instance, it is impossible to contract a virus from the Unix e-mail program Pine unless you explicitly download an attached file. FAS Webmail and Eudora are also much safer than Outlook. FAS Computer Services explicitly recommends users do not use Outlook or Outlook Express. </li>
        <li>Don't open attached files via e-mail. Whatever e-mail program you use, don't open any attached files, even from people you know, unless you were explicitly expecting that particular file. Many viruses use social engineering to try to convince you a file attachment containing the virus is something you really want to read. Be skeptical and careful! </li>
        <li>Use a secure Administrator password on Windows NT/2000/XP. Windows NT, 2000, and XP have a built-in Administrator account. This account is able to access all the files and settings on your computer. Many individuals choose no password, or a password like 'password,' for their Administrator password. This is highly insecure and can allow a malicious user to break into your computer over a network. Be sure to choose a password that is as tough to crack as your FAS account password is; however, you should never use your FAS account password anywhere else, including on your computer. </li>
        <li>Keep your computer up-to-date with the latest patches. New security holes in operating systems are found on a very regular basis. Use Windows Update or the Software Updater for Macintosh OS X to make sure your computer always has the latest fixes and patches for security holes. </li>
        <li>Use a firewall on the FAS Network or broadband connections. Viruses and other threats can come over networks in methods other than e-mail. Many malicious programs 'probe' your computer looking for known security holes or open services. A port scan of your system can take seconds and can find innumerable vulnerabilities. A firewall blocks outsiders from probing or connecting to your computer unless you initiate the connection via a Web browser or other program. ZoneAlarm is a popular free Windows firewall application and may be one good option for many users. If you have a cable modem or DSL connection at home, consider a dedicated hardware firewall/router from manufacturers like Linksys, SMC, or Netgear. </li>
      </ul>
      <p>Sophos is a popular virus scanner. Sophos scans your email for viruses before it is delivered to your account. If it finds a message that contains a virus as an attachment, Sophos will remove the attachment from the email. If the entire message is a virus, Sophos will replace the virus message with an alert message. Alerts can be delivered to your inbox, delivered to a separate mail folder, or automatically deleted based on user preference. </p>
      <p>Sophos can stop many common e-mail viruses; however, it is not a substitute for using desktop antivirus software (McAfee, Norton). To fully protect your personal computer, you should use Sophos in tandem with up-to-date desktop antivirus software, update your operating system regularly, use a firewall, choose secure passwords, and be cautious when sending or receiving files on the Internet. </p>
	  <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="7483891760";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
      <p>
      <?php include("../menu.php"); ?></p>
    </td>
  </tr>
</table>
</body>
</html>
