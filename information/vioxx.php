<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Vioxx Frequently Asked Questions and Answers FAQ - Lawsuit</title>
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Vioxx (rofecoxib) Questions and Answers</h3>
	 <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3478441614";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
      <p><b>1.&nbsp;&nbsp;&nbsp;&nbsp; What action did Merck take?&nbsp;</b></p>
      <p >Merck announced a voluntary worldwide withdrawal of Vioxx<b> </b>(rofecoxib).</p>
      <p ><b>2.&nbsp;&nbsp;What is Vioxx?</b></p>
      <p>Vioxx is a COX-2 selective nonsteroidal anti-inflammatory drug (NSAID).&nbsp; Vioxx<b> </b>is also related to the nonselective NSAIDs , such as ibuprofen and naproxen. Vioxx is a prescription medicine used to relieve signs and symptoms of arthritis, acute pain in adults, and painful menstrual cycles. </p>
      <p ><b>3.&nbsp;&nbsp;Did FDA require this action?</b></p>
      <p >No, Merck made this decision independent of input from FDA.&nbsp; The Agency has not had an opportunity to review the data from the study that was stopped in the depth that Merck has, but agrees with the company that there appear to be significant safety concerns for patients, particularly those taking the drug chronically.</p>
      <p >FDA plans to work closely with Merck to coordinate the withdrawal of this product from the US market.</p>
      <p ><b>4.&nbsp;&nbsp;What action did FDA take? </b></p>
      <p >FDA issued a public health advisory concerning the use of Vioxx. This advisory is based on Merck &amp; Co., Inc. voluntarily withdrawing Vioxx from the market due to safety concerns.</p>
      <p ><b>5.&nbsp;&nbsp;What should I do if I am currently taking Vioxx?</b></p>
      <p >The risk that an individual patient will suffer a heart attack or stroke related to Vioxx is very small.&nbsp; We encourage people taking Vioxx to contact their physician to discuss discontinuing use of Vioxx and alternative treatments.&nbsp; Any decision about which drug product to take to treat your symptoms should be made in consultation with your physician based on an assessment of your specific treatment needs.</p>
      <p > <b>6.&nbsp; What are the likely long-term health effects, if any, of taking this product?</b></p>
      <p >The new study shows that Vioxx may cause an increased risk in cardiovascular events such as heart attack and strokes during chronic use.</p>
      <p ><b>7. &nbsp;What evidence supports the Public Health Advisory?</b></p>
      <p >Merck�s decision to withdraw Vioxx from the market is based on new data from a trial called the APPROVe [ Adenomatous Polyp Prevention on VIOXX] trial.&nbsp; In the APPROVe trial, Vioxx was compared to placebo (sugar-pill).&nbsp; The purpose of the trial was to see if Vioxx 25 mg was effective in preventing the recurrence of colon polyps.&nbsp; This trial was stopped early because there was an increased risk for serious cardiovascular events, such as heart attacks and strokes, first observed after 18 months of continuous treatment with Vioxx compared with placebo.</p>
      <p ><b>8.&nbsp; Why wasn�t the APPROVe trial stopped earlier?</b></p>
      <p >The APPROVe trial began enrollment in 2000.&nbsp; The trial was being monitored by an independent data safety monitoring board (DSMB). It was not stopped earlier because the results for the first 18 months of the trial did not show any increased risk of confirmed cardiovascular events on Vioxx.&nbsp;&nbsp; </p>
      <p ><b>9.&nbsp; What did FDA know about the risk of heart attack and stroke when it approved Vioxx?</b></p>
      <p >FDA originally approved Vioxx in May 1999.&nbsp; The original safety database included approximately 5000 patients on Vioxx and did not show an increased risk of heart attack or stroke.&nbsp; A later study, VIGOR (VIOXX GI Outcomes Research), was primarily designed to look at the effects of Vioxx on side effects such as stomach ulcers and bleeding and was submitted to the FDA in June 2000.&nbsp; The study showed that patients taking Vioxx had fewer stomach ulcers and bleeding than patients taking naproxen, another NSAID, however, the study also showed a greater number of heart attacks in patients taking Vioxx. The VIGOR study was discussed at a February 2001 Arthritis Advisory Committee and the new safety information from this study was added to the labeling for Vioxx in April 2002. Merck then began to conduct longer-term trials to obtain more data on the risk for heart attack and stroke with chronic use of Vioxx.</p>
      <p ><b>10.&nbsp; Is FDA�s expedited review process putting riskier drugs on the market?&nbsp; </b></p>
      <p >No. Vioxx received a six-month priority review because the drug potentially provided a significant therapeutic advantage over existing approved drugs due to fewer gastrointestinal side effects, including bleeding.&nbsp; A product undergoing a priority review is held to the same rigorous standards for safety, efficacy, and quality that FDA expects from all drugs submitted for approval.&nbsp; </p>
      <p ><b>11. What other drugs are similar to Vioxx?</b></p>
      <p >Vioxx is a COX-2 selective, nonsteroidal anti-inflammatory drug (NSAID).&nbsp; Other COX-2 selective NSAIDs on the market at this time are Celebrex (celecoxib) and Bextra (valdecoxib). Vioxx is also related to the nonselective NSAIDs, such as ibuprofen and naproxen. You should consult your physician to determine which treatment is right for you. </p>
      <p ><b>12. Does today�s action suggest that other drugs in the same class are dangerous?</b></p>
      <p >The results of clinical studies with one drug in a given class do not necessarily apply to other drugs in the same class.&nbsp; All of the NSAIDs have risks when taken chronically, especially of gastrointestinal (stomach) bleeding, but also liver and kidney toxicity.&nbsp; Patients using these drugs for a long period of time (longer than two weeks) should be under the care of a physician.&nbsp; </p>
      <p ><b>13.&nbsp;&nbsp;Will Vioxx be recalled?</b></p>
      <p >FDA did not request a recall of Vioxx.&nbsp; This product is being voluntarily withdrawn from the market by Merck.</p>
      <p ><b>14.&nbsp; Can my pharmacist continue to fill my prescription for Vioxx?</b></p>
      <p >No, Merck is initiating a market withdrawal in the United States to the pharmacy level. This means Vioxx will no longer be available at pharmacies.</p>
      <p><b>15.&nbsp; How can I report a serious side effect with Vioxx to FDA?</b></p>
      <p>FDA encourages anyone aware of a serious adverse reaction to make a MedWatch report. You can report an adverse event in two ways:</p>
      <ul>
        <li>
          <p style="margin-left: 1in; text-indent: -0.25in;">Visit <a href="http://www.fda.gov/medwatch" >www.fda.gov/medwatch</a> and click on "How to Report" </p>
        </li>
        <li>
          <p style="margin-left: 1in; text-indent: -0.25in;">Call 1-800-FDA-1088 </p>
        </li>
      </ul>
      <p><b>16. &nbsp;Where can I get more information?</b></p>
      <p>You can obtain more information from Merck at:</p>
      <ul>
        <li>
          <p style="margin-left: 1in; text-indent: -0.25in;"> <a href="http://www.merck.com/" >www.merck.com</a> and <a href="http://www.vioxx.com/" >www.vioxx.com</a> , or</p>
        </li>
        <li>
          <p style="margin-left: 1in; text-indent: -0.25in;">1-888-36VIOXX (1-888-368-4699) </p>
        </li>
      </ul>
      <p>To find out more about Vioxx from FDA:</p>
      <ul>
        <li>
          <p style="margin-left: 0.5in; text-indent: -0.25in;">Visit the Drug Information web page at: <a href="http://www.fda.gov/cder" >www.fda.gov/cder</a></p>
        </li>
        <li>
          <p style="margin-left: 0.5in; text-indent: -0.25in;">Call Drug Information at: 888-INFO-FDA (888-463-6332)</p>
        </li>
      </ul>
	   <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3478441614";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<?php include("../menu.php"); ?>
    </td>
  </tr>
</table>
</body>
</html>
