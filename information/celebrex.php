<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Celebrex Frequently Asked Questions and Answers FAQ - Lawsuit - Heart Attack</title>
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Celebrex Questions and Answers</h3>
	 <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3478441614";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
     
	 <a name="top"></a><h1>FDA Statement</h1>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr> 
    <td width="67%"> 
      <p> FOR IMMEDIATE RELEASE<br>
        Statement<br>
        December 17, 2004 </p>

   </td>
<td width="33%">      <p class="contacts">Media Inquiries: 301-827-6242<br>
        Consumer Inquiries: 888-INFO-FDA<br>
        <a href="http://www.fda.gov/bbs/topics/news/2004/NEW01144.html" target="_blank">Story Link</a></p></td>
  </tr>
</tbody></table>
		<br>
<h2>FDA Statement on the Halting of a Clinical Trial of the Cox-2 Inhibitor Celebrex</h2>
		

<p>The FDA today released the following statement on the halting of a clinical trial of the Cox-2 inhibitor Celebrex (celecoxib):</p>
<p> The Food and Drug Administration (FDA) learned last night from the
National Cancer Institute (NCI) and Pfizer, Inc., that NCI has stopped
drug administration in an ongoing clinical trial investigating a new
use of Celebrex (celecoxib) to prevent colon polyps because of an
increased risk of cardiovascular (CV) events in patients taking
Celebrex versus those taking a placebo. </p>
<p> Patients in the clinical trial taking 400 mg. of Celebrex twice
daily had a 3.4 times greater risk of CV events compared to placebo.
For patients in the trial taking 200 mg. of Celebrex twice daily, the
risk was 2.5 times greater. The average duration of treatment in the
trial was 33 months.</p>
<p> A similar ongoing study comparing Celebrex 400 mg. once a day
versus placebo, in patients followed for a similar period of time, has
not shown increased risk. </p>
<p> Although these are important findings, at this point FDA has seen
only the preliminary results of the studies. FDA will obtain all
available data on these and other ongoing Celebrex trials as soon as
possible and will determine the appropriate regulatory action.</p>
<p> While we have not seen all available data on Celebrex, these
findings are similar to recent results from a study of Vioxx
(rofecoxib), another drug in the same class as Celebrex. Vioxx was
recently voluntarily withdrawn by Merck. Another drug in this class,
Bextra (valdecoxib) has shown an increased risk for CV events in
patients after heart surgery. Bextra and Celebrex are the only two
selective COX-2 agents currently on the U.S. market.</p>

<p> Physicians should consider this evolving information in evaluating
the risks and benefits of Celebrex in individual patients. FDA advises
evaluating alternative therapy. At this time, if physicians determine
that continued use is appropriate for individual patients, FDA advises
the use of the lowest effective dose of Celebrex. </p>
<p> Patients who are currently taking Celebrex and have questions or
concerns about the drug should discuss them with their physicians.</p>
<p> Celebrex was approved in 1998 for the treatment of osteoarthritis
and rheumatoid arthritis. Previous large studies of Celebrex, including
clinical trials and epidemiology studies, have not suggested the sort
of CV risk found in the NCI polyp study. Because similar long-term
studies of other products in the class of non-steroidal
anti-inflammatory drugs (NSAIDS), other than <br>
Cox-2 inhibitors have not been done, it is not known whether other NSAIDS pose a similar risk.</p>
<p> FDA will provide updates on Celebrex in particular and this class
of drugs in general as more information becomes available. </p>


<p align="left"><a href="http://www.fda.gov/cder/consumerinfo/druginfo/celebrex.htm" target="_blank">More Information</a> </p>
<hr>
 <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3478441614";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<hr>
<b>Pfizer Statement on New Information Regarding Cardiovascular Safety of Celebrex [<a href="http://www.celebrex.com/cardiovascular_safety_of_celebrex.asp" target="_blank">link</a>]</b></p>
 
 
  
 <p ><em>Dosing
in Long-Term Cancer Studies Suspended Due to Increased Cardiovascular
Risk in One Study; Preliminary Analysis of Second Long-Term Cancer
Study Shows No Increased Cardiovascular Risks</em></p>
  
 
 
  
<p ><b>NEW YORK, December 17</b> � Pfizer Inc
said it received new information last night about the cardiovascular
safety of its COX-2 inhibitor Celebrex (celecoxib) based on an analysis
of two long-term cancer trials.</p>

<p >As reported to Pfizer by the Data Safety and
Monitoring Board, one of the studies (the APC cancer trial)
demonstrated an increased cardiovascular risk over placebo, while the
other trial (the PreSAP cancer trial) revealed no greater
cardiovascular risk than placebo.</p>

<p >�These clinical trial results are new. The
cardiovascular findings in one of the studies (APC) are unexpected and
not consistent with the reported findings in the second study (PreSAP).
Pfizer is taking immediate steps to fully understand the results and
rapidly communicate new information to regulators, physicians and
patients around the world,� said Hank McKinnell, Pfizer chairman and
chief executive officer.</p> 

<p >Celebrex is approved for use in the United States
for the treatment of arthritis and pain, at recommended doses of 100mg
to 200mg daily for osteoarthritis and 200mg to 400mg a day for
rheumatoid arthritis. It is also approved for a rare condition called
familial adenomatous polyposis in doses up to 800mg per day. The APC
cancer trial studied Celebrex at doses of 400mg to 800mg per day. In
the PreSAP cancer trial the dose was 400mg per day.</p>

<p >�In placing this new information in context, it is
important to understand that the APC trial results differ from both the
PreSAP cardiovascular results as well as the large body of data that we
and others have accumulated over time, in which an increased risk of
serious cardiovascular events in arthritis patients, even at
higher-than-recommended doses, had not been seen,� said Dr. Joseph
Feczko, president of worldwide development for Pfizer.</p>

<p >�Celebrex is an important medicine that provides
necessary pain relief to many patients. Patients being treated with
Celebrex should discuss appropriate treatment options with their
healthcare professionals. Physicians should factor this new
information, as well as ulcer risks and gastrointestinal bleeding seen
with traditional NSAIDs, into their prescribing decision.�</p>

<p >In the Adenoma Prevention with Celecoxib (APC)
trial, patients taking 400mg and 800mg of Celebrex daily had an
approximately 2.5 fold increase in their risk of experiencing a major
fatal or non-fatal cardiovascular event compared to those patients
taking placebo, according to the National Cancer Institute (NCI). Based
on these statistically significant findings, the sponsor of the trial,
the NCI, has suspended the dosing of Celebrex in the study.</p>

<p >In a separate long-term study, the Prevention of
Spontaneous Adenomatopus Polyps (PreSAP) trial, there has been no
increased risk for Celebrex patients taking 400mg daily compared with
those taking placebo. These findings are based on an identical analysis
used to assess cardiovascular risk in the APC trial and conducted by
the same independent safety review board. The information from this
Pfizer sponsored trial was also received by Pfizer last night and, as
with the APC information, was immediately shared by the company with
the U.S. Food and Drug Administration.</p>

<p >The two studies, which are following patients over
a five-year period, have enrolled a total of about 3,600 patients, some
of whom have participated for more than four years. Pfizer estimates
that about 2,400 patients evaluated in the cardiovascular analysis have
completed two years of treatment.</p>

<p >A third long-term study involving Celebrex in
patients at high-risk for Alzheimer�s disease is also under way with
about 2,000 patients enrolled, about 750 of whom are on 400mg per day
of Celebrex. As with the cancer studies, this study is monitored by
independent safety experts who meet regularly to assess adverse events.
A review by this board as recent as December 10 did not result in any
recommendations to change the conduct of this study.</p>

<p >In September and October, the Data Safety and
Monitoring Boards of the APC and PreSAP cancer trials conducted a
preliminary review of all the then-available data and determined to
proceed with the studies. With the cooperation of Pfizer, the safety
review boards convened a panel of cardiovascular experts to conduct
additional reviews and analyses of the data from these two trials. Last
evening, Pfizer received preliminary information resulting from the
reviews. The company has not yet received the full analyses of these
studies.</p>

<p >As previously announced, Pfizer will continue to
work with FDA on the company�s plans to sponsor a major clinical study
to further assess Celebrex in osteoarthritis patients at high-risk for
cardiovascular disease.</p>

<p ><b>Additional Information on Celebrex </b><br>
Patients who have aspirin-sensitive asthma, or allergic reactions to
aspirin or other arthritis medicines or certain sulfa drugs called
sulfonamides, or who are in their third trimester of pregnancy should
not take Celebrex. As with all NSAIDs, serious gastrointestinal tract
ulcerations can occur without warning symptoms. Physicians and patients
should remain alert to the signs and symptoms of GI bleeding. Celebrex
does not affect platelet function and therefore should not be used for
cardiovascular prophylaxis. As with all NSAIDs, Celebrex should be used
with caution in patients with fluid retention, hypertension, or heart
failure. In overall clinical studies the most common side effects of
Celebrex were dyspepsia, diarrhea and abdominal pain, which were
generally mild to moderate. </p>

	 
	 
      <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="3478441614";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<?php include("../menu.php"); ?>
    </td>
  </tr>
</table>
</body>
</html>
