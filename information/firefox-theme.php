<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Theming Mozilla Firefox</title>
<meta name="description" content="My steps to customize Firefox to make it much more aqua like.">
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>FireFox - AquaFox - Theming Mozilla Firefox</h3>
     
      <p align="center"><img src="FireFox/alloyFF.png" alt="FireFox-AquaFox" width="500" height="115" border="1" title="Click to Enlarge"><br>
        Aluminum Alloy Graphics by <a href="http://www.maxthemes.com/" target="_blank">Max Themes</a></p>
        <p align="center"><img src="FireFox/siroFF.jpg" alt="FireFox-AquaFox" width="500" height="115" border="1" title="Click to Enlarge"><br>
        Siro Graphics by <a href="http://homepage.mac.com/dsky/" target="_blank">Daisuke Yamashita</a></p>
        <p align="center"><img src="FireFox/aquaFF.jpg" alt="FireFox-AquaFox" width="500" height="115" border="1" title="Click to Enlarge"><br>
        Default Buttons with an Aqua Desktop as background.</p>
      <p>Theming <a href="http://www.spreadfirefox.com/?q=affiliates&id=25940&t=1" target="_blank">Firefox</a>. I decided that I wanted to make a Firefox theme, but I didn't want to spend the time to learn how to make official themes. So I did some research and found out that you could change the button area via style sheets. This tutorial is how to make a CSS theme under Mac OS X. If a PC user knows how to find their userChrome.css folder this should work for them too. </p>
      <p>One <font color="#990000">important</font> note: This way of Theming Firefox does have an issue. You can only use default icons in the toolbar. This means forward, back, home, etc. Any extension icons that you add to the toolbar will not work. You will end up with a mess. The icon between the address bar and google bar, <a href="aquafox.php">as shown here</a>, is an example of an extension icon that I had added to Firefox's toolbar and can't use with this Theming method.</p>
      <p>To theme Firefox this way is pretty easy, just a few steps to follow.</p>
      <p>1) download four graphic files. <a href="FireFox/toolbar.png" target="_blank">Buttons Big</a> <a href="FireFox/toolbar-small.png" target="_blank">Buttons Small</a> <a href="FireFox/pinstripe.png" target="_blank">Pinstripe</a> <a href="FireFox/header.gif" target="_blank">Header</a> Now open your favorite graphics program and edit these  files, replacing what you want. Pinstripe and Header can also be edited to be any size. When finished save them out as PNG files, just as you found them. Tiffs also work and I'm pretty sure any graphics file will too.</p>
      <p>2) quit Firefox</p>      
      <p>3) navigate to your Firefox's Chrome folder. <br>
      home -&gt; library -&gt; application support -&gt; firefox -&gt; profiles -&gt; (random letters).default -&gt; chrome</p>
      <p>4) create a folder in here for your theme. I'm calling mine alloy. Copy the four files you created into this folder.</p>
      <p>5) inside the chrome folder you should see a file called userChrome.css. Duplicate this to make a backup copy. Then open userChrome.css in a text editor.</p>
      <p>6) add the following lines to the bottom of that file.</p>    
      <blockquote><code>/* theming firefox by TwisterMc */<br>
        #PersonalToolbar {<br>
background-image: url(&quot;alloy/pinstripe.png&quot;) !important;<br>
background-repeat: repeat !important;<br>
border-bottom-width: 0px !important;<br>
}<br>
menubar, toolbox, toolbar { <br>
background-image: url(&quot;alloy/header.png&quot;) !important; <br>
background-color: none !important; <br>
}<br>
.toolbarbutton-1 {<br>
list-style-image: url(&quot;alloy/Toolbar.png&quot;) !important;<br>
}<br>
toolbar[iconsize=&quot;small&quot;] .toolbarbutton-1 {<br>
list-style-image: url(&quot;alloy/Toolbar-small.png&quot;) !important;<br>
}<br>
/* end theming firefox by TwisterMc */ </code></blockquote>     
      <p>Make sure to change the folder name (alloy) to your own.
</p>
      <p>7) Restart Firefox to see your changes!</p>
      <p>Too un-install a theme you can do two things. a) delete your folder and userChrome.css and rename that version that you backed up to userChrome.css and restart Firefox. b) open userChrome.css and remove the lines you added.</p>
      <p>Would you like to see more about <a href="firefox.php">Firefox customization</a>?</p>      
      <p>Do you have some tips or tricks? How about questions? Join our <a href="http://www.jrwrestling.com/forum/showthread.php?p=4235#post4235">Firefox discussion</a>. Need more things to customize? Check out <a href="http://www.mozilla.org/support/firefox/tips.html" target="_blank">Mozilla's Tips</a>. </p>
      <p>&nbsp;</p>
      <p> <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="2904136653";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
          <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
      </p>
      <p>
        <?php include("../menu.php"); ?>
      </p>
    </td>
  </tr>
</table>
</body>
</html>
