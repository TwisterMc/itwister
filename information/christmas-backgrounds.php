<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Christmas Desktops Backgrounds Holiday Wallpaper</title>
<meta name="description" content="Get your Christmas - Holiday desktops, backgrounds and wallpaper today!">
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Christmas Desktops Backgrounds Holiday Wallpaper</h3>
      <p>Download your <b>Christmas and Holiday background/wallpaper</b> today!  Enjoy it on your Mac or PC.  All designs are created at 1280x960.  If your screen is smaller than that your computer should shrink down the image and make it look good.  To download the images right click on the download link and choose to save it to the desktop.  Then go into your system settings and change your background image or wallpaper. Happy Holidays!</p>
      <p align="center">Get more desktops from <a href="http://pixojo.com">Pixojo's desktop collection.</a></p>
      <table border="0" cellpadding="2" cellspacing="2">
	 
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td width="136" rowspan="3"><img src="desktopx/christmas/blocks.gif" alt="Christmas Boxes Desktop" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Christmas Boxes</b></p>
                  <p><b><a href="desktopx/christmas/blocks.png" target="_blank">Download</a></b>
                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
            </table>
          </td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="136" rowspan="3"><img src="desktopx/christmas/candyCane.gif" alt="Candy Cane Wallpaper" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Candy Cane</b></p>
                  <p><b><a href="desktopx/christmas/candyCane.png" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="136" rowspan="3"><img src="desktopx/christmas/frostySnow.gif" alt="Let It Snow Background" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Let It Snow</b></p>
                  <p><b><a href="desktopx/christmas/frostySnow.png" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="136" rowspan="3"><img src="desktopx/christmas/frostyStarB.gif" alt="Blue Star Sky Desktop" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Blue Sky Star</b></p>
                  <p><b><a href="desktopx/christmas/frostyStarB.png" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td rowspan="3" width="136"><img src="desktopx/christmas/frostyStarW.gif" alt="White Sky Star Wallpaper" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>White Sky Star</b></p>
                  <p><b><a href="desktopx/christmas/frostyStarW.png" target="_blank">Download</a></b></p>        
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td width="136" rowspan="3"><img src="desktopx/christmas/green.gif" alt="Green Christmas Background" width="136" height="104"></td>
              <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
              <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
            </tr>
            <tr>
              <td align="left" height="84" valign="top"><p><b>Green Christmas</b></p>
                  <p><b><a href="desktopx/christmas/green.png" target="_blank">Download</a></b></p>
              </td>
              <td background="../images1/roundy/roundbox_right.gif"></td>
            </tr>
            <tr>
              <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
              <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
            </tr>
          </table></td>
        </tr>
		<tr>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="136" rowspan="3"><img src="desktopx/christmas/red.gif" alt="Red Christmas" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Red Christmas</b></p>
                  <p><b><a href="desktopx/christmas/red.png" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
              <tr>
                <td width="136" rowspan="3"><img src="desktopx/christmas/snowflakes1.gif" alt="Holiday Snowflakes on Strings" width="136" height="104"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Snowflakes on Strings</b></p>
                  <p><b><a href="desktopx/christmas/snowflakes1.png" target="_blank">Download</a></b></p>        
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
              <td width="136" rowspan="3"><img src="desktopx/christmas/snowflakes2.gif" alt="Winter Snowflakes" width="136" height="104"></td>
              <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
              <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
            </tr>
            <tr>
              <td align="left" height="84" valign="top"><p><b>Snowflakes</b></p>
                  <p><b><a href="desktopx/christmas/snowflakes2.png" target="_blank">Download</a></b></p>
              </td>
              <td background="../images1/roundy/roundbox_right.gif"></td>
            </tr>
            <tr>
              <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
              <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
            </tr>
          </table></td>
        </tr>
		 <tr>
	  
	  <td align="center" valign="middle">
=	  </td>
=	  <td></td>
	  </tr>
      </table>
	  <p>
      <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="5420425660";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
      <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></p>
      <?php include("../menu.php"); ?>
      
    </td>
  </tr>
</table>
</body>
</html>
