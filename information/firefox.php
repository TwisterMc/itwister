<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>FireFox - AquaFox - Customizing Mozilla Firefox</title>
<meta name="description" content="My steps to customize Firefox to make it much more aqua like.">
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>FireFox - AquaFox - Customizing Mozilla Firefox</h3>
     
      <p align="center"><a href="aquafox.php"><img src="FireFox/AquaFoxSmall.png" alt="FireFox-AquaFox" width="350" height="246" border="0" title="Click to Enlarge"></a> </p>
      <p>Have you checked out <a href="http://www.spreadfirefox.com/?q=affiliates&id=25940&t=1" target="_blank">Firefox</a>? It's one of the most customizable browsers I've found. I've taken may steps and experimented with many different parts of Firefox. When it all comes down to it, most things are editable in the userChrome.css file. That's right, for the most part, it's just CSS changes. With a few graphics thrown in of course.</p>
      <p>First thing to note is that you need to back stuff up first. There is no guarantee that you won't screw something up if you going and start editing stuff. Also, these techniques have been used in the PR and RC versions of Firefox but are not transferable. That means that if you get it to look good on one computer you probably can't just copy the files over to a new computer. Also, I'm on a Mac and have only tried these techniques on a Mac. I'm unsure if they work on a PC but I'm pretty sure they should. One more, I don't know how well these changes will do with updgrades. Only time will tell. </p>
      <p>So what have I done to Firefox?</p>
      <p>&#8226; I have <a href="http://www.hicksdesign.co.uk/journal/596/safari-style-tabs-for-firefox-os-x" target="_blank">Safari Tabs</a> installed in Firefox. Thanks to <a href="http://www.hicksdesign.co.uk/" target="_blank">hicksdesign</a> for coming up with these edits. Once installed and working, I opened up the graphic files and removed the pinstripes. Now I have solid white Safari style tabs in Firefox</p>
      <p>&#8226; I have <a href="http://kmgerich.com/archive/000072.html" target="_blank">pretty widgets</a> installed in Firefox also. It makes the checkboxes, input boxes and radio buttons a little more like MacOSX</p>
         <p>&#8226; <a href="http://sage.mozdev.org/" target="_blank">Sage</a> is the RSS reader I have installed, that is what is showing on the screen shot. I then applied an <a href="http://www.hicksdesign.co.uk/journal/588/sage-on-os-x" target="_blank">aqua makeover</a> to make it much nicer on the eyes. I also replaced the Sage leaf icon with the new RSS/Live Bookmark icon in Firefox RC1</p>
         <p>&#8226; Address bar fix -   In Firefox's address bar type about:config and you will see hundreds of options that you can customize! Change browser.urlbar.clickSelectsAll to false. This tells the browser, than when you click in the address bar, don't highlight everything. Just keep the courser where you clicked!</p>
         <p>&#8226; No focus box around links when clicked on - In the about:config area change browser.display.focus_ring_width equal to 0.  This should make it go away.
         <p><b>My userChrome.css edits</b> - Now is where I really started playing with things. Go to your <b>home folder -&gt; application Support -&gt; Firefox -&gt; Profiles -&gt; [random folder] -&gt; Chrome</b> Here is where you edit your CSS file and add any images.</p>
         <p>&#8226; Toolbar - For the toolbar background I used the header.png file that was included with the Sage makeover files. I edited it to make it a little smaller and then added it to the Chrome folder. Then, inside userChrome.css I added.         
      <blockquote><code>menubar, toolbox, toolbar { 
          <br>
        background-image: url("header.png") !important;<br> 
   background-color: none !important;<br> 
 }</code></blockquote>       
      <p>&#8226; Bookmark Bar Background - Here the only change I made was to pinstripe.png inside the Chrome folder. Once changed it took effect.</p>
      <p>&#8226; I've changed a few other things, like the blue folder text to make them easier to see and changed all bookmark text go a lighter gray. All can be edited in the userChrome.css      <blockquote><code>
	  /* Make bookmark folders bold and blue */<br>
.bookmark-item[type="menu"] > .toolbarbutton-text {
   font-weight: 900;
   color: #3d6bff;
}
<br>
/* Make tab group bookmarks  */<br>
.bookmark-group > .toolbarbutton-text {
   font-weight: 900;
   color: #3d6bff;
}
<br>
/* bookmark folders */<br>
menu.bookmark-item {
  color: #3d6bff !important;
  font-weight: 900 !important;
}
<br>
/* You could select normal bookmarks this way */<br>
menuitem.bookmark-item {
  color: #464646 !important;
}</code></blockquote>
      <p><b>Extensions Installed</b></p>
      <p><a href="http://spellbound.sourceforge.net/" target="_blank">SpellBound</a><br>
        <a href="http://www.tapouillo.com/firefox_extension/" target="_blank">Google Page Rank</a><br>
        <a href="http://adblock.mozdev.org/" target="_blank">Ad Block</a><br>
        <a href="http://weatherfox.mozdev.org/" target="_blank">WeatherFox</a><br>
        <a href="http://dmextension.mozdev.org/" target="_blank">Download Manager Tweak</a><br>
<a href="http://sage.mozdev.org/" target="_blank">Sage</a><br>
<a href="http://update.mozilla.org/extensions/moreinfo.php?id=60&vid=645" target="_blank">Web Developer<br>
</a><a href="http://extensionroom.mozdev.org/more-info/copyimage" target="_blank">Copy Image</a></p>
<p><b>Would you like to create custom <a href="firefox-theme.php">firefox themes</a>?</b></p>
      <p>Do you have some tips or tricks? How about questions? Join our <a href="http://www.jrwrestling.com/forum/showthread.php?p=4235#post4235">Firefox discussion</a>. Need more things to customize? Check out <a href="http://www.mozilla.org/support/firefox/tips.html" target="_blank">Mozilla's Tips</a>. </p>
      <p>&nbsp;</p>
      <p> <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="2904136653";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
          <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                  </script>
      </p>
      <p>
        <?php include("../menu.php"); ?>
      </p>
    </td>
  </tr>
</table>
</body>
</html>
