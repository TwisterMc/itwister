<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Halloween Desktops Backgrounds Wallpaper</title>
<meta name="description" content="Get your halloween desktops, backgrounds and wallpaper today!">
<link href="../style2.css" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="750" border="0" align="center" cellpadding="4" cellspacing="0" class="outsideBorder">
  <tr>
    <td><h3>Halloween Desktops, Backgrounds and Wallpaper</h3>
    <p align="center">Get more desktops from <a href="http://pixojo.com">Pixojo's desktop collection.</a></p>
      <table border="0" cellpadding="2" cellspacing="2">
        <tr>
          <td valign="top">
            <table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td rowspan="3"><img src="desktopx/slick.jpg" height="104" width="136"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Slick Orange</b></p>
                  <p><b><a href="http://www.itwister.net/information/desktopx/halloween/SO_2_1280.jpg" target="_blank">Download</a></b>
                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
            </table>
          </td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td rowspan="3"><img src="desktopx/crystal.jpg" height="104" width="136"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Orange Crystal</b></p>
                  <p><b><a href="http://www.itwister.net/information/desktopx/halloween/OrangeCrystal.jpg" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td rowspan="3"><img src="desktopx/aqua.jpg" height="104" width="136"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Aqua  Orange</b></p>
                  <p><b><a href="http://www.itwister.net/information/desktopx/halloween/Aqua-Plastic-Orange.jpg" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
        </tr>
        <tr>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td rowspan="3"><img src="desktopx/pumpkin.jpg" height="104" width="136"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Halloween Pumpkin</b></p>
                  <p><b><a href="http://www.itwister.net/information/desktopx/halloween/halloweendesktop.gif" target="_blank">Download</a></b>                  </p>
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td rowspan="3"><img src="desktopx/jack-pumpkin.jpg" height="104" width="136"></td>
                <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
              </tr>
              <tr>
                <td align="left" height="84" valign="top"><p><b>Pumpkin &amp; Jack </b></p>
                  <p><b><a href="http://www.itwister.net/information/desktopx/halloween/halloweendesktop.jpg" target="_blank">Download</a></b></p>        
                </td>
                <td background="../images1/roundy/roundbox_right.gif"></td>
              </tr>
              <tr>
                <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
                <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
              </tr>
          </table></td>
          <td valign="top"><table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td rowspan="3"><img src="desktopx/hops.jpg" height="104" width="136"></td>
              <td background="../images1/roundy/roundbox_top.gif" height="10"></td>
              <td height="10" width="10"><img src="../images1/roundy/roundbox_topright.gif"></td>
            </tr>
            <tr>
              <td align="left" height="84" valign="top"><p><b>Orange Hops</b></p>
                  <p><b><a href="http://www.itwister.net/information/desktopx/halloween/hops1600x1200.jpg" target="_blank">Download</a></b></p>
              </td>
              <td background="../images1/roundy/roundbox_right.gif"></td>
            </tr>
            <tr>
              <td background="../images1/roundy/roundbox_bottom.gif" height="10"></td>
              <td height="10" width="10"><img src="../images1/roundy/roundbox_bottomright.gif"></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="5420425660";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "114477";
google_color_url = "114477";
google_color_text = "000000";
//--></script>
      <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<p>Halloween Desktops, Backgrounds and Wallpaper page has been last updated on 9/22/05 with updated links to the desktops.  Now it's easier to access the Halloween desktop pictures and easier to enjoy them as well.</p><p>Also check out the <a href="http://www.itwister.net/NightmareBeforeChristmas/">Nightmare Before Christmas</a> page for some great Jack Skellington and crew stuff.</p>
      <?php include("../menu.php"); ?>
      </p>
    </td>
  </tr>
</table>
</body>
</html>
