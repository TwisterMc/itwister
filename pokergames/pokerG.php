<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
<?php include("termsINC.php"); ?>
<h3>G-Note</h3><p>
  A one thousand dollar bill.</p>
<h3>Get The Right Price</h3><p>
  The pot odds are favorable enough for you to justify calling a bet or a raise with a drawing hand.</p>
<h3>Get Full Value</h3><p>
  Betting, raising and re-raising in order to manipulate the size of the pot so that you will be getting maximum pot odds if you win the hand.</p>
<h3>Get There</h3><p>
  To make your hand.</p>
<h3>Give Action</h3><p>
  Betting, calling, raising or re-raising.</p>
<h3>Gut-Shot</h3><p>
  A card drawn to fill an inside straight.</p>
<h3>Gypsy In</h3><p>
  In lowball, to limp in.</p>

 </td>
  </tr>
</table>
</body>
</html>
