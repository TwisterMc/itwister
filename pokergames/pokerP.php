<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Paint Cards</h3><p>
  King, Queen and Jack; face cards; court cards; picture cards.</p>
<h3>Pair</h3><p>
  Two cards of the same denomination.</p>
<h3>Pass</h3><p>
  Fold.</p>
<h3>Pat Hand</h3><p>
  A hand that is played as dealt, without changing a card; usually a straight, flush or full house.</p>
<h3>Pay Off</h3><p>
  To call a bet or raise when you don't think you have the best hand.</p>
<h3>Pay Station</h3><p>
  A player who calls bets and raises much more than is typical; a calling station.</p>
<h3>Picture Cards</h3><p>
  King, Queen and Jack; face cards; court cards; paint cards.</p>
<h3>Pip</h3><p>
  The suit symbols on a non-court card, indicating its rank.</p>
<h3>Play Back</h3><p>
  To raise or re-raise an opponent's bet.</p>
<h3>Play Fast</h3><p>
  Aggressively betting a drawing hand to get full value for it if you make it.</p>
<h3>Play With</h3><p>
  Staying in the hand by betting, calling, raising, or re-raising.</p>
<h3>Playing the Board</h3><p>
  In flop games, if your best five card hand uses the five community cards.</p>
<h3>Pocket</h3><p>
  Another term for hole.</p>
<h3>Pocket Rockets</h3><p>
  A pair of aces in the hole.</p>
<h3>Position</h3><p>
  Your seat in relation to the dealer, and thus your place in the betting order.</p>
<h3>Post</h3><p>
  To post a bet is to place your chips in the pot.</p>
<h3>Pot</h3><p>
  The money or chips in the center of the table.</p>
<h3>Pot Limit</h3><p>
  A game in which the maximum bet is the total of the pot.</p>
<h3>Pot Odds</h3><p>
  The amount of money in the pot versus the amount of money it will cost you to continue in the hand.</p>
<h3>Prop</h3><p>
  Short for proposition player; similar to a shill, but plays with his own money.</p>
<h3>Proposition Player</h3><p>
  A cardroom employee who joins a game with his own money when the game is shorthanded, or to get a game started; similar to a shill.</p>
<h3>Protect A Hand</h3><p>
  To protect a hand is to bet so as to reduce the chances of anyone outdrawing you by getting them to fold.</p>
<h3>Protect Your Cards</h3><p>
  To protect your cards is to place a chip or some other small object on top of them so that they don't accidentally get mucked by the dealer, mixed with another player's discards, or otherwise become dead when you'd like to play them.</p>
<h3>Provider</h3><p>
  A player who makes the game profitable for the other players at the table; a nicer term for a fish.</p>
<h3>Push</h3><p>
  When the hand is finished and a winner is determined, the dealer pushes the chips towards the winner.</p>
<h3>Put Down</h3><p>
  Fold.</p>
<h3>Put Him On</h3><p>
  To guess an opponent's hand and play accordingly.</p>
<h3>Putting On The Heat</h3><p>
  Pressuring your opponents with aggressive betting strategies to get the most value from your hand.</p>

 </td>
  </tr>
</table>
</body>
</html>
