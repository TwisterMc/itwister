<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Main Pot</h3><p>
  When a player goes all-in, that player is only eligible to win the main pot - the pot consisting of the bets they were able to match. Additional bets are placed in a side pot and are contested among the remaining players.</p>
<h3>Make</h3><p>
  To make the deck is to shuffle.</p>
<h3>Make A Move</h3><p>
  To try a bluff.</p>
<h3>Maniac</h3><p>
  A very aggressive player who plays hands that more conservative players would probably not consider.</p>
<h3>Mark</h3><p>
  A sucker.</p>
<h3>Marker</h3><p>
  An IOU.</p>
<h3>Mechanic</h3><p>
  A cheat who manipulates the deck.</p>
<h3>Meet</h3><p>
  To call.</p>
<h3>Middle Pair</h3><p>
  In flop games, a middle pair is made by pairing with the middle card on the flop.</p>
<h3>Middle Position</h3><p>
  A position on a round of betting somewhere in the middle.</p>
<h3>Miss</h3><p>
  To be unable to make your drawing hand when the final cards are dealt.</p>
<h3>Monster</h3><p>
  A hand that is almost certain to win.</p>
<h3>Move In</h3><p>
  To go all-in.</p>
<h3>Muck</h3><p>
  To discard a hand; also the discard pile in which all cards are dead.</p>

 </td>
  </tr>
</table>
</body>
</html>
