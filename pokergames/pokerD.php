<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
<?php include("termsINC.php"); ?>
<h3>Dead Card</h3><p>
  A card no longer legally playable.</p>
<h3>Dead Hand</h3><p>
  A hand no longer legally playable, due to some irregularity.</p>
<h3>Dead Money</h3><p>
  Money put into the pot by players who have already folded.</p>
<h3>Dealer's Choice</h3><p>
  A game in which each dealer, in turn, chooses the type of poker to be played.</p>
<h3>Declaration</h3><p>
  In high-low poker, declaring by the use of coins or chips whether one is aiming to win the high or the low end of the pot, or both.</p>
<h3>Declare Games</h3><p>
  Games in which a player must declare the value of his hand in order to claim the pot.</p>
<h3>Deuce</h3><p>
  A two, the lowest ranking card in high poker.</p>
<h3>Deuce to Seven</h3><p>
  Another term for Kansas City Lowball, a two to seven without a flush,being the best hand.</p>
<h3>Dominate</h3><p>
  Said of a starting hand that will almost always beat another starting hand.</p>
<h3>Door Card</h3><p>
  In Seven-Card Stud, the first exposed card in a player's hand.</p>
<h3>Double Belly Buster</h3><p>
  A hand with two inside straight draws.</p>
<h3>Double Gut Shot</h3><p>
  A draw to a broken sequence of cards, in which either of two cards will make the straight.</p>
<h3>Double Through</h3><p>
  Going all-in against an opponent in order to double your stack if you win the hand.</p>
<h3>Down Cards</h3><p>
  Hole cards.</p>
<h3>Down To The Felt</h3><p>
  A player who has lost most of his chips.</p>
<h3>Draw Lowball</h3><p>
  A form of poker in which the lowest hand wins.</p>
<h3>Draw Out</h3><p>
  To improve your hand so that it beats an opponent who had a better hand than yours prior to your draw.</p>
<h3>Draw Poker</h3><p>
  A form of poker in which each player receives five cards and then has the option of discarding one or more of them and receiving new cards in their place.</p>
<h3>Drawing Dead</h3><p>
  Drawing to a hand that cannot possibly win.</p>
<h3>Drawing Hand</h3><p>
  A potentially strong hand requiring a particular card from the draw to make it.</p>
<h3>Driver's Seat</h3><p>
  The player who is making all the betting and thus appears to hold the strongest hand is said to be in the driver's seat.</p>
<h3>Drop</h3><p>
  To fold.</p>

 </td>
  </tr>
</table>
</body>
</html>
