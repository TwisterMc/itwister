<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>

<h3>Table</h3><p>
  Refers to the poker table itself, or the collective players in the game.</p>
<h3>Table Cop</h3><p>
  A player who calls with the intention of keeping other players honest.</p>
<h3>Table Stakes</h3><p>
  A poker game in which a player cannot bet more than the money he has on the table.</p>
<h3>Table Talk</h3><p>
  Any discussion at the table of the hand currently underway, especially by players not involved in the pot, and especially any talk that might affect play.</p>
<h3>Take Off A Card</h3><p>
  To call a single bet in order to see one more card.</p>
<h3>Take Off The Gloves</h3><p>
  To use an aggressive betting strategy to bully opponents.</p>
<h3>Take The Odds</h3><p>
  To wager less money on a proposition than you hope to win.</p>
<h3>Tap City</h3><p>
  To go broke.</p>
<h3>Tap Out</h3><p>
  To bet all one's chips.</p>
<h3>Tapped Out</h3><p>
  Broke, busted.</p>
<h3>Tell</h3><p>
  A player's nervous habit or mannerism which might reveal his hand.</p>
<h3>Texas Hold 'Em</h3><p>
  A form of poker in which players use five community cards in combination with their two hole cards to form the best five-card hand. Also called hold 'em.</p>
<h3>Third Pair</h3><p>
  In flop games, pairing the third highest card on board.</p>
<h3>Third Street</h3><p>
  In Seven-Card Stud, the first round of betting on the first three cards.</p>
<h3>Three Flush</h3><p>
  Three cards of the same suit, requiring two more to make a flush.</p>
<h3>Three Of A Kind</h3><p>
  Three cards of the same denomination, with two side cards; trips.</p>
<h3>Throwing A Party</h3><p>
  When several loose or amateur players are making significant monetary contributions to the pot.</p>
<h3>Tight</h3><p>
  A conservative player who only plays strong hands, or playing on fewer hands than the norm.</p>
<h3>Tight Game</h3><p>
  A game with a small number of players in most pots.</p>
<h3>Tilt</h3><p>
  See on tilt.</p>
<h3>To Go</h3><p>
  An amount &quot;to go&quot; is the amount it takes to enter the pot.</p>
<h3>Toke</h3><p>
  A tip to the dealer.</p>
<h3>Top Pair</h3><p>
  In flop games, pairing the highest card on board.</p>
<h3>Trey</h3><p>
  A three.</p>
<h3>Triplets</h3><p>
  Three of a kind.</p>
<h3>Trips</h3><p>
  Slang for triplets; three of a kind.</p>
<h3>Turn</h3><p>
  In flop games, the fourth street card.</p>
<h3>Two Flush</h3><p>
  Two cards of the same suit, requiring three more to make a flush.</p>
<h3>Two Pair</h3><p>
  A hand with two pairs and a kicker.</p>

 </td>
  </tr>
</table>
</body>
</html>
