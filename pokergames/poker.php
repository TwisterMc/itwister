<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
	<?php include("termsINC.php"); ?>
<h3>Ace-High</h3><p>
A five-card hand containing an ace but no pair; beats a king-high, but loses to any pair or above.
<h3>Aces Full</h3><p>
  A full house with aces over any pair.</p>
<h3>Aces Up</h3><p>
  Two pairs, one of which is aces.</p>
<h3>Action</h3><p>
  The betting.</p>
<h3>Active Player</h3><p>
  A player still in the pot.</p>
<h3>Add-On</h3><p>
  The opportunity to buy additional chips in some tournaments.</p>
<h3>Advertise</h3><p>
  To make a bluff with the deliberate intention of being exposed as a loose player.</p>
<h3>All-In</h3><p>
  When a player bets all his or her remaining chips.</p>
<h3>An Ace Working</h3><p>
  An ace in hand.</p>
<h3>Angle</h3><p>
  Any technically legal but ethically dubious way to increase your expectation at a game; a trick.</p>
<h3>Ante</h3><p>
  A token bet required before the start of a hand.</p>
<h3>Baby</h3><p>
  A small card.</p>
<h3>Back Door Flush (or Straight)</h3><p>
  When the last two cards make a player's hand, even though he or she played on the flop for some other reason.</p>
<h3>Back Into A Hand</h3><p>
  To draw into a hand different from the one you were originally trying to make.</p>
<h3>Bad Beat</h3><p>
  When a strong hand is beaten by a lucky hand; a longshot win.</p>
<h3>Bankroll</h3><p>
  The amount of money you have available to wager.</p>
<h3>Behind</h3><p>
  You're behind if you don't have the best hand before the last cards have been dealt.</p>
<h3>Belly Buster</h3><p>
  A draw to fill an inside straight; a gut shot.</p>
<h3>Bet</h3><p>
  To voluntarily put money or chips into the pot.</p>
<h3>Bet For Value</h3><p>
  Betting in order to raise the amount in the pot, not to make your opponents fold.</p>
<h3>Bet Into</h3><p>
  To bet before a stronger hand, or a player who bet strongly on the previous round.</p>
<h3>Bet The Pot</h3><p>
  To bet the total value of the pot.</p>
<h3>Betting Black</h3><p>
  Betting $100 amounts (black is a common color for $100 chips).</p>
<h3>Betting Green</h3><p>
  Betting $25 amounts (green is a common color for $25 chips).</p>
<h3>Betting Red</h3><p>
  Betting $5 amounts (red is a common color for $5 chips).</p>
<h3>Betting White</h3><p>
  Betting $1 amounts (white is a common color for $1 chips).</p>
<h3>Betting Interval</h3><p>
  The period during which each active player has the right to check, bet or raise; the round of betting. It ends when the last bet or raise has been called by all players still in the hand.</p>
<h3>Bicycle</h3><p>
  The lowest possible hand in lowball: Ace-2-3-4-5. Also called a wheel.</p>
<h3>Big Bet Poker</h3><p>
  Another term for pot-limit and no-limit poker.</p>
<h3>Big Blind</h3><p>
  The forced bet in second position before any cards are dealt. Usually this is a Live Blind, which means that the player in this position can raise if no one else has before the cards are dealt.</p>
<h3>Big Slick</h3><p>
  The Ace-King card combination.</p>
<h3>Black Leg</h3><p>
  Archaic term for crooked card-sharp.</p>
<h3>Blank</h3><p>
  A card that is of no value to a player's hand.</p>
<h3>Blind</h3><p>
  A forced bet that one or more players to the dealer's left must make before any cards are dealt to start the action on the first round of betting.</p>
<h3>Blind Raise</h3><p>
  When a player raises without first looking at his or her cards.</p>
<h3>Blow Back</h3><p>
  To lose back one's profits.</p>
<h3>Bluff</h3><p>
  To bet or raise with a hand that is unlikely to be the best hand.</p>
<h3>Board</h3><p>
  In flop games, the five cards that are turned face up in the center of the table; in Seven-Card Stud, the four cards that are dealt face up to each player.</p>
<h3>Boat</h3><p>
  Another name for full house.</p>
<h3>Bottom Pair</h3><p>
  When you use the lowest card on the flop to make a pair.</p>
<h3>Bounty</h3><p>
  A small amount of cash awarded to a player when he knocks out another player in some tournaments.</p>
<h3>Brick</h3><p>
  A blank.</p>
<h3>Bring-In</h3><p>
  The forced bet made on the first betting round by the player dealt the lowest card showing in Seven-Card Stud and the highest card showing in razz.</p>
<h3>Bring It In</h3><p>
  To start the betting on the first round.</p>
<h3>Broadway</h3><p>
  An ace high straight.</p>
<h3>Brush</h3><p>
  A cardroom employee responsible for managing the seating list.</p>
<h3>Buck</h3><p>
  In all flop games, a small disk used to indicate the dealer, or used to signify the player in the last position if a house dealer is used; a button.</p>
<h3>Bug</h3><p>
  A Joker that can be used to make straights and flushes and can be paired with Aces, but not with any other cards.</p>
<h3>Bullet</h3><p>
  An Ace.</p>
<h3>Bullets</h3><p>
  A pair of Aces.</p>
<h3>Bump</h3><p>
  To raise.</p>
<h3>Buried Pair</h3><p>
  In stud games, a pair in the hole.</p>
<h3>Burn</h3><p>
  To deal off the top card, face down, before dealing out the cards (to prevent cheating); or to set aside a card which has been inadvertently revealed.</p>
<h3>Bust</h3><p>
  A worthless hand that has failed to improve as the player hoped; a busted hand.</p>
<h3>Bust a Player</h3><p>
  To deprive a player of all his chips; in tournament play, to eliminate a player.</p>
<h3>Bust Out</h3><p>
  To be eliminated from a tournament by losing all your chips.</p>
<h3>Busted</h3><p>
  Broke, tapped.</p>
<h3>Busted Flush</h3><p>
  A hand with only four of five cards in a flush.</p>
<h3>Button</h3><p>
  In all flop games, a small disk used to signify the player in the last position if a house dealer is used; a buck.</p>
<h3>Buy-In</h3><p>
  The miniumum amount of money required to sit down in a particular game.</p>
<h3>Cage</h3><p>
  The cashier, where you exchange cash for chips and vice versa.</p>
<h3>Call</h3><p>
  To match, rather than raise, the previous bet.</p>
<h3>Call Cold</h3><p>
  To call a bet and raise at once.</p>
<h3>Calling Station</h3><p>
  A player who invariably calls, and is therefore hard to bluff out.</p>
<h3>Cap</h3><p>
  In limit games, the limit on the number of raises in a round of betting.</p>
<h3>Card Room</h3><p>
  The room or area in a casino where poker is played.</p>
<h3>Case Card</h3><p>
  The last card of a denomination or suit, when the rest have already been seen.</p>
<h3>Case Chips</h3><p>
  A player's last chips.</p>
<h3>Cash In</h3><p>
  To leave the game and convert one's chips to cash, either with the dealer or at the cage.</p>
<h3>Cash Out</h3><p>
  To leave a game and cash in one's chips at the cage.</p>
<h3>Caught Speeding</h3><p>
  Slang for caught bluffing.</p>
<h3>Chase</h3><p>
  To stay in against an apparently stronger hand, usually in the hope of filling a straight or flush.</p>
<h3>Check</h3><p>
  To abstain from betting, reserving the right to call or raise if another player bets. Also another name for a chip.</p>
<h3>Check-Raise</h3><p>
  To check and raise in a betting round.</p>
<h3>Check In The Dark</h3><p>
  To check before looking at the card or cards just dealt.</p>
<h3>Cheese</h3><p>
  A very substandard starting hand.</p>
<h3>Chip Race</h3><p>
  As the limits increase in tournaments, lower denomination chips are taken out of circulation. Rather than rounding odd chips up or down for each player, the players are dealt a card for each odd chip. The player with the highest card is given all the odd chips, which are then colored up.</p>
<h3>Chop</h3><p>
  To return the blinds to the players who posted them and move on to the next hand, if nobody calls the blind.</p>
<h3>Cinch Hand</h3><p>
  An unbeatable hand; nuts.</p>
<h3>Closed Hand</h3><p>
  A hand in which all cards are concealed from the opponents.</p>
<h3>Closed Poker</h3><p>
  Games in which all of the cards are dealt face down.</p>
<h3>Coffee Housing</h3><p>
  An attempt to mislead opponents about one's hand by means of devious speech or behavior.</p>
<h3>Cold</h3><p>
  If a player says his cards have &quot;gone cold,&quot; he's having a bad streak.</p>
<h3>Cold Call</h3><p>
  To call a raise without having already put the initial bet into the pot.</p>
<h3>Cold Deck</h3><p>
  A fixed deck.</p>
<h3>Color Up</h3><p>
  To exchange one's chips for chips of higher value, usually to reduce the number of chips one has on the table.</p>
<h3>Come</h3><p>
  Playing a worthless hand in the hope of improving it is called &quot;playing on the come.&quot;</p>
<h3>Come Hand</h3><p>
  A hand that has not yet been made, requiring one or more cards from the draw to complete it.</p>
<h3>Come Over The Top</h3><p>
  To raise or reraise an opponent's bet.</p>
<h3>Commit Fully</h3><p>
  To put in as many chips as necessary to play your hand to the river, even if they're your case chips.</p>
<h3>Community Cards</h3><p>
  In flop games and similar games, the cards dealt face up in the center of the table that are shared by all active players.</p>
<h3>Connectors</h3><p>
  Consecutive cards which might make a straight.</p>
<h3>Counterfeit</h3><p>
  In Omaha Hi/Lo, when the board pairs your key low card, demoting the value of your hand.</p>
<h3>Cowboy</h3><p>
  Slang for a King.</p>
<h3>Crack</h3><p>
  To beat a powerful hand.</p>
<h3>Crying Call</h3><p>
  A call with a hand you think has a small chance of winning.</p>
<h3>Cut It Up</h3><p>
  To split the pot after a tie.</p>
<h3>Cut The Pot</h3><p>
  To take a percentage of each pot for the casino running the game.</p>
<h3>Dead Card</h3><p>
  A card no longer legally playable.</p>
<h3>Dead Hand</h3><p>
  A hand no longer legally playable, due to some irregularity.</p>
<h3>Dead Money</h3><p>
  Money put into the pot by players who have already folded.</p>
<h3>Dealer's Choice</h3><p>
  A game in which each dealer, in turn, chooses the type of poker to be played.</p>
<h3>Declaration</h3><p>
  In high-low poker, declaring by the use of coins or chips whether one is aiming to win the high or the low end of the pot, or both.</p>
<h3>Declare Games</h3><p>
  Games in which a player must declare the value of his hand in order to claim the pot.</p>
<h3>Deuce</h3><p>
  A two, the lowest ranking card in high poker.</p>
<h3>Deuce to Seven</h3><p>
  Another term for Kansas City Lowball, a two to seven without a flush,being the best hand.</p>
<h3>Dominate</h3><p>
  Said of a starting hand that will almost always beat another starting hand.</p>
<h3>Door Card</h3><p>
  In Seven-Card Stud, the first exposed card in a player's hand.</p>
<h3>Double Belly Buster</h3><p>
  A hand with two inside straight draws.</p>
<h3>Double Gut Shot</h3><p>
  A draw to a broken sequence of cards, in which either of two cards will make the straight.</p>
<h3>Double Through</h3><p>
  Going all-in against an opponent in order to double your stack if you win the hand.</p>
<h3>Down Cards</h3><p>
  Hole cards.</p>
<h3>Down To The Felt</h3><p>
  A player who has lost most of his chips.</p>
<h3>Draw Lowball</h3><p>
  A form of poker in which the lowest hand wins.</p>
<h3>Draw Out</h3><p>
  To improve your hand so that it beats an opponent who had a better hand than yours prior to your draw.</p>
<h3>Draw Poker</h3><p>
  A form of poker in which each player receives five cards and then has the option of discarding one or more of them and receiving new cards in their place.</p>
<h3>Drawing Dead</h3><p>
  Drawing to a hand that cannot possibly win.</p>
<h3>Drawing Hand</h3><p>
  A potentially strong hand requiring a particular card from the draw to make it.</p>
<h3>Driver's Seat</h3><p>
  The player who is making all the betting and thus appears to hold the strongest hand is said to be in the driver's seat.</p>
<h3>Drop</h3><p>
  To fold.</p>
<h3>Early Position</h3><p>
  A position on a round of betting in which you must act before most of the other players.</p>
<h3>Effective Odds</h3><p>
  The ratio of the total amount of money you expect to win if you make your hand to the total amount of bets you will have to call to continue from the present round of betting to the end of the hand.</p>
<h3>Equity</h3><p>
  The value of a particular hand or combination of cards.</p>
<h3>Even Money</h3><p>
  A wager in which you hope to win the same amount as you bet.</p>
<h3>Expectation</h3><p>
  The profit or loss you would expect to make on average over a number of hands.</p>
<h3>Family Pot</h3><p>
  A pot in which most of the players at the table are still involved at the end of the hand.</p>
<h3>Favorite</h3><p>
  A hand that has the best chance of winning.</p>
<h3>Fifth Street</h3><p>
  In flop games, the final round of betting and the fifth community card on the board; in stud games, the fifth card dealt to each player and the third betting round (on the third upcard).</p>
<h3>Fill</h3><p>
  To pull the card one is seeking; to hit.</p>
<h3>Fill Up</h3><p>
  To make a full house.</p>
<h3>Fish</h3><p>
  A poor player; an amateur who is losing a lot of money.</p>
<h3>Fishhooks</h3><p>
  Slang for Jacks.</p>
<h3>Five-Card Draw</h3><p>
  A draw poker game in which the players start with five cards and then may draw to replace them.</p>
<h3>Five-Card Stud</h3><p>
  A stud poker game in which each player gets one concealed card and four exposed cards.</p>
<h3>Flat Call</h3><p>
  To call a bet without raising.</p>
<h3>Flat Limit</h3><p>
  A betting limit in a poker game that does not escalate from one round to the next.</p>
<h3>Flop</h3><p>
  In flop games, the first three community cards, which are turned face up simultaneously and start the second round of betting.</p>
<h3>Flop Games</h3><p>
  A family of poker games played with five community cards. The first three cards, turned face up simultaneously, are called the flop. Popular flop games include Texas Hold 'Em and Omaha.</p>
<h3>Flush</h3><p>
  Five cards of the same suit.</p>
<h3>Flush Draw</h3><p>
  Having four cards of the same suit, and hoping to draw a fifth to make a flush.</p>
<h3>Fold</h3><p>
  To withdraw from the hand rather than bet or raise; to give up.</p>
<h3>Forced Bet</h3><p>
  A required bet to start the action on the first round of a poker hand.</p>
<h3>Four-Flush</h3><p>
  Four cards of the same suit, requiring a fifth to make a flush.</p>
<h3>Four Of A Kind</h3><p>
  Four cards of the same denomination.</p>
<h3>Fourth Street</h3><p>
  In flop games, the fourth card on board and the third round of betting, the turn; in Seven-Card Stud, the fourth card dealt to each player and the second round of betting (on the second upcard).</p>
<h3>Free Card</h3><p>
  A card that a player gets without having to call a bet.</p>
<h3>Free Ride</h3><p>
  To stay in a hand without being forced to bet.</p>
<h3>Freeroll</h3><p>
  A situation in which two players have the same hand, but one of the players has a chance to better his hand.</p>
<h3>Freeze Out</h3><p>
  A game or tournament in which all players start with the same amount and play until one player has won all the chips.</p>
<h3>Full House</h3><p>
  Any three cards of the same denomination, plus any pair of a different denomination.</p>
<h3>G-Note</h3><p>
  A one thousand dollar bill.</p>
<h3>Get The Right Price</h3><p>
  The pot odds are favorable enough for you to justify calling a bet or a raise with a drawing hand.</p>
<h3>Get Full Value</h3><p>
  Betting, raising and re-raising in order to manipulate the size of the pot so that you will be getting maximum pot odds if you win the hand.</p>
<h3>Get There</h3><p>
  To make your hand.</p>
<h3>Give Action</h3><p>
  Betting, calling, raising or re-raising.</p>
<h3>Gut-Shot</h3><p>
  A card drawn to fill an inside straight.</p>
<h3>Gypsy In</h3><p>
  In lowball, to limp in.</p>
<h3>Hand</h3><p>
  A player's best five cards.</p>
<h3>Heads-Up</h3><p>
  A game between just two players, often the climax of a tournament.</p>
<h3>High-Low</h3><p>
  A poker game in which the highest and lowest hands share the pot. Also called High-Low Split.</p>
<h3>Hit</h3><p>
  To pull the card one is seeking; to fill.</p>
<h3>Hit And Run</h3><p>
  A player who has only been at the table a short amount of time and leaves after winning a big pot.</p>
<h3>Hold 'Em</h3><p>
  A form of poker in which players use five community cards in combination with their two hole cards to form the best five-card hand. Also called Texas hold 'em.</p>
<h3>Hole</h3><p>
  The concealed card or cards.</p>
<h3>Hole Card</h3><p>
  A card concealed in a player's hand.</p>
<h3>Home Run Hitter</h3><p>
  A player who makes big plays that require maximum risk.</p>
<h3>Horsing</h3><p>
  Passing a small amount of money to another player after winning a pot; scooting.</p>
<h3>House</h3><p>
  The establishment; the casino or cardroom.</p>
<h3>Hot</h3><p>
  Said of a player on a winning streak.</p>
<h3>Ignorant End</h3><p>
  The low end straight.</p>
<h3>Implied Odds</h3><p>
  The amount of money you expect to win if you make your hand versus the amount of money it will cost you to continue playing.</p>
<h3>In</h3><p>
  A player is &quot;in&quot; if he or she has called all bets.</p>
<h3>In the Air</h3><p>
  When the tournament director instructs the dealers to get the cards in the air, it means to start dealing.</p>
<h3>In The Dark</h3><p>
  To check or bet blind, without looking at your cards.</p>
<h3>Inside Straight</h3><p>
  Four cards requiring one in the middle to fill a straight.</p>
<h3>Insurance</h3><p>
  Selling the actual outcome of the hand for its mathematical equity.</p>
<h3>Isolate</h3><p>
  To raise with the intention of reaching a heads up between yourself and a single other player.</p>
<h3>Jackpot Poker</h3><p>
  A form of poker in which the cardroom offers a jackpot for particularly bad beats. Typically you must have aces full or better.</p>
<h3>Jacks Or Better</h3><p>
  A form of draw poker in which a player needs at least a pair of Jacks to start the betting.</p>
<h3>Jam</h3><p>
  To bet or raise the maximum.</p>
<h3>Jammed Pot</h3><p>
  The pot has been raised the maximum number of times, and may also be multi-way.</p>
<h3>Joker</h3><p>
  The fifty-third card in the deck, used as a wild card or a bug.</p>
<h3>Kansas City Lowball</h3><p>
  A form of lowball poker played for a deuce to seven low.</p>
<h3>Keep Honest</h3><p>
  To call an opponent on the river, even though you believe he has a better hand than you do.</p>
<h3>Key Card</h3><p>
  The one card that will make your hand.</p>
<h3>Key Hand</h3><p>
  In a tournament, the hand that proves to be a turning point, for better or worse.</p>
<h3>Kibitzer</h3><p>
  A non-playing spectator; a railbird.</p>
<h3>Kick It</h3><p>
  To raise.</p>
<h3>Kicker</h3><p>
  The highest unpaired side card.</p>
<h3>Kill</h3><p>
  A kill game is one in which a player may place an extra bet, causing the betting limits to go up for just that hand. The player posting the bet is the &quot;killer,&quot; and the hand is considered a &quot;kill pot.&quot; The player is said to have &quot;killed the pot&quot; for the amount of the kill.</p>
<h3>Knave</h3><p>
  A Jack.</p>
<h3>Late Position</h3><p>
  A position on a round of betting in which you act after most of the other players have acted.</p>
<h3>Lay Down</h3><p>
  To reveal one's hand in a showdown.</p>
<h3>Lay Down Your Hand</h3><p>
  To fold.</p>
<h3>Lay The Odds</h3><p>
  To wager more money on a proposition than you hope to win.</p>
<h3>Lead</h3><p>
  To be the first to enter the pot after the blind.</p>
<h3>Leak</h3><p>
  To lose back part or all of one's winnings through other gambling habits.</p>
<h3>Legitimate Hand</h3><p>
  A strong hand that is not a bluff.</p>
<h3>Limit Poker</h3><p>
  A game with fixed minimum and maximum betting intervals.</p>
<h3>Limp In</h3><p>
  To enter the round by calling a bet rather than raising.</p>
<h3>Limper</h3><p>
  A player who enters the pot for the minimum bet.</p>
<h3>Live Blind</h3><p>
  When the player is allowed to raise even if no one else raises first; straddle.</p>
<h3>Live Card</h3><p>
  In stud games, a card that has not yet been seen in an opponent's hand and is presumed likely to be still in play.</p>
<h3>Live Hand</h3><p>
  A hand that is still eligible to win the pot.</p>
<h3>Live One</h3><p>
  An inexperienced, bad or loose player who apparently has plenty of money to lose; a rich sucker.</p>
<h3>Lock</h3><p>
  A hand that cannot lose; a cinch hand.</p>
<h3>Long Odds</h3><p>
  The odds for an event that has a relatively small chance of occurring.</p>
<h3>Look</h3><p>
  To call the final bet (before the showdown).</p>
<h3>Loose</h3><p>
  Playing more hands than the norm.</p>
<h3>Loose Game</h3><p>
  A game with a lot of players in most pots.</p>
<h3>Lowball</h3><p>
  A form of poker in which the lowest hand wins.</p>
<h3>Main Pot</h3><p>
  When a player goes all-in, that player is only eligible to win the main pot - the pot consisting of the bets they were able to match. Additional bets are placed in a side pot and are contested among the remaining players.</p>
<h3>Make</h3><p>
  To make the deck is to shuffle.</p>
<h3>Make A Move</h3><p>
  To try a bluff.</p>
<h3>Maniac</h3><p>
  A very aggressive player who plays hands that more conservative players would probably not consider.</p>
<h3>Mark</h3><p>
  A sucker.</p>
<h3>Marker</h3><p>
  An IOU.</p>
<h3>Mechanic</h3><p>
  A cheat who manipulates the deck.</p>
<h3>Meet</h3><p>
  To call.</p>
<h3>Middle Pair</h3><p>
  In flop games, a middle pair is made by pairing with the middle card on the flop.</p>
<h3>Middle Position</h3><p>
  A position on a round of betting somewhere in the middle.</p>
<h3>Miss</h3><p>
  To be unable to make your drawing hand when the final cards are dealt.</p>
<h3>Monster</h3><p>
  A hand that is almost certain to win.</p>
<h3>Move In</h3><p>
  To go all-in.</p>
<h3>Muck</h3><p>
  To discard a hand; also the discard pile in which all cards are dead.</p>
<h3>Narrow the Field</h3><p>
  To bet or raise in order to scare off other players whose hands are currently worse than yours, but have the potential to improve.</p>
<h3>Nit</h3><p>
  To bide your time, patiently waiting for a playable hand.</p>
<h3>No-Limit Poker</h3><p>
  A game in which players can bet as much as they have in front of them on any given round.</p>
<h3>Nut Flush</h3><p>
  The best available flush.</p>
<h3>Nuts</h3><p>
  The best possible hand at any point in the game, a cinch hand.</p>
<h3>Odds</h3><p>
  The probability of making a hand versus the probability of not making the hand.</p>
<h3>Offsuit</h3><p>
  Two different suits, used to describe the first two cards.</p>
<h3>Omaha</h3><p>
  A flop game similar to Hold 'Em, but each player is dealt four cards instead of two, and a hand must be made using exactly two pocket cards, plus three from the table.</p>
<h3>On Board</h3><p>
  On the table; in the game.</p>
<h3>On The Come</h3><p>
  A hand that is drawing to a straight or flush.</p>
<h3>On Tilt</h3><p>
  Playing poorly, usually because of becoming emotionally upset.</p>
<h3>One-Gap</h3><p>
  An inside straight.</p>
<h3>Open</h3><p>
  To make the first bet.</p>
<h3>Open-Ended Straight</h3><p>
  Four consecutive cards requiring one at either end to make a straight.</p>
<h3>Open Card</h3><p>
  Exposed card; a card dealt face-up.</p>
<h3>Open Pair</h3><p>
  An exposed pair; a pair of face-up cards.</p>
<h3>Open Poker</h3><p>
  Games where some of the cards are dealt face up.</p>
<h3>Option</h3><p>
  When a player posts a live blind, that player is given the option to raise when their turn comes around, even if no one else has raised; straddle.</p>
<h3>Out</h3><p>
  A card remaining in the deck that could hopefully improve your hand.</p>
<h3>Outdraw</h3><p>
  To beat an opponent by drawing to a better hand.</p>
<h3>Outrun</h3><p>
  Outdraw.</p>
<h3>Overcall</h3><p>
  To call a bet after another player has already called.</p>
<h3>Overcard</h3><p>
  In stud games, a card higher than your opponent's probable pair; in flop games, a card higher than any card on the board.</p>
<h3>Overpair</h3><p>
  In flop games, a wired pair higher than any card on the board.</p>
<h3>Paint Cards</h3><p>
  King, Queen and Jack; face cards; court cards; picture cards.</p>
<h3>Pair</h3><p>
  Two cards of the same denomination.</p>
<h3>Pass</h3><p>
  Fold.</p>
<h3>Pat Hand</h3><p>
  A hand that is played as dealt, without changing a card; usually a straight, flush or full house.</p>
<h3>Pay Off</h3><p>
  To call a bet or raise when you don't think you have the best hand.</p>
<h3>Pay Station</h3><p>
  A player who calls bets and raises much more than is typical; a calling station.</p>
<h3>Picture Cards</h3><p>
  King, Queen and Jack; face cards; court cards; paint cards.</p>
<h3>Pip</h3><p>
  The suit symbols on a non-court card, indicating its rank.</p>
<h3>Play Back</h3><p>
  To raise or re-raise an opponent's bet.</p>
<h3>Play Fast</h3><p>
  Aggressively betting a drawing hand to get full value for it if you make it.</p>
<h3>Play With</h3><p>
  Staying in the hand by betting, calling, raising, or re-raising.</p>
<h3>Playing the Board</h3><p>
  In flop games, if your best five card hand uses the five community cards.</p>
<h3>Pocket</h3><p>
  Another term for hole.</p>
<h3>Pocket Rockets</h3><p>
  A pair of aces in the hole.</p>
<h3>Position</h3><p>
  Your seat in relation to the dealer, and thus your place in the betting order.</p>
<h3>Post</h3><p>
  To post a bet is to place your chips in the pot.</p>
<h3>Pot</h3><p>
  The money or chips in the center of the table.</p>
<h3>Pot Limit</h3><p>
  A game in which the maximum bet is the total of the pot.</p>
<h3>Pot Odds</h3><p>
  The amount of money in the pot versus the amount of money it will cost you to continue in the hand.</p>
<h3>Prop</h3><p>
  Short for proposition player; similar to a shill, but plays with his own money.</p>
<h3>Proposition Player</h3><p>
  A cardroom employee who joins a game with his own money when the game is shorthanded, or to get a game started; similar to a shill.</p>
<h3>Protect A Hand</h3><p>
  To protect a hand is to bet so as to reduce the chances of anyone outdrawing you by getting them to fold.</p>
<h3>Protect Your Cards</h3><p>
  To protect your cards is to place a chip or some other small object on top of them so that they don't accidentally get mucked by the dealer, mixed with another player's discards, or otherwise become dead when you'd like to play them.</p>
<h3>Provider</h3><p>
  A player who makes the game profitable for the other players at the table; a nicer term for a fish.</p>
<h3>Push</h3><p>
  When the hand is finished and a winner is determined, the dealer pushes the chips towards the winner.</p>
<h3>Put Down</h3><p>
  Fold.</p>
<h3>Put Him On</h3><p>
  To guess an opponent's hand and play accordingly.</p>
<h3>Putting On The Heat</h3><p>
  Pressuring your opponents with aggressive betting strategies to get the most value from your hand.</p>
<h3>Quads</h3><p>
  Four of a kind.</p>
<h3>Qualifier</h3><p>
  In high-low, a requirement the hand must meet to be eligible for a portion of the pot.</p>
<h3>Rack</h3><p>
  A plastic tray which holds 100 chips in 5 stacks of 20.</p>
<h3>Rag Off</h3><p>
  To get a card on the river that doesn't help you.</p>
<h3>Ragged Flop</h3><p>
  Flop cards that are of no use to any player's hand.</p>
<h3>Rags</h3><p>
  Worthless cards; blanks.</p>
<h3>Rail</h3><p>
  The sideline at a poker table.</p>
<h3>Railbird</h3><p>
  A non-playing spectator or kibitzer, often used to describe a broke ex-player.</p>
<h3>Rainbow</h3><p>
  Three or four cards of different suits.</p>
<h3>Raise</h3><p>
  To call and increase the previous bet.</p>
<h3>Rake</h3><p>
  Chips taken from the pot by the dealer on behalf of the house.</p>
<h3>Rank</h3><p>
  The value of a card. Each card has a suit and a rank.</p>
<h3>Rap</h3><p>
  To knock the table, indicating a check.</p>
<h3>Razz</h3><p>
  Seven-card stud lowball. Shortened from &quot;razzle dazzle.&quot;</p>
<h3>Read</h3><p>
  To try and determine your opponent's cards or betting strategy.</p>
<h3>Rebuy</h3><p>
  To start again, for an additional entry fee, in tournament play (where permitted).</p>
<h3>Redraw</h3><p>
  A draw to an even better hand when you currently are holding the nuts.</p>
<h3>Represent</h3><p>
  To bet in a way that suggests you are holding a strong hand.</p>
<h3>Re-raise</h3><p>
  To raise a raise.</p>
<h3>Reverse Implied Odds</h3><p>
  The ratio of the amount of money now in the pot to the amount of money you will have to call to continue from the present round to the end of the hand.</p>
<h3>Riffle</h3><p>
  To shuffle; or to fidget with your chips.</p>
<h3>Ring Game</h3><p>
  A non-tournament game.</p>
<h3>River</h3><p>
  In flop games, the last round of betting on the fifth street card; in stud games, the last round of betting on the seventh street card.</p>
<h3>Rock</h3><p>
  A very tight, conservative player.</p>
<h3>Rock Garden</h3><p>
  A table populated with rocks.</p>
<h3>Roll</h3><p>
  To turn a card face-up.</p>
<h3>Rolled Up</h3><p>
  In Seven-Card Stud, three of a kind on third street (the first three cards).</p>
<h3>Rough</h3><p>
  A lowball hand that is not perfect.</p>
<h3>Round of Betting</h3><p>
  The period during which each active player has the right to check, bet or raise. It ends when the last bet or raise has been called by all players still in the hand.</p>
<h3>Rounder</h3><p>
  A professional player who &quot;makes the rounds&quot; of the big poker games in the country.</p>
<h3>Royal Flush</h3><p>
  The best possible poker hand, consisting of the 10 through the Ace, all the same suit.</p>
<h3>Run</h3><p>
  A straight, or a series of good cards.</p>
<h3>Run Over</h3><p>
  Playing aggressively in an attempt to control the other players.</p>
<h3>Runner-Runner</h3><p>
  A hand made on the last two cards.</p>
<h3>Running</h3><p>
  Two needed cards that come as the last two cards dealt.</p>
<h3>Running Bad</h3><p>
  On a losing streak.</p>
<h3>Running Good</h3><p>
  On a winning streak.</p>
<h3>Running Pair</h3><p>
  When the last two cards on the board make a pair.</p>
<h3>Rush</h3><p>
  Several winning hands in a short period of time.</p>
<h3>Sandbag</h3><p>
  To check a strong hand with the intention of raising or re-raising.</p>
<h3>Satellite</h3><p>
  A small-stakes tournament whose winner obtains cheap entry into a bigger tournament.</p>
<h3>Scare Card</h3><p>
  An up card that looks as though it might have made a strong hand.</p>
<h3>School</h3><p>
  The players in a regular game.</p>
<h3>Scoop</h3><p>
  To win the entire pot.</p>
<h3>Scooting</h3><p>
  Passing chips to another player after winning a pot; horsing.</p>
<h3>Seat Charge</h3><p>
  In public cardrooms, an hourly fee for playing poker.</p>
<h3>Seating List</h3><p>
  In most cardrooms, if there is no seat available for you when you arrive, you can put your name on a list to be seated when a seat opens up.</p>
<h3>Second Pair</h3><p>
  In flop games, pairing the second highest card on board.</p>
<h3>See</h3><p>
  To call.</p>
<h3>Semi-Bluff</h3><p>
  To bet with a hand which isn't the best hand, but which has a reasonable chance of improving.</p>
<h3>Set</h3><p>
  Three of a kind; trips (usually applies to a pair in hand and a matching card on board).</p>
<h3>Set You In</h3><p>
  To bet as much as your opponent has left in front of him.</p>
<h3>Seventh Street</h3><p>
  The final betting round on the last card in Seven-Card Stud.</p>
<h3>Shill</h3><p>
  A cardroom employee, often an off-duty dealer, who plays with house money to make up a game.</p>
<h3>Shootout</h3><p>
  A tournament format in which a single player ends up with the entire prize money, or in which play continues at each table until only one player remains.</p>
<h3>Short Odds</h3><p>
  The odds for an event that has a good chance of occurring.</p>
<h3>Short-Stacked</h3><p>
  Having only a small number of chips left.</p>
<h3>Show One, Show All</h3><p>
  A rule that says if a player shows their cards to anyone at the table they can be asked to show everyone else.</p>
<h3>Showdown</h3><p>
  The point at the end of the final round of betting when all the remaining player's cards are turned up to see which player has won the pot.</p>
<h3>Side Card</h3><p>
  An unmatched card which may determine the winner between two otherwise equal hands.</p>
<h3>Side Pot</h3><p>
  A separate pot contested by other players when one player is all-in.</p>
<h3>Sixth Street</h3><p>
  In Seven-Card Stud, the fourth round of betting on the sixth card.</p>
<h3>Skin</h3><p>
  To fix the cards; cheat.</p>
<h3>Slow Play</h3><p>
  Disguising the value of a strong hand by underbetting, to trick an opponent.</p>
<h3>Slowroll</h3><p>
  To reveal one's hand slowly at showdown, one card at a time, to heighten the drama.</p>
<h3>Small Blind</h3><p>
  The smaller of the two compulsory bets in flop games, made by the player in the first postion to the dealer's left.</p>
<h3>Smooth</h3><p>
  The best possible low hand with a particular high card.</p>
<h3>Smooth Call</h3><p>
  To call rather than raise an opponent's bet.</p>
<h3>Snap Off</h3><p>
  To beat another player, often a bluffer, and usually without a powerful hand.</p>
<h3>Speed</h3><p>
  The level of aggressiveness with which you play. Fast play is more aggressive, slow play is more passive.</p>
<h3>Splash Around</h3><p>
  To play more loosely than you should.</p>
<h3>Splash The Pot</h3><p>
  To throw your chips into the pot, instead of placing them in front of you. This makes it difficult for the dealer to determine the amount you bet.</p>
<h3>Split</h3><p>
  A tie.</p>
<h3>Spread</h3><p>
  When a cardroom starts a table for a particular game, it is said to spread that game. If you want to know what games are played in a particular place, you can ask what they spread.</p>
<h3>Spread Limit</h3><p>
  Betting limits in which there is a fixed minimum and maximum bet for each betting round.</p>
<h3>Squeeze</h3><p>
  To look slowly at the extremities of your hole cards, without removing them from the table, to worry your opponents and heighten the drama.</p>
<h3>Stack</h3><p>
  The pile of chips in front of a player.</p>
<h3>Stand Pat</h3><p>
  To decline an opportunity to draw cards.</p>
<h3>Stand-Off</h3><p>
  A tie, in which the players divide the pot equally.</p>

<h3>Stay</h3><p>
  To remain in a hand with a call rather than a raise.</p>
<h3>Steal</h3><p>
  A bluff in late position, attempting to steal the pot from a table of apparently weak hands.</p>
<h3>Steaming</h3><p>
  Playing poorly and wildly, often because the player is emotionally upset.</p>
<h3>Steel Wheel</h3><p>
  In lowball, a straight flush, five high (Ace-2-3-4-5).</p>
<h3>Straddle</h3><p>
  To make a blind raise before the deal; big blind.</p>
<h3>Straight</h3><p>
  Five consecutive cards of mixed suits.</p>
<h3>Straight Flush</h3><p>
  Five consecutive cards of the same suit.</p>
<h3>Streak</h3><p>
  A run of good or bad cards.</p>
<h3>String Bet</h3><p>
  An illegal bet in which a player puts some chips in the pot, then reaches back to his stack for more, without having first verbally stated the full amount of his bet.</p>
<h3>Structure</h3><p>
  The limits set upon the ante, forced bets and subsequent bets and raises in any given game.</p>
<h3>Stuck</h3><p>
  Slang for losing, often a substantial amount of money.</p>
<h3>Stud</h3><p>
  Any form of poker in which the first card or cards are dealt down, or in the hole, followed by several open, or face up, cards.</p>
<h3>Suck Out</h3><p>
  To win a hand by hitting a very weak draw, often with poor pot odds.</p>
<h3>Suited</h3><p>
  Cards of the same suit.</p>
<h3>Sweat</h3><p>
  To watch a player from the rail.</p>
<h3>Sweeten The Pot</h3><p>
  Slang for raise.</p>
<h3>Table</h3><p>
  Refers to the poker table itself, or the collective players in the game.</p>
<h3>Table Cop</h3><p>
  A player who calls with the intention of keeping other players honest.</p>
<h3>Table Stakes</h3><p>
  A poker game in which a player cannot bet more than the money he has on the table.</p>
<h3>Table Talk</h3><p>
  Any discussion at the table of the hand currently underway, especially by players not involved in the pot, and especially any talk that might affect play.</p>
<h3>Take Off A Card</h3><p>
  To call a single bet in order to see one more card.</p>
<h3>Take Off The Gloves</h3><p>
  To use an aggressive betting strategy to bully opponents.</p>
<h3>Take The Odds</h3><p>
  To wager less money on a proposition than you hope to win.</p>
<h3>Tap City</h3><p>
  To go broke.</p>
<h3>Tap Out</h3><p>
  To bet all one's chips.</p>
<h3>Tapped Out</h3><p>
  Broke, busted.</p>
<h3>Tell</h3><p>
  A player's nervous habit or mannerism which might reveal his hand.</p>
<h3>Texas Hold 'Em</h3><p>
  A form of poker in which players use five community cards in combination with their two hole cards to form the best five-card hand. Also called hold 'em.</p>
<h3>Third Pair</h3><p>
  In flop games, pairing the third highest card on board.</p>
<h3>Third Street</h3><p>
  In Seven-Card Stud, the first round of betting on the first three cards.</p>
<h3>Three Flush</h3><p>
  Three cards of the same suit, requiring two more to make a flush.</p>
<h3>Three Of A Kind</h3><p>
  Three cards of the same denomination, with two side cards; trips.</p>
<h3>Throwing A Party</h3><p>
  When several loose or amateur players are making significant monetary contributions to the pot.</p>
<h3>Tight</h3><p>
  A conservative player who only plays strong hands, or playing on fewer hands than the norm.</p>
<h3>Tight Game</h3><p>
  A game with a small number of players in most pots.</p>
<h3>Tilt</h3><p>
  See on tilt.</p>
<h3>To Go</h3><p>
  An amount &quot;to go&quot; is the amount it takes to enter the pot.</p>
<h3>Toke</h3><p>
  A tip to the dealer.</p>
<h3>Top Pair</h3><p>
  In flop games, pairing the highest card on board.</p>
<h3>Trey</h3><p>
  A three.</p>
<h3>Triplets</h3><p>
  Three of a kind.</p>
<h3>Trips</h3><p>
  Slang for triplets; three of a kind.</p>
<h3>Turn</h3><p>
  In flop games, the fourth street card.</p>
<h3>Two Flush</h3><p>
  Two cards of the same suit, requiring three more to make a flush.</p>
<h3>Two Pair</h3><p>
  A hand with two pairs and a kicker.</p>
<h3>Under-Raise</h3><p>
  To raise less than the previous bet; allowed only if a player is going all-in.</p>
<h3>Under The Gun</h3><p>
  The first to bet.</p>
<h3>Underdog</h3><p>
  A hand that does not have the best chance of winning before all the cards are dealt.</p>
<h3>Up Card</h3><p>
  An open card, a card dealt face-up.</p>
<h3>ke Up With A Hand</h3><p>
  To be dealt a hand with winning potential.</p>
<h3>Walk</h3><p>
  To walk is to be away from the table long enough to miss one or more hands.</p>
<h3>Walkers</h3><p>
  Players who walk frequently.</p>
<h3>Wheel</h3><p>
  The lowest hand in lowball, Ace-2-3-4-5; also known as a bicycle.</p>
<h3>Whipsaw</h3><p>
  To raise before, and after, a caller who gets caught in the middle.</p>
<h3>Wild Card</h3><p>
  A card designated as a joker, playable as any value.</p>
<h3>Wired Pair</h3><p>
  A pair in hand.</p>
<h3>World's Fair</h3><p>
  A big hand.</p>
 </td>
  </tr>
</table>
</body>
</html>
