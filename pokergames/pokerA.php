<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
	<?php include("termsINC.php"); ?>
<h3>Ace-High</h3><p>
A five-card hand containing an ace but no pair; beats a king-high, but loses to any pair or above.
<h3>Aces Full</h3><p>
  A full house with aces over any pair.</p>
<h3>Aces Up</h3><p>
  Two pairs, one of which is aces.</p>
<h3>Action</h3><p>
  The betting.</p>
<h3>Active Player</h3><p>
  A player still in the pot.</p>
<h3>Add-On</h3><p>
  The opportunity to buy additional chips in some tournaments.</p>
<h3>Advertise</h3><p>
  To make a bluff with the deliberate intention of being exposed as a loose player.</p>
<h3>All-In</h3><p>
  When a player bets all his or her remaining chips.</p>
<h3>An Ace Working</h3><p>
  An ace in hand.</p>
<h3>Angle</h3><p>
  Any technically legal but ethically dubious way to increase your expectation at a game; a trick.</p>
<h3>Ante</h3><p>
  A token bet required before the start of a hand.</p>
 </td>
  </tr>
</table>
</body>
</html>
