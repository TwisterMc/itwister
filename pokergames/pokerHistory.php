<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
There seem to be differences of opinion on the origin of Poker. Moreover, there seems to be no clear or direct early ancestor of the game. It is more likely that Poker derived its present day form from elements of many different games. The consensus is that because of it's basic principal, its birth is a very old one.
<p>Jonathan H. Green makes one of the earliest written references to Poker in 1834. In his writing, Green mentions rules to what he called the &quot;cheating game,&quot; which was then being played on Mississippi riverboats. He soon realized that his was the first such reference to the game, and since it was not mentioned in the current American Hoyle, he chose to call the game Poker.</p>
<p>The game he described was played with 20 cards, using only the aces, kings, queens, jacks and tens. Two to four people could play, and each was dealt five cards. By the time Green wrote about it, poker had become the number one cheating game on the Mississippi boats, receiving even more action than Three-Card Monte. Most people taken by Three-Card Monte thought the 20-card poker seemed more a legitimate game, and they came back time and time again. It would certainly appear, then, that Poker was developed by the cardsharps.</p>
<p>The origin of the word Poker is also well debated. Most of the dictionaries and game historians say that it comes from an eighteenth-century French game, poque. However, there are other references to pochspiel, which is a German game. In pochspiel, there is an element of bluffing, where players would indicate whether they wanted to pass or open by rapping on the table and saying, &quot;Ich Poche!&quot; Some say it may even have derived come the Hindu word, pukka.</p>
<p>Yet another possible explanation for the word poker, is that it came from a version of an underworld slang word, &quot;poke,&quot; a term used by pickpockets. Cardsharps who used the 20-card cheating game to relieve a sucker from his poke may have used that word among themselves, adding an r to make it &quot;poker.&quot; The thought was that if the sharps used the word &quot;poker&quot; in front of their victims, those wise to the underworld slang would not surmise the change.</p>
<p>There are those who also believe that &quot;poke&quot; probably came from &quot;hocus-pocus&quot;, a term widely used by magicians. The game of Poker later evolved to include 32 cards, and eventually the modern day deck of 52, not counting the two Jokers.</p>
<p>The game of Poker has evolved through the years, through many backroom games to the present day casinos around the world. Its history is rich with famous places and characters. For example, during the Wild West period of United States history, a saloon with a Poker table could be found in just about every town from coast to coast.</p>
<p>Today, Poker is carefully regulated by gambling laws, and saloons have given way to casinos and cardrooms, but Poker is played more than any other card game in the world. It has grown into a sporting event, with competitions and tournaments all around the world. Tournaments take place almost every week of the year somewhere in the world.</p>
<p>If you compare the prizes of major sporting events around the world, you will find that the monetary outcome of any given event in Poker would (pardon the pun) stack up. Poker today is one of the fastest growing, but hardly recognized sporting events. The pinnacle of the poker world, The World Series of Poker, attracts players from all over the world every year to compete for money and titles as the world's top Poker players.</p>
<p>Poker will always be around and will continue to grow and flourish like so many other past times. There will always be a game to play, money to be won, and crowns to be worn.</p>
 </td>
  </tr>
</table>
</body>
</html>
