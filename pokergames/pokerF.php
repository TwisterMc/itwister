<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
<?php include("termsINC.php"); ?>
<h3>Family Pot</h3><p>
  A pot in which most of the players at the table are still involved at the end of the hand.</p>
<h3>Favorite</h3><p>
  A hand that has the best chance of winning.</p>
<h3>Fifth Street</h3><p>
  In flop games, the final round of betting and the fifth community card on the board; in stud games, the fifth card dealt to each player and the third betting round (on the third upcard).</p>
<h3>Fill</h3><p>
  To pull the card one is seeking; to hit.</p>
<h3>Fill Up</h3><p>
  To make a full house.</p>
<h3>Fish</h3><p>
  A poor player; an amateur who is losing a lot of money.</p>
<h3>Fishhooks</h3><p>
  Slang for Jacks.</p>
<h3>Five-Card Draw</h3><p>
  A draw poker game in which the players start with five cards and then may draw to replace them.</p>
<h3>Five-Card Stud</h3><p>
  A stud poker game in which each player gets one concealed card and four exposed cards.</p>
<h3>Flat Call</h3><p>
  To call a bet without raising.</p>
<h3>Flat Limit</h3><p>
  A betting limit in a poker game that does not escalate from one round to the next.</p>
<h3>Flop</h3><p>
  In flop games, the first three community cards, which are turned face up simultaneously and start the second round of betting.</p>
<h3>Flop Games</h3><p>
  A family of poker games played with five community cards. The first three cards, turned face up simultaneously, are called the flop. Popular flop games include Texas Hold 'Em and Omaha.</p>
<h3>Flush</h3><p>
  Five cards of the same suit.</p>
<h3>Flush Draw</h3><p>
  Having four cards of the same suit, and hoping to draw a fifth to make a flush.</p>
<h3>Fold</h3><p>
  To withdraw from the hand rather than bet or raise; to give up.</p>
<h3>Forced Bet</h3><p>
  A required bet to start the action on the first round of a poker hand.</p>
<h3>Four-Flush</h3><p>
  Four cards of the same suit, requiring a fifth to make a flush.</p>
<h3>Four Of A Kind</h3><p>
  Four cards of the same denomination.</p>
<h3>Fourth Street</h3><p>
  In flop games, the fourth card on board and the third round of betting, the turn; in Seven-Card Stud, the fourth card dealt to each player and the second round of betting (on the second upcard).</p>
<h3>Free Card</h3><p>
  A card that a player gets without having to call a bet.</p>
<h3>Free Ride</h3><p>
  To stay in a hand without being forced to bet.</p>
<h3>Freeroll</h3><p>
  A situation in which two players have the same hand, but one of the players has a chance to better his hand.</p>
<h3>Freeze Out</h3><p>
  A game or tournament in which all players start with the same amount and play until one player has won all the chips.</p>
<h3>Full House</h3><p>
  Any three cards of the same denomination, plus any pair of a different denomination.</p>

 </td>
  </tr>
</table>
</body>
</html>
