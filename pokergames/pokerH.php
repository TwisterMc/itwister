<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Hand</h3><p>
  A player's best five cards.</p>
<h3>Heads-Up</h3><p>
  A game between just two players, often the climax of a tournament.</p>
<h3>High-Low</h3><p>
  A poker game in which the highest and lowest hands share the pot. Also called High-Low Split.</p>
<h3>Hit</h3><p>
  To pull the card one is seeking; to fill.</p>
<h3>Hit And Run</h3><p>
  A player who has only been at the table a short amount of time and leaves after winning a big pot.</p>
<h3>Hold 'Em</h3><p>
  A form of poker in which players use five community cards in combination with their two hole cards to form the best five-card hand. Also called Texas hold 'em.</p>
<h3>Hole</h3><p>
  The concealed card or cards.</p>
<h3>Hole Card</h3><p>
  A card concealed in a player's hand.</p>
<h3>Home Run Hitter</h3><p>
  A player who makes big plays that require maximum risk.</p>
<h3>Horsing</h3><p>
  Passing a small amount of money to another player after winning a pot; scooting.</p>
<h3>House</h3><p>
  The establishment; the casino or cardroom.</p>
<h3>Hot</h3><p>
  Said of a player on a winning streak.</p>

 </td>
  </tr>
</table>
</body>
</html>
