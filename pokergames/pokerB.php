<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
<?php include("termsINC.php"); ?>
<h3>Baby</h3><p>
  A small card.</p>
<h3>Back Door Flush (or Straight)</h3><p>
  When the last two cards make a player's hand, even though he or she played on the flop for some other reason.</p>
<h3>Back Into A Hand</h3><p>
  To draw into a hand different from the one you were originally trying to make.</p>
<h3>Bad Beat</h3><p>
  When a strong hand is beaten by a lucky hand; a longshot win.</p>
<h3>Bankroll</h3><p>
  The amount of money you have available to wager.</p>
<h3>Behind</h3><p>
  You're behind if you don't have the best hand before the last cards have been dealt.</p>
<h3>Belly Buster</h3><p>
  A draw to fill an inside straight; a gut shot.</p>
<h3>Bet</h3><p>
  To voluntarily put money or chips into the pot.</p>
<h3>Bet For Value</h3><p>
  Betting in order to raise the amount in the pot, not to make your opponents fold.</p>
<h3>Bet Into</h3><p>
  To bet before a stronger hand, or a player who bet strongly on the previous round.</p>
<h3>Bet The Pot</h3><p>
  To bet the total value of the pot.</p>
<h3>Betting Black</h3><p>
  Betting $100 amounts (black is a common color for $100 chips).</p>
<h3>Betting Green</h3><p>
  Betting $25 amounts (green is a common color for $25 chips).</p>
<h3>Betting Red</h3><p>
  Betting $5 amounts (red is a common color for $5 chips).</p>
<h3>Betting White</h3><p>
  Betting $1 amounts (white is a common color for $1 chips).</p>
<h3>Betting Interval</h3><p>
  The period during which each active player has the right to check, bet or raise; the round of betting. It ends when the last bet or raise has been called by all players still in the hand.</p>
<h3>Bicycle</h3><p>
  The lowest possible hand in lowball: Ace-2-3-4-5. Also called a wheel.</p>
<h3>Big Bet Poker</h3><p>
  Another term for pot-limit and no-limit poker.</p>
<h3>Big Blind</h3><p>
  The forced bet in second position before any cards are dealt. Usually this is a Live Blind, which means that the player in this position can raise if no one else has before the cards are dealt.</p>
<h3>Big Slick</h3><p>
  The Ace-King card combination.</p>
<h3>Black Leg</h3><p>
  Archaic term for crooked card-sharp.</p>
<h3>Blank</h3><p>
  A card that is of no value to a player's hand.</p>
<h3>Blind</h3><p>
  A forced bet that one or more players to the dealer's left must make before any cards are dealt to start the action on the first round of betting.</p>
<h3>Blind Raise</h3><p>
  When a player raises without first looking at his or her cards.</p>
<h3>Blow Back</h3><p>
  To lose back one's profits.</p>
<h3>Bluff</h3><p>
  To bet or raise with a hand that is unlikely to be the best hand.</p>
<h3>Board</h3><p>
  In flop games, the five cards that are turned face up in the center of the table; in Seven-Card Stud, the four cards that are dealt face up to each player.</p>
<h3>Boat</h3><p>
  Another name for full house.</p>
<h3>Bottom Pair</h3><p>
  When you use the lowest card on the flop to make a pair.</p>
<h3>Bounty</h3><p>
  A small amount of cash awarded to a player when he knocks out another player in some tournaments.</p>
<h3>Brick</h3><p>
  A blank.</p>
<h3>Bring-In</h3><p>
  The forced bet made on the first betting round by the player dealt the lowest card showing in Seven-Card Stud and the highest card showing in razz.</p>
<h3>Bring It In</h3><p>
  To start the betting on the first round.</p>
<h3>Broadway</h3><p>
  An ace high straight.</p>
<h3>Brush</h3><p>
  A cardroom employee responsible for managing the seating list.</p>
<h3>Buck</h3><p>
  In all flop games, a small disk used to indicate the dealer, or used to signify the player in the last position if a house dealer is used; a button.</p>
<h3>Bug</h3><p>
  A Joker that can be used to make straights and flushes and can be paired with Aces, but not with any other cards.</p>
<h3>Bullet</h3><p>
  An Ace.</p>
<h3>Bullets</h3><p>
  A pair of Aces.</p>
<h3>Bump</h3><p>
  To raise.</p>
<h3>Buried Pair</h3><p>
  In stud games, a pair in the hole.</p>
<h3>Burn</h3><p>
  To deal off the top card, face down, before dealing out the cards (to prevent cheating); or to set aside a card which has been inadvertently revealed.</p>
<h3>Bust</h3><p>
  A worthless hand that has failed to improve as the player hoped; a busted hand.</p>
<h3>Bust a Player</h3><p>
  To deprive a player of all his chips; in tournament play, to eliminate a player.</p>
<h3>Bust Out</h3><p>
  To be eliminated from a tournament by losing all your chips.</p>
<h3>Busted</h3><p>
  Broke, tapped.</p>
<h3>Busted Flush</h3><p>
  A hand with only four of five cards in a flush.</p>
<h3>Button</h3><p>
  In all flop games, a small disk used to signify the player in the last position if a house dealer is used; a buck.</p>
<h3>Buy-In</h3><p>
  The miniumum amount of money required to sit down in a particular game.</p>
</td>
  </tr>
</table>
</body>
</html>
