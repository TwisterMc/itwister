<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
<h3>POKER HAND RANKS</h3><p>
If you are a beginner, you should first take the time to learn the ranking of poker hands.
<h3>Royal Flush:</h3><p>
  The five highest cards in a suit- ace, king, queen, jack, and ten, all of the same suit. If you get this hand, put on your best poker Face and let the money pour in.</p>
<h3>Straight Flush:</h3><p>
  Any five cards of the same suit that are in consecutive order. Not as grand as a Royal Flush, but still a great hand to get.</p>
<h3>Four of a Kind:</h3><p>
  Four cards of the same number or face. This is still a rare hand to get. Go ahead and bet high, in order for someone to beat you they would have to have a higher 4 of a kind, straight flush, or royal flush.</p>
<h3>Full House:</h3><p>
  Any three cards of the same suit, in addition to any pair of a different denomination. If another player has a full house in hand also, the tie is broken by which ever player has the denomination of the three-card suit.</p>
<h3>Flush:</h3><p>
  Any five non-consecutive cards of the same suit. (Diamonds, hearts, clubs, or spades)</p>
<h3>Straight:</h3><p>
  Any five consecutive cards of a different suit. In this case, the ace can count as a one or an ace. (Ace, two, three, four, five)</p>
<h3>Three of a Kind:</h3><p>
  Three cards of the same denomination. (Ace, ace, ace)</p>
<h3>Two pair:</h3><p>
  Any two cards of the same denomination in addition to another two cards of the same denomination of another suit. (Two, two, &amp; jack, jack)</p>
<h3>One pair:</h3><p>
  Any two cards of the same denomination. (ace, ace)</p>
<h3>High Card:</h3><p>
  The person holding the highest card when no other player has any of the other sets. (Ace beats king etc.)</p>
 </td>
  </tr>
</table>
</body>
</html>
