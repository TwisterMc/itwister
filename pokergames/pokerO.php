<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Odds</h3><p>
  The probability of making a hand versus the probability of not making the hand.</p>
<h3>Offsuit</h3><p>
  Two different suits, used to describe the first two cards.</p>
<h3>Omaha</h3><p>
  A flop game similar to Hold 'Em, but each player is dealt four cards instead of two, and a hand must be made using exactly two pocket cards, plus three from the table.</p>
<h3>On Board</h3><p>
  On the table; in the game.</p>
<h3>On The Come</h3><p>
  A hand that is drawing to a straight or flush.</p>
<h3>On Tilt</h3><p>
  Playing poorly, usually because of becoming emotionally upset.</p>
<h3>One-Gap</h3><p>
  An inside straight.</p>
<h3>Open</h3><p>
  To make the first bet.</p>
<h3>Open-Ended Straight</h3><p>
  Four consecutive cards requiring one at either end to make a straight.</p>
<h3>Open Card</h3><p>
  Exposed card; a card dealt face-up.</p>
<h3>Open Pair</h3><p>
  An exposed pair; a pair of face-up cards.</p>
<h3>Open Poker</h3><p>
  Games where some of the cards are dealt face up.</p>
<h3>Option</h3><p>
  When a player posts a live blind, that player is given the option to raise when their turn comes around, even if no one else has raised; straddle.</p>
<h3>Out</h3><p>
  A card remaining in the deck that could hopefully improve your hand.</p>
<h3>Outdraw</h3><p>
  To beat an opponent by drawing to a better hand.</p>
<h3>Outrun</h3><p>
  Outdraw.</p>
<h3>Overcall</h3><p>
  To call a bet after another player has already called.</p>
<h3>Overcard</h3><p>
  In stud games, a card higher than your opponent's probable pair; in flop games, a card higher than any card on the board.</p>
<h3>Overpair</h3><p>
  In flop games, a wired pair higher than any card on the board.</p>

 </td>
  </tr>
</table>
</body>
</html>
