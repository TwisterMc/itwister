<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Late Position</h3><p>
  A position on a round of betting in which you act after most of the other players have acted.</p>
<h3>Lay Down</h3><p>
  To reveal one's hand in a showdown.</p>
<h3>Lay Down Your Hand</h3><p>
  To fold.</p>
<h3>Lay The Odds</h3><p>
  To wager more money on a proposition than you hope to win.</p>
<h3>Lead</h3><p>
  To be the first to enter the pot after the blind.</p>
<h3>Leak</h3><p>
  To lose back part or all of one's winnings through other gambling habits.</p>
<h3>Legitimate Hand</h3><p>
  A strong hand that is not a bluff.</p>
<h3>Limit Poker</h3><p>
  A game with fixed minimum and maximum betting intervals.</p>
<h3>Limp In</h3><p>
  To enter the round by calling a bet rather than raising.</p>
<h3>Limper</h3><p>
  A player who enters the pot for the minimum bet.</p>
<h3>Live Blind</h3><p>
  When the player is allowed to raise even if no one else raises first; straddle.</p>
<h3>Live Card</h3><p>
  In stud games, a card that has not yet been seen in an opponent's hand and is presumed likely to be still in play.</p>
<h3>Live Hand</h3><p>
  A hand that is still eligible to win the pot.</p>
<h3>Live One</h3><p>
  An inexperienced, bad or loose player who apparently has plenty of money to lose; a rich sucker.</p>
<h3>Lock</h3><p>
  A hand that cannot lose; a cinch hand.</p>
<h3>Long Odds</h3><p>
  The odds for an event that has a relatively small chance of occurring.</p>
<h3>Look</h3><p>
  To call the final bet (before the showdown).</p>
<h3>Loose</h3><p>
  Playing more hands than the norm.</p>
<h3>Loose Game</h3><p>
  A game with a lot of players in most pots.</p>
<h3>Lowball</h3><p>
  A form of poker in which the lowest hand wins.</p>

 </td>
  </tr>
</table>
</body>
</html>
