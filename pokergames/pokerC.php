<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
<?php include("termsINC.php"); ?>
<h3>Cage</h3><p>
  The cashier, where you exchange cash for chips and vice versa.</p>
<h3>Call</h3><p>
  To match, rather than raise, the previous bet.</p>
<h3>Call Cold</h3><p>
  To call a bet and raise at once.</p>
<h3>Calling Station</h3><p>
  A player who invariably calls, and is therefore hard to bluff out.</p>
<h3>Cap</h3><p>
  In limit games, the limit on the number of raises in a round of betting.</p>
<h3>Card Room</h3><p>
  The room or area in a casino where poker is played.</p>
<h3>Case Card</h3><p>
  The last card of a denomination or suit, when the rest have already been seen.</p>
<h3>Case Chips</h3><p>
  A player's last chips.</p>
<h3>Cash In</h3><p>
  To leave the game and convert one's chips to cash, either with the dealer or at the cage.</p>
<h3>Cash Out</h3><p>
  To leave a game and cash in one's chips at the cage.</p>
<h3>Caught Speeding</h3><p>
  Slang for caught bluffing.</p>
<h3>Chase</h3><p>
  To stay in against an apparently stronger hand, usually in the hope of filling a straight or flush.</p>
<h3>Check</h3><p>
  To abstain from betting, reserving the right to call or raise if another player bets. Also another name for a chip.</p>
<h3>Check-Raise</h3><p>
  To check and raise in a betting round.</p>
<h3>Check In The Dark</h3><p>
  To check before looking at the card or cards just dealt.</p>
<h3>Cheese</h3><p>
  A very substandard starting hand.</p>
<h3>Chip Race</h3><p>
  As the limits increase in tournaments, lower denomination chips are taken out of circulation. Rather than rounding odd chips up or down for each player, the players are dealt a card for each odd chip. The player with the highest card is given all the odd chips, which are then colored up.</p>
<h3>Chop</h3><p>
  To return the blinds to the players who posted them and move on to the next hand, if nobody calls the blind.</p>
<h3>Cinch Hand</h3><p>
  An unbeatable hand; nuts.</p>
<h3>Closed Hand</h3><p>
  A hand in which all cards are concealed from the opponents.</p>
<h3>Closed Poker</h3><p>
  Games in which all of the cards are dealt face down.</p>
<h3>Coffee Housing</h3><p>
  An attempt to mislead opponents about one's hand by means of devious speech or behavior.</p>
<h3>Cold</h3><p>
  If a player says his cards have &quot;gone cold,&quot; he's having a bad streak.</p>
<h3>Cold Call</h3><p>
  To call a raise without having already put the initial bet into the pot.</p>
<h3>Cold Deck</h3><p>
  A fixed deck.</p>
<h3>Color Up</h3><p>
  To exchange one's chips for chips of higher value, usually to reduce the number of chips one has on the table.</p>
<h3>Come</h3><p>
  Playing a worthless hand in the hope of improving it is called &quot;playing on the come.&quot;</p>
<h3>Come Hand</h3><p>
  A hand that has not yet been made, requiring one or more cards from the draw to complete it.</p>
<h3>Come Over The Top</h3><p>
  To raise or reraise an opponent's bet.</p>
<h3>Commit Fully</h3><p>
  To put in as many chips as necessary to play your hand to the river, even if they're your case chips.</p>
<h3>Community Cards</h3><p>
  In flop games and similar games, the cards dealt face up in the center of the table that are shared by all active players.</p>
<h3>Connectors</h3><p>
  Consecutive cards which might make a straight.</p>
<h3>Counterfeit</h3><p>
  In Omaha Hi/Lo, when the board pairs your key low card, demoting the value of your hand.</p>
<h3>Cowboy</h3><p>
  Slang for a King.</p>
<h3>Crack</h3><p>
  To beat a powerful hand.</p>
<h3>Crying Call</h3><p>
  A call with a hand you think has a small chance of winning.</p>
<h3>Cut It Up</h3><p>
  To split the pot after a tie.</p>
<h3>Cut The Pot</h3><p>
  To take a percentage of each pot for the casino running the game.</p>

 </td>
  </tr>
</table>
</body>
</html>
