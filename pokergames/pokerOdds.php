<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
POKER ODDS<br>
These are the odds of beeing dealt these hands (based on the first 5 cards).
<p>Royal Flush<br>
  1 in 650,000</p>
<p>Straight Flush<br>
  1 in 72,200</p>
<p>Four of a Kind<br>
  1 in 4,200</p>
<p>Full House<br>
  1 in 700</p>
<p>Flush<br>
  1 in 510</p>
<p>Straight<br>
  1 in 250</p>
<p>3 of a Kind<br>
  1 in 48</p>
<p>2 Pair<br>
  1 in 21</p>
<p>1 Pair<br>
  1 in 2.4</p>
<p>No Pair<br>
  1 in 2<br>
</p>
<p>These are the odds of beeing dealt these hands (7 cards)</p>
<p>Royal Flush<br>
  0.0002%</p>
<p>Straight Flush<br>
  0.0012%</p>
<p>Four of a Kind<br>
  0.0240%</p>
<p>Full House<br>
  0.1441%</p>
<p>Flush<br>
  0.1967%</p>
<p>Straight<br>
  0.3532%</p>
<p>3 of a Kind<br>
  2.1128%</p>
<p>2 Pair<br>
  4.7539%</p>
<p>1 Pair<br>
  42.2569%</p>
<p>Nothing<br>
  50.1570%</p>
<p>No guarantees.</p>
 </td>
  </tr>
</table>
</body>
</html>
