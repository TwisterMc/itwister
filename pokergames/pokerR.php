<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>

<h3>Rack</h3><p>
  A plastic tray which holds 100 chips in 5 stacks of 20.</p>
<h3>Rag Off</h3><p>
  To get a card on the river that doesn't help you.</p>
<h3>Ragged Flop</h3><p>
  Flop cards that are of no use to any player's hand.</p>
<h3>Rags</h3><p>
  Worthless cards; blanks.</p>
<h3>Rail</h3><p>
  The sideline at a poker table.</p>
<h3>Railbird</h3><p>
  A non-playing spectator or kibitzer, often used to describe a broke ex-player.</p>
<h3>Rainbow</h3><p>
  Three or four cards of different suits.</p>
<h3>Raise</h3><p>
  To call and increase the previous bet.</p>
<h3>Rake</h3><p>
  Chips taken from the pot by the dealer on behalf of the house.</p>
<h3>Rank</h3><p>
  The value of a card. Each card has a suit and a rank.</p>
<h3>Rap</h3><p>
  To knock the table, indicating a check.</p>
<h3>Razz</h3><p>
  Seven-card stud lowball. Shortened from &quot;razzle dazzle.&quot;</p>
<h3>Read</h3><p>
  To try and determine your opponent's cards or betting strategy.</p>
<h3>Rebuy</h3><p>
  To start again, for an additional entry fee, in tournament play (where permitted).</p>
<h3>Redraw</h3><p>
  A draw to an even better hand when you currently are holding the nuts.</p>
<h3>Represent</h3><p>
  To bet in a way that suggests you are holding a strong hand.</p>
<h3>Re-raise</h3><p>
  To raise a raise.</p>
<h3>Reverse Implied Odds</h3><p>
  The ratio of the amount of money now in the pot to the amount of money you will have to call to continue from the present round to the end of the hand.</p>
<h3>Riffle</h3><p>
  To shuffle; or to fidget with your chips.</p>
<h3>Ring Game</h3><p>
  A non-tournament game.</p>
<h3>River</h3><p>
  In flop games, the last round of betting on the fifth street card; in stud games, the last round of betting on the seventh street card.</p>
<h3>Rock</h3><p>
  A very tight, conservative player.</p>
<h3>Rock Garden</h3><p>
  A table populated with rocks.</p>
<h3>Roll</h3><p>
  To turn a card face-up.</p>
<h3>Rolled Up</h3><p>
  In Seven-Card Stud, three of a kind on third street (the first three cards).</p>
<h3>Rough</h3><p>
  A lowball hand that is not perfect.</p>
<h3>Round of Betting</h3><p>
  The period during which each active player has the right to check, bet or raise. It ends when the last bet or raise has been called by all players still in the hand.</p>
<h3>Rounder</h3><p>
  A professional player who &quot;makes the rounds&quot; of the big poker games in the country.</p>
<h3>Royal Flush</h3><p>
  The best possible poker hand, consisting of the 10 through the Ace, all the same suit.</p>
<h3>Run</h3><p>
  A straight, or a series of good cards.</p>
<h3>Run Over</h3><p>
  Playing aggressively in an attempt to control the other players.</p>
<h3>Runner-Runner</h3><p>
  A hand made on the last two cards.</p>
<h3>Running</h3><p>
  Two needed cards that come as the last two cards dealt.</p>
<h3>Running Bad</h3><p>
  On a losing streak.</p>
<h3>Running Good</h3><p>
  On a winning streak.</p>
<h3>Running Pair</h3><p>
  When the last two cards on the board make a pair.</p>
<h3>Rush</h3><p>
  Several winning hands in a short period of time.</p>

 </td>
  </tr>
</table>
</body>
</html>
