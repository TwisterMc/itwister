<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Kansas City Lowball</h3><p>
  A form of lowball poker played for a deuce to seven low.</p>
<h3>Keep Honest</h3><p>
  To call an opponent on the river, even though you believe he has a better hand than you do.</p>
<h3>Key Card</h3><p>
  The one card that will make your hand.</p>
<h3>Key Hand</h3><p>
  In a tournament, the hand that proves to be a turning point, for better or worse.</p>
<h3>Kibitzer</h3><p>
  A non-playing spectator; a railbird.</p>
<h3>Kick It</h3><p>
  To raise.</p>
<h3>Kicker</h3><p>
  The highest unpaired side card.</p>
<h3>Kill</h3><p>
  A kill game is one in which a player may place an extra bet, causing the betting limits to go up for just that hand. The player posting the bet is the &quot;killer,&quot; and the hand is considered a &quot;kill pot.&quot; The player is said to have &quot;killed the pot&quot; for the amount of the kill.</p>
<h3>Knave</h3><p>
  A Jack.</p>

 </td>
  </tr>
</table>
</body>
</html>
