<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">


<?php include("termsINC.php"); ?>


<h3>Wake Up With A Hand</h3><p>
  To be dealt a hand with winning potential.</p>
<h3>Walk</h3><p>
  To walk is to be away from the table long enough to miss one or more hands.</p>
<h3>Walkers</h3><p>
  Players who walk frequently.</p>
<h3>Wheel</h3><p>
  The lowest hand in lowball, Ace-2-3-4-5; also known as a bicycle.</p>
<h3>Whipsaw</h3><p>
  To raise before, and after, a caller who gets caught in the middle.</p>
<h3>Wild Card</h3><p>
  A card designated as a joker, playable as any value.</p>
<h3>Wired Pair</h3><p>
  A pair in hand.</p>
<h3>World's Fair</h3><p>
  A big hand.</p>
 </td>
  </tr>
</table>
</body>
</html>
