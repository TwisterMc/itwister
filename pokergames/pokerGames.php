<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">
Draw Poker
<p>To begin, two players at the table make a small bet, or blind wager, before receiving any cards. Each player seated in the game takes a turn at placing such a wager.</p>
<p>Players first receive five cards dealt face down. Players pick up the cards and look at them and then decide if they wish to stay in the game. If so, a bet must now be placed.</p>
<p>Once all bets are called the first round of betting is complete.</p>
<p>Players may now choose to exchange with the dealer any number of cards from their hand. This is known as 'the draw'.</p>
<p>Once the draw is complete a second round of betting takes place.</p>
<p>If all bets are called there is a showdown.</p>
<p>The winner is the player with the highest ranking hand.</p>
<p>If only one player remains (ie. no showdown) they will win the pot without having to expose their cards.</p>
<p>Return to Top<br>
  Seven-Card Stud</p>
<p>Seven-Card Stud is easy for beginners to learn, yet challenging enough to hold the interest of seasoned players. In Seven-Card Stud, each player receives two cards face down and one card face up. The player with the low card opens the betting. Each player in turn must call, raise, or fold. On all subsequent rounds, the player with the best hand open the betting.</p>
<p>Each player is then dealt 3 cards face up with a betting round following each card.</p>
<p>The last card dealt to each remaining player is face down with the final betting round following.</p>
<p>Players remaining in the hand will then show their hands and the winning poker hand will be awarded the pot. Suits are not used in determining the winning hand, and tied hands will split the pot.</p>
<p>Return to Top<br>
  Seven-Card Stud High/Low Split</p>
<p>High/Low Split differs from the high-only game in that the player with the highest card must open the betting.</p>
<p>At the showdown, if a qualifying low hand (five cards of 8 or below with no pair) exists, it will be awarded one half the pot and the best poker hand is awarded the other half.</p>
<p>In the absence of a qualifying low hand, the best poker hand wins the entire pot. A straight or a flush, 8-high or lower, may be both the highest and lowest hand, in which case it wins the whole pot.</p>
<p>Return to Top<br>
  Five-Card Stud</p>
<p>All players place a small wager known as an 'ante'. The dealer then deals each player two cards, one dealt face down, known as the 'hole' card and the other face up.</p>
<p>The player with the lowest ranking up-card must place the opening bet.</p>
<p>Each player to the left of the player making the opening bet must in turn either call the opening bet, raise it or fold.</p>
<p>When this round is complete each player is then dealt another card face up followed by a round of betting. This continues until all players have fice cards, one face down and four face up or until only one player remains.</p>
<p>From the third card onwards, the betting is started by the highest poker hand showing.</p>
<p>All players have five cards in their hand, one down and four up.</p>
<p>If there is a showdown, players show all five cards. The winning hand is the highest ranking five card poker hand.</p>
<p>Return to Top<br>
  Texas Hold 'Em</p>
<p>Texas Hold 'Em is a seven card poker game with simple rules that a beginner can easily learn and begin to play immediately. In spite of the simplicity, it is a fast and complex game that takes skill and practice to master. No wonder it has become one of the most popular forms of poker played today.</p>
<p>Texas Hold 'Em uses a disc called a dealer button to indicate where the cards are to be dealt. Prior to the deal, the two players to the left of the button place live bets called the small and large blinds. It's called blind because it's made prior to seeing any cards. It's called live because it counts as part of any further bets in the first betting round.</p>
<p>The play begins with two cards dealt face-down to each player. Action starts with a betting round beginning with the player to the left of the blind bets and continuing clockwise around the table. Players may bet, check, raise, or fold in turn. The large blind has the privilege of last action and may check or raise the bet.</p>
<p>Three community cards are then dealt face up in the center of the table and another betting round takes place beginning with the player to the left of the dealer button. Another card is dealt face up followed by a round of betting. The fifth and last card is dealt face up and a final round of betting takes place.</p>
<p>Players remaining in the hand will then show their cards and the winning poker hand will be awarded the pot. Any combination of hole cards and community cards may be used to make the best five-card poker hand.</p>
<p>Return to Top<br>
  Omaha</p>
<p>Omaha is a form of Hold 'Em that brings a few variables to the table. First you'll receive four cards face down prior to the flop and you must use two cards from your hand combined with three cards from the board to form your best five-card poker hand. Throw in the 8 or better hi/lo variable and you've got a challenge for poker players at all levels.</p>
<p>Omaha is a nine-card poker game that uses a dealer button and blinds as in Texas Hold 'Em. The play begins with four cards dealt face down to each player. Action starts with the first player to the left of the blinds beginning the betting. Players may bet, check, raise, or fold in turn.</p>
<p>Community cards are then dealt face up in the center of the table in the same manner as in Texas Hold 'Em with betting after each round.</p>
<p>After the final betting round, the players remaining in the hand will then show all their cards. To qualify for a winning low hand, a player must have five cards of 8 or lower with no pair using two from their hand and three from the board. Straights or flushes may be used for the low hand if all the cards are 8 or below.</p>
<p>If there is a qualifying low hand, it splits the pot with the winning high hand. Other wise, the high hand takes the entire pot.</p>
<p>Remember, you must use two cards from your hand and three from the board to make the best five-card poker hand.</p>
<p>Return to Top<br>
  2 Card Manila</p>
<p>2 Card Manila shall be played with a reduced deck of 32 cards, with the maximum number of players being 11. The Ace cannot be used as a loc card to form a straight in all manila games.</p>
<p>11 players x 2 cards = 22</p>
<p>Community + burn cards = 9</p>
<p>Total = 31</p>
<p>The Ace cannot be used as a loc card to form a straight in all manila games.</p>
<p>In 2 Card Manila players are dealt 2 cards one at a time starting with the blind.</p>
<p>Flop comes down one at a time until 5 community cards are face up on the table. There is a burn card between each community card, making a total of 4 burn cards. In 2 Card Manila the 2 hole cards must be used, as well as 3 from the board.</p>
<p>Players are required to either bet or 'check' on each card as it is opened.</p>
<p>In 2 Card Manila an ace may only count high in a straight. For example, A, 7, 8, 9, 10 is NOT a straight. The only time you can use A in a straight is 10, J, Q, K, A</p>
<p>A flush beats a full house in any reduced deck game.</p>
<p>The betting structure for $2 manila is $2, $4, $4, $4, $8 and $5 manila is $5, $10, $10, $10, $20</p>
<p>Only one blind, before cards are dealt. (eg $2), all players can either call the $2, raise to $6 or fold.</p>
<p>After all bets are called, a card is burnt, and the second community card is dealt. Players can either, check, bet ($4) or fold. same thing up to 5 cards. The bet on the fifth card is now $8</p>
<p>The amount you can raise on the first round is double the blind and added onto the blind, so in a $2 game the raise would be $4 making the total to call $6. on a $5 blind, the raise is $10, making it $15 in total to call. All subsequent rounds, the raise is only double; eg. The bet is $4, first raise to make it $8, second raise to $12. Only 3 raises per round permitted when there are more than 2 players in the game.</p>
<p>At the showdown players use their two hole cards and any three cards from the five community cards in the center of the table to form a five card poker hand.<br>
  3 Card Manila</p>
<p>3 Card Manila is played the same way as 2 Card Manila except each player initially receives three cards and the deck may include 6's.</p>
<p>At the showdown players use any two of their three hole cards with any three cards from the five community cards to form a five card poker hand.</p>
<p>Only One Blind</p>
<p>In 3 Card Manila players are dealt 3 cards one at a time starting with the blind. (Canberra differs here by giving first 2 cards at a time then the third card one at a time.)</p>
<p>Flop comes down one at a time until 5 community cards are face up on the table. There is a burn card between each community card, making a total of 4 burn cards.</p>
<p>In 3 Card Manila 2 of the 3 hole cards must be used, as well as 3 from the board</p>
<p>In 3 Card Manila an ace may only count high in a straight. eg. A, 7, 8, 9, 10 is NOT a straight. The only time you can use A in a straight is 10, J, Q, K, A</p>
<p>3 Card Manila shall be played with a reduced deck of 36 cards, with the maximum number of players being 9.</p>
<p>9 players x 3 cards = 27</p>
<p>Community + burn cards = 9</p>
<p>Total = 36</p>
<p>A flush beats a full house in any reduced deck game.</p>
<p>Betting Structures:</p>
<p>We only deal limit manila in Melbourne, so here are the structures.</p>
<p>The betting structure for $2 manila is $2, $4, $4, $4, $8 and $5 manila is $5, $10, $10, $10, $20</p>
<p>Only one blind, before cards are dealt. (eg $2), all players can either call the $2, raise to $6 or fold.</p>
<p>after all bets are called, a card is burnt, and the second community card is dealt. Players can either, check, bet ($4) or fold. same thing up to 5 cards. The bet on the fifth card is now $8</p>
<p>The amount you can raise on the first round is double the blind and added onto the blind, so in a $2 game the raise would be $4 making the total to call $6. on a $5 blind, the raise is $10, making it $15 in total to call. All subsequent rounds, the raise is only double; eg. The bet is $4, first raise to make it $8, second raise to $12. Only 3 raises per round permitted when there are more than 2 players in the game.<br>
  Omaha 8/B or Omaha Hi/Lo<br>
  -By Jan Fisher</p>
<p>As in any dealer-button game, a random draw will determine the position to start with the button.</p>
<p>Two blinds will be posted to the left of the button. The small blind will be equal to one-half the small bet, the big blind will equal the small bet.</p>
<p>Both blinds are live and may raise the pot.</p>
<p>In the showdown, the player must use two cards from his hand and three from the board exactly. Player may use different cards for high and low.</p>
<p>In order for there to be a low, there must be a combination of two cards from the players hand and three from the board with a denomination of eight or smaller. Ace is low for this purpose. If there is no low, high will get the entire pot.</p>
<p>A wheel (A-2-3-4-5) is the best possible low and neither straights nor flushes count against you for low.</p>
<p>If a player misses his blind or blinds, he may wait until the big blind to post to come back in. Otherwise, he must post the missed blinds. Only the big blind is live.</p>
<p>A player who has posted the blind who is not present to act on his hand will have his hand killed and will not get his blind back.</p>
<p>Winning called hand must show all four cards to be awarded any part of the pot.</p>
<p>In the event of ties, the pot will be split.</p>
<p>Odd chip will go left of the button on both high and low pot odd chip.<br>
</p>
<p>&nbsp;</p>
<p>Ace-High<br>
  A five-card hand containing an ace but no pair; beats a king-high, but loses to any pair or above.</p>
<p>Aces Full<br>
  A full house with aces over any pair.</p>
<p>Aces Up<br>
  Two pairs, one of which is aces.</p>
<p>Action<br>
  The betting.</p>
<p>Active Player<br>
  A player still in the pot.</p>
<p>Add-On<br>
  The opportunity to buy additional chips in some tournaments.</p>
<p>Advertise<br>
  To make a bluff with the deliberate intention of being exposed as a loose player.</p>
<p>All-In<br>
  When a player bets all his or her remaining chips.</p>
<p>An Ace Working<br>
  An ace in hand.</p>
<p>Angle<br>
  Any technically legal but ethically dubious way to increase your expectation at a game; a trick.</p>
<p>Ante<br>
  A token bet required before the start of a hand.</p>
<p>Baby<br>
  A small card.</p>
<p>Back Door Flush (or Straight)<br>
  When the last two cards make a player's hand, even though he or she played on the flop for some other reason.</p>
<p>Back Into A Hand<br>
  To draw into a hand different from the one you were originally trying to make.</p>
<p>Bad Beat<br>
  When a strong hand is beaten by a lucky hand; a longshot win.</p>
<p>Bankroll<br>
  The amount of money you have available to wager.</p>
<p>Behind<br>
  You're behind if you don't have the best hand before the last cards have been dealt.</p>
<p>Belly Buster<br>
  A draw to fill an inside straight; a gut shot.</p>
<p>Bet<br>
  To voluntarily put money or chips into the pot.</p>
<p>Bet For Value<br>
  Betting in order to raise the amount in the pot, not to make your opponents fold.</p>
<p>Bet Into<br>
  To bet before a stronger hand, or a player who bet strongly on the previous round.</p>
<p>Bet The Pot<br>
  To bet the total value of the pot.</p>
<p>Betting Black<br>
  Betting $100 amounts (black is a common color for $100 chips).</p>
<p>Betting Green<br>
  Betting $25 amounts (green is a common color for $25 chips).</p>
<p>Betting Red<br>
  Betting $5 amounts (red is a common color for $5 chips).</p>
<p>Betting White<br>
  Betting $1 amounts (white is a common color for $1 chips).</p>
<p>Betting Interval<br>
  The period during which each active player has the right to check, bet or raise; the round of betting. It ends when the last bet or raise has been called by all players still in the hand.</p>
<p>Bicycle<br>
  The lowest possible hand in lowball: Ace-2-3-4-5. Also called a wheel.</p>
<p>Big Bet Poker<br>
  Another term for pot-limit and no-limit poker.</p>
<p>Big Blind<br>
  The forced bet in second position before any cards are dealt. Usually this is a Live Blind, which means that the player in this position can raise if no one else has before the cards are dealt.</p>
<p>Big Slick<br>
  The Ace-King card combination.</p>
<p>Black Leg<br>
  Archaic term for crooked card-sharp.</p>
<p>Blank<br>
  A card that is of no value to a player's hand.</p>
<p>Blind<br>
  A forced bet that one or more players to the dealer's left must make before any cards are dealt to start the action on the first round of betting.</p>
<p>Blind Raise<br>
  When a player raises without first looking at his or her cards.</p>
<p>Blow Back<br>
  To lose back one's profits.</p>
<p>Bluff<br>
  To bet or raise with a hand that is unlikely to be the best hand.</p>
<p>Board<br>
  In flop games, the five cards that are turned face up in the center of the table; in Seven-Card Stud, the four cards that are dealt face up to each player.</p>
<p>Boat<br>
  Another name for full house.</p>
<p>Bottom Pair<br>
  When you use the lowest card on the flop to make a pair.</p>
<p>Bounty<br>
  A small amount of cash awarded to a player when he knocks out another player in some tournaments.</p>
<p>Brick<br>
  A blank.</p>
<p>Bring-In<br>
  The forced bet made on the first betting round by the player dealt the lowest card showing in Seven-Card Stud and the highest card showing in razz.</p>
<p>Bring It In<br>
  To start the betting on the first round.</p>
<p>Broadway<br>
  An ace high straight.</p>
<p>Brush<br>
  A cardroom employee responsible for managing the seating list.</p>
<p>Buck<br>
  In all flop games, a small disk used to indicate the dealer, or used to signify the player in the last position if a house dealer is used; a button.</p>
<p>Bug<br>
  A Joker that can be used to make straights and flushes and can be paired with Aces, but not with any other cards.</p>
<p>Bullet<br>
  An Ace.</p>
<p>Bullets<br>
  A pair of Aces.</p>
<p>Bump<br>
  To raise.</p>
<p>Buried Pair<br>
  In stud games, a pair in the hole.</p>
<p>Burn<br>
  To deal off the top card, face down, before dealing out the cards (to prevent cheating); or to set aside a card which has been inadvertently revealed.</p>
<p>Bust<br>
  A worthless hand that has failed to improve as the player hoped; a busted hand.</p>
<p>Bust a Player<br>
  To deprive a player of all his chips; in tournament play, to eliminate a player.</p>
<p>Bust Out<br>
  To be eliminated from a tournament by losing all your chips.</p>
<p>Busted<br>
  Broke, tapped.</p>
<p>Busted Flush<br>
  A hand with only four of five cards in a flush.</p>
<p>Button<br>
  In all flop games, a small disk used to signify the player in the last position if a house dealer is used; a buck.</p>
<p>Buy-In<br>
  The miniumum amount of money required to sit down in a particular game.</p>
<p>Cage<br>
  The cashier, where you exchange cash for chips and vice versa.</p>
<p>Call<br>
  To match, rather than raise, the previous bet.</p>
<p>Call Cold<br>
  To call a bet and raise at once.</p>
<p>Calling Station<br>
  A player who invariably calls, and is therefore hard to bluff out.</p>
<p>Cap<br>
  In limit games, the limit on the number of raises in a round of betting.</p>
<p>Card Room<br>
  The room or area in a casino where poker is played.</p>
<p>Case Card<br>
  The last card of a denomination or suit, when the rest have already been seen.</p>
<p>Case Chips<br>
  A player's last chips.</p>
<p>Cash In<br>
  To leave the game and convert one's chips to cash, either with the dealer or at the cage.</p>
<p>Cash Out<br>
  To leave a game and cash in one's chips at the cage.</p>
<p>Caught Speeding<br>
  Slang for caught bluffing.</p>
<p>Chase<br>
  To stay in against an apparently stronger hand, usually in the hope of filling a straight or flush.</p>
<p>Check<br>
  To abstain from betting, reserving the right to call or raise if another player bets. Also another name for a chip.</p>
<p>Check-Raise<br>
  To check and raise in a betting round.</p>
<p>Check In The Dark<br>
  To check before looking at the card or cards just dealt.</p>
<p>Cheese<br>
  A very substandard starting hand.</p>
<p>Chip Race<br>
  As the limits increase in tournaments, lower denomination chips are taken out of circulation. Rather than rounding odd chips up or down for each player, the players are dealt a card for each odd chip. The player with the highest card is given all the odd chips, which are then colored up.</p>
<p>Chop<br>
  To return the blinds to the players who posted them and move on to the next hand, if nobody calls the blind.</p>
<p>Cinch Hand<br>
  An unbeatable hand; nuts.</p>
<p>Closed Hand<br>
  A hand in which all cards are concealed from the opponents.</p>
<p>Closed Poker<br>
  Games in which all of the cards are dealt face down.</p>
<p>Coffee Housing<br>
  An attempt to mislead opponents about one's hand by means of devious speech or behavior.</p>
<p>Cold<br>
  If a player says his cards have &quot;gone cold,&quot; he's having a bad streak.</p>
<p>Cold Call<br>
  To call a raise without having already put the initial bet into the pot.</p>
<p>Cold Deck<br>
  A fixed deck.</p>
<p>Color Up<br>
  To exchange one's chips for chips of higher value, usually to reduce the number of chips one has on the table.</p>
<p>Come<br>
  Playing a worthless hand in the hope of improving it is called &quot;playing on the come.&quot;</p>
<p>Come Hand<br>
  A hand that has not yet been made, requiring one or more cards from the draw to complete it.</p>
<p>Come Over The Top<br>
  To raise or reraise an opponent's bet.</p>
<p>Commit Fully<br>
  To put in as many chips as necessary to play your hand to the river, even if they're your case chips.</p>
<p>Community Cards<br>
  In flop games and similar games, the cards dealt face up in the center of the table that are shared by all active players.</p>
<p>Connectors<br>
  Consecutive cards which might make a straight.</p>
<p>Counterfeit<br>
  In Omaha Hi/Lo, when the board pairs your key low card, demoting the value of your hand.</p>
<p>Cowboy<br>
  Slang for a King.</p>
<p>Crack<br>
  To beat a powerful hand.</p>
<p>Crying Call<br>
  A call with a hand you think has a small chance of winning.</p>
<p>Cut It Up<br>
  To split the pot after a tie.</p>
<p>Cut The Pot<br>
  To take a percentage of each pot for the casino running the game.</p>
<p>Dead Card<br>
  A card no longer legally playable.</p>
<p>Dead Hand<br>
  A hand no longer legally playable, due to some irregularity.</p>
<p>Dead Money<br>
  Money put into the pot by players who have already folded.</p>
<p>Dealer's Choice<br>
  A game in which each dealer, in turn, chooses the type of poker to be played.</p>
<p>Declaration<br>
  In high-low poker, declaring by the use of coins or chips whether one is aiming to win the high or the low end of the pot, or both.</p>
<p>Declare Games<br>
  Games in which a player must declare the value of his hand in order to claim the pot.</p>
<p>Deuce<br>
  A two, the lowest ranking card in high poker.</p>
<p>Deuce to Seven<br>
  Another term for Kansas City Lowball, a two to seven without a flush,being the best hand.</p>
<p>Dominate<br>
  Said of a starting hand that will almost always beat another starting hand.</p>
<p>Door Card<br>
  In Seven-Card Stud, the first exposed card in a player's hand.</p>
<p>Double Belly Buster<br>
  A hand with two inside straight draws.</p>
<p>Double Gut Shot<br>
  A draw to a broken sequence of cards, in which either of two cards will make the straight.</p>
<p>Double Through<br>
  Going all-in against an opponent in order to double your stack if you win the hand.</p>
<p>Down Cards<br>
  Hole cards.</p>
<p>Down To The Felt<br>
  A player who has lost most of his chips.</p>
<p>Draw Lowball<br>
  A form of poker in which the lowest hand wins.</p>
<p>Draw Out<br>
  To improve your hand so that it beats an opponent who had a better hand than yours prior to your draw.</p>
<p>Draw Poker<br>
  A form of poker in which each player receives five cards and then has the option of discarding one or more of them and receiving new cards in their place.</p>
<p>Drawing Dead<br>
  Drawing to a hand that cannot possibly win.</p>
<p>Drawing Hand<br>
  A potentially strong hand requiring a particular card from the draw to make it.</p>
<p>Driver's Seat<br>
  The player who is making all the betting and thus appears to hold the strongest hand is said to be in the driver's seat.</p>
<p>Drop<br>
  To fold.</p>
<p>Early Position<br>
  A position on a round of betting in which you must act before most of the other players.</p>
<p>Effective Odds<br>
  The ratio of the total amount of money you expect to win if you make your hand to the total amount of bets you will have to call to continue from the present round of betting to the end of the hand.</p>
<p>Equity<br>
  The value of a particular hand or combination of cards.</p>
<p>Even Money<br>
  A wager in which you hope to win the same amount as you bet.</p>
<p>Expectation<br>
  The profit or loss you would expect to make on average over a number of hands.</p>
<p>Family Pot<br>
  A pot in which most of the players at the table are still involved at the end of the hand.</p>
<p>Favorite<br>
  A hand that has the best chance of winning.</p>
<p>Fifth Street<br>
  In flop games, the final round of betting and the fifth community card on the board; in stud games, the fifth card dealt to each player and the third betting round (on the third upcard).</p>
<p>Fill<br>
  To pull the card one is seeking; to hit.</p>
<p>Fill Up<br>
  To make a full house.</p>
<p>Fish<br>
  A poor player; an amateur who is losing a lot of money.</p>
<p>Fishhooks<br>
  Slang for Jacks.</p>
<p>Five-Card Draw<br>
  A draw poker game in which the players start with five cards and then may draw to replace them.</p>
<p>Five-Card Stud<br>
  A stud poker game in which each player gets one concealed card and four exposed cards.</p>
<p>Flat Call<br>
  To call a bet without raising.</p>
<p>Flat Limit<br>
  A betting limit in a poker game that does not escalate from one round to the next.</p>
<p>Flop<br>
  In flop games, the first three community cards, which are turned face up simultaneously and start the second round of betting.</p>
<p>Flop Games<br>
  A family of poker games played with five community cards. The first three cards, turned face up simultaneously, are called the flop. Popular flop games include Texas Hold 'Em and Omaha.</p>
<p>Flush<br>
  Five cards of the same suit.</p>
<p>Flush Draw<br>
  Having four cards of the same suit, and hoping to draw a fifth to make a flush.</p>
<p>Fold<br>
  To withdraw from the hand rather than bet or raise; to give up.</p>
<p>Forced Bet<br>
  A required bet to start the action on the first round of a poker hand.</p>
<p>Four-Flush<br>
  Four cards of the same suit, requiring a fifth to make a flush.</p>
<p>Four Of A Kind<br>
  Four cards of the same denomination.</p>
<p>Fourth Street<br>
  In flop games, the fourth card on board and the third round of betting, the turn; in Seven-Card Stud, the fourth card dealt to each player and the second round of betting (on the second upcard).</p>
<p>Free Card<br>
  A card that a player gets without having to call a bet.</p>
<p>Free Ride<br>
  To stay in a hand without being forced to bet.</p>
<p>Freeroll<br>
  A situation in which two players have the same hand, but one of the players has a chance to better his hand.</p>
<p>Freeze Out<br>
  A game or tournament in which all players start with the same amount and play until one player has won all the chips.</p>
<p>Full House<br>
  Any three cards of the same denomination, plus any pair of a different denomination.</p>
<p>G-Note<br>
  A one thousand dollar bill.</p>
<p>Get The Right Price<br>
  The pot odds are favorable enough for you to justify calling a bet or a raise with a drawing hand.</p>
<p>Get Full Value<br>
  Betting, raising and re-raising in order to manipulate the size of the pot so that you will be getting maximum pot odds if you win the hand.</p>
<p>Get There<br>
  To make your hand.</p>
<p>Give Action<br>
  Betting, calling, raising or re-raising.</p>
<p>Gut-Shot<br>
  A card drawn to fill an inside straight.</p>
<p>Gypsy In<br>
  In lowball, to limp in.</p>
<p>Hand<br>
  A player's best five cards.</p>
<p>Heads-Up<br>
  A game between just two players, often the climax of a tournament.</p>
<p>High-Low<br>
  A poker game in which the highest and lowest hands share the pot. Also called High-Low Split.</p>
<p>Hit<br>
  To pull the card one is seeking; to fill.</p>
<p>Hit And Run<br>
  A player who has only been at the table a short amount of time and leaves after winning a big pot.</p>
<p>Hold 'Em<br>
  A form of poker in which players use five community cards in combination with their two hole cards to form the best five-card hand. Also called Texas hold 'em.</p>
<p>Hole<br>
  The concealed card or cards.</p>
<p>Hole Card<br>
  A card concealed in a player's hand.</p>
<p>Home Run Hitter<br>
  A player who makes big plays that require maximum risk.</p>
<p>Horsing<br>
  Passing a small amount of money to another player after winning a pot; scooting.</p>
<p>House<br>
  The establishment; the casino or cardroom.</p>
<p>Hot<br>
  Said of a player on a winning streak.</p>
<p>Ignorant End<br>
  The low end straight.</p>
<p>Implied Odds<br>
  The amount of money you expect to win if you make your hand versus the amount of money it will cost you to continue playing.</p>
<p>In<br>
  A player is &quot;in&quot; if he or she has called all bets.</p>
<p>In the Air<br>
  When the tournament director instructs the dealers to get the cards in the air, it means to start dealing.</p>
<p>In The Dark<br>
  To check or bet blind, without looking at your cards.</p>
<p>Inside Straight<br>
  Four cards requiring one in the middle to fill a straight.</p>
<p>Insurance<br>
  Selling the actual outcome of the hand for its mathematical equity.</p>
<p>Isolate<br>
  To raise with the intention of reaching a heads up between yourself and a single other player.</p>
<p>Jackpot Poker<br>
  A form of poker in which the cardroom offers a jackpot for particularly bad beats. Typically you must have aces full or better.</p>
<p>Jacks Or Better<br>
  A form of draw poker in which a player needs at least a pair of Jacks to start the betting.</p>
<p>Jam<br>
  To bet or raise the maximum.</p>
<p>Jammed Pot<br>
  The pot has been raised the maximum number of times, and may also be multi-way.</p>
<p>Joker<br>
  The fifty-third card in the deck, used as a wild card or a bug.</p>
<p>Kansas City Lowball<br>
  A form of lowball poker played for a deuce to seven low.</p>
<p>Keep Honest<br>
  To call an opponent on the river, even though you believe he has a better hand than you do.</p>
<p>Key Card<br>
  The one card that will make your hand.</p>
<p>Key Hand<br>
  In a tournament, the hand that proves to be a turning point, for better or worse.</p>
<p>Kibitzer<br>
  A non-playing spectator; a railbird.</p>
<p>Kick It<br>
  To raise.</p>
<p>Kicker<br>
  The highest unpaired side card.</p>
<p>Kill<br>
  A kill game is one in which a player may place an extra bet, causing the betting limits to go up for just that hand. The player posting the bet is the &quot;killer,&quot; and the hand is considered a &quot;kill pot.&quot; The player is said to have &quot;killed the pot&quot; for the amount of the kill.</p>
<p>Knave<br>
  A Jack.</p>
<p>Late Position<br>
  A position on a round of betting in which you act after most of the other players have acted.</p>
<p>Lay Down<br>
  To reveal one's hand in a showdown.</p>
<p>Lay Down Your Hand<br>
  To fold.</p>
<p>Lay The Odds<br>
  To wager more money on a proposition than you hope to win.</p>
<p>Lead<br>
  To be the first to enter the pot after the blind.</p>
<p>Leak<br>
  To lose back part or all of one's winnings through other gambling habits.</p>
<p>Legitimate Hand<br>
  A strong hand that is not a bluff.</p>
<p>Limit Poker<br>
  A game with fixed minimum and maximum betting intervals.</p>
<p>Limp In<br>
  To enter the round by calling a bet rather than raising.</p>
<p>Limper<br>
  A player who enters the pot for the minimum bet.</p>
<p>Live Blind<br>
  When the player is allowed to raise even if no one else raises first; straddle.</p>
<p>Live Card<br>
  In stud games, a card that has not yet been seen in an opponent's hand and is presumed likely to be still in play.</p>
<p>Live Hand<br>
  A hand that is still eligible to win the pot.</p>
<p>Live One<br>
  An inexperienced, bad or loose player who apparently has plenty of money to lose; a rich sucker.</p>
<p>Lock<br>
  A hand that cannot lose; a cinch hand.</p>
<p>Long Odds<br>
  The odds for an event that has a relatively small chance of occurring.</p>
<p>Look<br>
  To call the final bet (before the showdown).</p>
<p>Loose<br>
  Playing more hands than the norm.</p>
<p>Loose Game<br>
  A game with a lot of players in most pots.</p>
<p>Lowball<br>
  A form of poker in which the lowest hand wins.</p>
<p>Main Pot<br>
  When a player goes all-in, that player is only eligible to win the main pot - the pot consisting of the bets they were able to match. Additional bets are placed in a side pot and are contested among the remaining players.</p>
<p>Make<br>
  To make the deck is to shuffle.</p>
<p>Make A Move<br>
  To try a bluff.</p>
<p>Maniac<br>
  A very aggressive player who plays hands that more conservative players would probably not consider.</p>
<p>Mark<br>
  A sucker.</p>
<p>Marker<br>
  An IOU.</p>
<p>Mechanic<br>
  A cheat who manipulates the deck.</p>
<p>Meet<br>
  To call.</p>
<p>Middle Pair<br>
  In flop games, a middle pair is made by pairing with the middle card on the flop.</p>
<p>Middle Position<br>
  A position on a round of betting somewhere in the middle.</p>
<p>Miss<br>
  To be unable to make your drawing hand when the final cards are dealt.</p>
<p>Monster<br>
  A hand that is almost certain to win.</p>
<p>Move In<br>
  To go all-in.</p>
<p>Muck<br>
  To discard a hand; also the discard pile in which all cards are dead.</p>
<p>Narrow the Field<br>
  To bet or raise in order to scare off other players whose hands are currently worse than yours, but have the potential to improve.</p>
<p>Nit<br>
  To bide your time, patiently waiting for a playable hand.</p>
<p>No-Limit Poker<br>
  A game in which players can bet as much as they have in front of them on any given round.</p>
<p>Nut Flush<br>
  The best available flush.</p>
<p>Nuts<br>
  The best possible hand at any point in the game, a cinch hand.</p>
<p>Odds<br>
  The probability of making a hand versus the probability of not making the hand.</p>
<p>Offsuit<br>
  Two different suits, used to describe the first two cards.</p>
<p>Omaha<br>
  A flop game similar to Hold 'Em, but each player is dealt four cards instead of two, and a hand must be made using exactly two pocket cards, plus three from the table.</p>
<p>On Board<br>
  On the table; in the game.</p>
<p>On The Come<br>
  A hand that is drawing to a straight or flush.</p>
<p>On Tilt<br>
  Playing poorly, usually because of becoming emotionally upset.</p>
<p>One-Gap<br>
  An inside straight.</p>
<p>Open<br>
  To make the first bet.</p>
<p>Open-Ended Straight<br>
  Four consecutive cards requiring one at either end to make a straight.</p>
<p>Open Card<br>
  Exposed card; a card dealt face-up.</p>
<p>Open Pair<br>
  An exposed pair; a pair of face-up cards.</p>
<p>Open Poker<br>
  Games where some of the cards are dealt face up.</p>
<p>Option<br>
  When a player posts a live blind, that player is given the option to raise when their turn comes around, even if no one else has raised; straddle.</p>
<p>Out<br>
  A card remaining in the deck that could hopefully improve your hand.</p>
<p>Outdraw<br>
  To beat an opponent by drawing to a better hand.</p>
<p>Outrun<br>
  Outdraw.</p>
<p>Overcall<br>
  To call a bet after another player has already called.</p>
<p>Overcard<br>
  In stud games, a card higher than your opponent's probable pair; in flop games, a card higher than any card on the board.</p>
<p>Overpair<br>
  In flop games, a wired pair higher than any card on the board.</p>
<p>Paint Cards<br>
  King, Queen and Jack; face cards; court cards; picture cards.</p>
<p>Pair<br>
  Two cards of the same denomination.</p>
<p>Pass<br>
  Fold.</p>
<p>Pat Hand<br>
  A hand that is played as dealt, without changing a card; usually a straight, flush or full house.</p>
<p>Pay Off<br>
  To call a bet or raise when you don't think you have the best hand.</p>
<p>Pay Station<br>
  A player who calls bets and raises much more than is typical; a calling station.</p>
<p>Picture Cards<br>
  King, Queen and Jack; face cards; court cards; paint cards.</p>
<p>Pip<br>
  The suit symbols on a non-court card, indicating its rank.</p>
<p>Play Back<br>
  To raise or re-raise an opponent's bet.</p>
<p>Play Fast<br>
  Aggressively betting a drawing hand to get full value for it if you make it.</p>
<p>Play With<br>
  Staying in the hand by betting, calling, raising, or re-raising.</p>
<p>Playing the Board<br>
  In flop games, if your best five card hand uses the five community cards.</p>
<p>Pocket<br>
  Another term for hole.</p>
<p>Pocket Rockets<br>
  A pair of aces in the hole.</p>
<p>Position<br>
  Your seat in relation to the dealer, and thus your place in the betting order.</p>
<p>Post<br>
  To post a bet is to place your chips in the pot.</p>
<p>Pot<br>
  The money or chips in the center of the table.</p>
<p>Pot Limit<br>
  A game in which the maximum bet is the total of the pot.</p>
<p>Pot Odds<br>
  The amount of money in the pot versus the amount of money it will cost you to continue in the hand.</p>
<p>Prop<br>
  Short for proposition player; similar to a shill, but plays with his own money.</p>
<p>Proposition Player<br>
  A cardroom employee who joins a game with his own money when the game is shorthanded, or to get a game started; similar to a shill.</p>
<p>Protect A Hand<br>
  To protect a hand is to bet so as to reduce the chances of anyone outdrawing you by getting them to fold.</p>
<p>Protect Your Cards<br>
  To protect your cards is to place a chip or some other small object on top of them so that they don't accidentally get mucked by the dealer, mixed with another player's discards, or otherwise become dead when you'd like to play them.</p>
<p>Provider<br>
  A player who makes the game profitable for the other players at the table; a nicer term for a fish.</p>
<p>Push<br>
  When the hand is finished and a winner is determined, the dealer pushes the chips towards the winner.</p>
<p>Put Down<br>
  Fold.</p>
<p>Put Him On<br>
  To guess an opponent's hand and play accordingly.</p>
<p>Putting On The Heat<br>
  Pressuring your opponents with aggressive betting strategies to get the most value from your hand.</p>
<p>Quads<br>
  Four of a kind.</p>
<p>Qualifier<br>
  In high-low, a requirement the hand must meet to be eligible for a portion of the pot.</p>
<p>Rack<br>
  A plastic tray which holds 100 chips in 5 stacks of 20.</p>
<p>Rag Off<br>
  To get a card on the river that doesn't help you.</p>
<p>Ragged Flop<br>
  Flop cards that are of no use to any player's hand.</p>
<p>Rags<br>
  Worthless cards; blanks.</p>
<p>Rail<br>
  The sideline at a poker table.</p>
<p>Railbird<br>
  A non-playing spectator or kibitzer, often used to describe a broke ex-player.</p>
<p>Rainbow<br>
  Three or four cards of different suits.</p>
<p>Raise<br>
  To call and increase the previous bet.</p>
<p>Rake<br>
  Chips taken from the pot by the dealer on behalf of the house.</p>
<p>Rank<br>
  The value of a card. Each card has a suit and a rank.</p>
<p>Rap<br>
  To knock the table, indicating a check.</p>
<p>Razz<br>
  Seven-card stud lowball. Shortened from &quot;razzle dazzle.&quot;</p>
<p>Read<br>
  To try and determine your opponent's cards or betting strategy.</p>
<p>Rebuy<br>
  To start again, for an additional entry fee, in tournament play (where permitted).</p>
<p>Redraw<br>
  A draw to an even better hand when you currently are holding the nuts.</p>
<p>Represent<br>
  To bet in a way that suggests you are holding a strong hand.</p>
<p>Re-raise<br>
  To raise a raise.</p>
<p>Reverse Implied Odds<br>
  The ratio of the amount of money now in the pot to the amount of money you will have to call to continue from the present round to the end of the hand.</p>
<p>Riffle<br>
  To shuffle; or to fidget with your chips.</p>
<p>Ring Game<br>
  A non-tournament game.</p>
<p>River<br>
  In flop games, the last round of betting on the fifth street card; in stud games, the last round of betting on the seventh street card.</p>
<p>Rock<br>
  A very tight, conservative player.</p>
<p>Rock Garden<br>
  A table populated with rocks.</p>
<p>Roll<br>
  To turn a card face-up.</p>
<p>Rolled Up<br>
  In Seven-Card Stud, three of a kind on third street (the first three cards).</p>
<p>Rough<br>
  A lowball hand that is not perfect.</p>
<p>Round of Betting<br>
  The period during which each active player has the right to check, bet or raise. It ends when the last bet or raise has been called by all players still in the hand.</p>
<p>Rounder<br>
  A professional player who &quot;makes the rounds&quot; of the big poker games in the country.</p>
<p>Royal Flush<br>
  The best possible poker hand, consisting of the 10 through the Ace, all the same suit.</p>
<p>Run<br>
  A straight, or a series of good cards.</p>
<p>Run Over<br>
  Playing aggressively in an attempt to control the other players.</p>
<p>Runner-Runner<br>
  A hand made on the last two cards.</p>
<p>Running<br>
  Two needed cards that come as the last two cards dealt.</p>
<p>Running Bad<br>
  On a losing streak.</p>
<p>Running Good<br>
  On a winning streak.</p>
<p>Running Pair<br>
  When the last two cards on the board make a pair.</p>
<p>Rush<br>
  Several winning hands in a short period of time.</p>
<p>Sandbag<br>
  To check a strong hand with the intention of raising or re-raising.</p>
<p>Satellite<br>
  A small-stakes tournament whose winner obtains cheap entry into a bigger tournament.</p>
<p>Scare Card<br>
  An up card that looks as though it might have made a strong hand.</p>
<p>School<br>
  The players in a regular game.</p>
<p>Scoop<br>
  To win the entire pot.</p>
<p>Scooting<br>
  Passing chips to another player after winning a pot; horsing.</p>
<p>Seat Charge<br>
  In public cardrooms, an hourly fee for playing poker.</p>
<p>Seating List<br>
  In most cardrooms, if there is no seat available for you when you arrive, you can put your name on a list to be seated when a seat opens up.</p>
<p>Second Pair<br>
  In flop games, pairing the second highest card on board.</p>
<p>See<br>
  To call.</p>
<p>Semi-Bluff<br>
  To bet with a hand which isn't the best hand, but which has a reasonable chance of improving.</p>
<p>Set<br>
  Three of a kind; trips (usually applies to a pair in hand and a matching card on board).</p>
<p>Set You In<br>
  To bet as much as your opponent has left in front of him.</p>
<p>Seventh Street<br>
  The final betting round on the last card in Seven-Card Stud.</p>
<p>Shill<br>
  A cardroom employee, often an off-duty dealer, who plays with house money to make up a game.</p>
<p>Shootout<br>
  A tournament format in which a single player ends up with the entire prize money, or in which play continues at each table until only one player remains.</p>
<p>Short Odds<br>
  The odds for an event that has a good chance of occurring.</p>
<p>Short-Stacked<br>
  Having only a small number of chips left.</p>
<p>Show One, Show All<br>
  A rule that says if a player shows their cards to anyone at the table they can be asked to show everyone else.</p>
<p>Showdown<br>
  The point at the end of the final round of betting when all the remaining player's cards are turned up to see which player has won the pot.</p>
<p>Side Card<br>
  An unmatched card which may determine the winner between two otherwise equal hands.</p>
<p>Side Pot<br>
  A separate pot contested by other players when one player is all-in.</p>
<p>Sixth Street<br>
  In Seven-Card Stud, the fourth round of betting on the sixth card.</p>
<p>Skin<br>
  To fix the cards; cheat.</p>
<p>Slow Play<br>
  Disguising the value of a strong hand by underbetting, to trick an opponent.</p>
<p>Slowroll<br>
  To reveal one's hand slowly at showdown, one card at a time, to heighten the drama.</p>
<p>Small Blind<br>
  The smaller of the two compulsory bets in flop games, made by the player in the first postion to the dealer's left.</p>
<p>Smooth<br>
  The best possible low hand with a particular high card.</p>
<p>Smooth Call<br>
  To call rather than raise an opponent's bet.</p>
<p>Snap Off<br>
  To beat another player, often a bluffer, and usually without a powerful hand.</p>
<p>Speed<br>
  The level of aggressiveness with which you play. Fast play is more aggressive, slow play is more passive.</p>
<p>Splash Around<br>
  To play more loosely than you should.</p>
<p>Splash The Pot<br>
  To throw your chips into the pot, instead of placing them in front of you. This makes it difficult for the dealer to determine the amount you bet.</p>
<p>Split<br>
  A tie.</p>
<p>Spread<br>
  When a cardroom starts a table for a particular game, it is said to spread that game. If you want to know what games are played in a particular place, you can ask what they spread.</p>
<p>Spread Limit<br>
  Betting limits in which there is a fixed minimum and maximum bet for each betting round.</p>
<p>Squeeze<br>
  To look slowly at the extremities of your hole cards, without removing them from the table, to worry your opponents and heighten the drama.</p>
<p>Stack<br>
  The pile of chips in front of a player.</p>
<p>Stand Pat<br>
  To decline an opportunity to draw cards.</p>
<p>Stand-Off<br>
  A tie, in which the players divide the pot equally.</p>
<p>Stay<br>
  To remain in a hand with a call rather than a raise.</p>
<p>Steal<br>
  A bluff in late position, attempting to steal the pot from a table of apparently weak hands.</p>
<p>Steaming<br>
  Playing poorly and wildly, often because the player is emotionally upset.</p>
<p>Steel Wheel<br>
  In lowball, a straight flush, five high (Ace-2-3-4-5).</p>
<p>Straddle<br>
  To make a blind raise before the deal; big blind.</p>
<p>Straight<br>
  Five consecutive cards of mixed suits.</p>
<p>Straight Flush<br>
  Five consecutive cards of the same suit.</p>
<p>Streak<br>
  A run of good or bad cards.</p>
<p>String Bet<br>
  An illegal bet in which a player puts some chips in the pot, then reaches back to his stack for more, without having first verbally stated the full amount of his bet.</p>
<p>Structure<br>
  The limits set upon the ante, forced bets and subsequent bets and raises in any given game.</p>
<p>Stuck<br>
  Slang for losing, often a substantial amount of money.</p>
<p>Stud<br>
  Any form of poker in which the first card or cards are dealt down, or in the hole, followed by several open, or face up, cards.</p>
<p>Suck Out<br>
  To win a hand by hitting a very weak draw, often with poor pot odds.</p>
<p>Suited<br>
  Cards of the same suit.</p>
<p>Sweat<br>
  To watch a player from the rail.</p>
<p>Sweeten The Pot<br>
  Slang for raise.</p>
<p>Table<br>
  Refers to the poker table itself, or the collective players in the game.</p>
<p>Table Cop<br>
  A player who calls with the intention of keeping other players honest.</p>
<p>Table Stakes<br>
  A poker game in which a player cannot bet more than the money he has on the table.</p>
<p>Table Talk<br>
  Any discussion at the table of the hand currently underway, especially by players not involved in the pot, and especially any talk that might affect play.</p>
<p>Take Off A Card<br>
  To call a single bet in order to see one more card.</p>
<p>Take Off The Gloves<br>
  To use an aggressive betting strategy to bully opponents.</p>
<p>Take The Odds<br>
  To wager less money on a proposition than you hope to win.</p>
<p>Tap City<br>
  To go broke.</p>
<p>Tap Out<br>
  To bet all one's chips.</p>
<p>Tapped Out<br>
  Broke, busted.</p>
<p>Tell<br>
  A player's nervous habit or mannerism which might reveal his hand.</p>
<p>Texas Hold 'Em<br>
  A form of poker in which players use five community cards in combination with their two hole cards to form the best five-card hand. Also called hold 'em.</p>
<p>Third Pair<br>
  In flop games, pairing the third highest card on board.</p>
<p>Third Street<br>
  In Seven-Card Stud, the first round of betting on the first three cards.</p>
<p>Three Flush<br>
  Three cards of the same suit, requiring two more to make a flush.</p>
<p>Three Of A Kind<br>
  Three cards of the same denomination, with two side cards; trips.</p>
<p>Throwing A Party<br>
  When several loose or amateur players are making significant monetary contributions to the pot.</p>
<p>Tight<br>
  A conservative player who only plays strong hands, or playing on fewer hands than the norm.</p>
<p>Tight Game<br>
  A game with a small number of players in most pots.</p>
<p>Tilt<br>
  See on tilt.</p>
<p>To Go<br>
  An amount &quot;to go&quot; is the amount it takes to enter the pot.</p>
<p>Toke<br>
  A tip to the dealer.</p>
<p>Top Pair<br>
  In flop games, pairing the highest card on board.</p>
<p>Trey<br>
  A three.</p>
<p>Triplets<br>
  Three of a kind.</p>
<p>Trips<br>
  Slang for triplets; three of a kind.</p>
<p>Turn<br>
  In flop games, the fourth street card.</p>
<p>Two Flush<br>
  Two cards of the same suit, requiring three more to make a flush.</p>
<p>Two Pair<br>
  A hand with two pairs and a kicker.</p>
<p>Under-Raise<br>
  To raise less than the previous bet; allowed only if a player is going all-in.</p>
<p>Under The Gun<br>
  The first to bet.</p>
<p>Underdog<br>
  A hand that does not have the best chance of winning before all the cards are dealt.</p>
<p>Up Card<br>
  An open card, a card dealt face-up.</p>
<p>ke Up With A Hand<br>
  To be dealt a hand with winning potential.</p>
<p>Walk<br>
  To walk is to be away from the table long enough to miss one or more hands.</p>
<p>Walkers<br>
  Players who walk frequently.</p>
<p>Wheel<br>
  The lowest hand in lowball, Ace-2-3-4-5; also known as a bicycle.</p>
<p>Whipsaw<br>
  To raise before, and after, a caller who gets caught in the middle.</p>
<p>Wild Card<br>
  A card designated as a joker, playable as any value.</p>
<p>Wired Pair<br>
  A pair in hand.</p>
<p>World's Fair<br>
  A big hand.</p>
 </td>
  </tr>
</table>
</body>
</html>
