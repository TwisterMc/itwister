<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>

<h3>Sandbag</h3><p>
  To check a strong hand with the intention of raising or re-raising.</p>
<h3>Satellite</h3><p>
  A small-stakes tournament whose winner obtains cheap entry into a bigger tournament.</p>
<h3>Scare Card</h3><p>
  An up card that looks as though it might have made a strong hand.</p>
<h3>School</h3><p>
  The players in a regular game.</p>
<h3>Scoop</h3><p>
  To win the entire pot.</p>
<h3>Scooting</h3><p>
  Passing chips to another player after winning a pot; horsing.</p>
<h3>Seat Charge</h3><p>
  In public cardrooms, an hourly fee for playing poker.</p>
<h3>Seating List</h3><p>
  In most cardrooms, if there is no seat available for you when you arrive, you can put your name on a list to be seated when a seat opens up.</p>
<h3>Second Pair</h3><p>
  In flop games, pairing the second highest card on board.</p>
<h3>See</h3><p>
  To call.</p>
<h3>Semi-Bluff</h3><p>
  To bet with a hand which isn't the best hand, but which has a reasonable chance of improving.</p>
<h3>Set</h3><p>
  Three of a kind; trips (usually applies to a pair in hand and a matching card on board).</p>
<h3>Set You In</h3><p>
  To bet as much as your opponent has left in front of him.</p>
<h3>Seventh Street</h3><p>
  The final betting round on the last card in Seven-Card Stud.</p>
<h3>Shill</h3><p>
  A cardroom employee, often an off-duty dealer, who plays with house money to make up a game.</p>
<h3>Shootout</h3><p>
  A tournament format in which a single player ends up with the entire prize money, or in which play continues at each table until only one player remains.</p>
<h3>Short Odds</h3><p>
  The odds for an event that has a good chance of occurring.</p>
<h3>Short-Stacked</h3><p>
  Having only a small number of chips left.</p>
<h3>Show One, Show All</h3><p>
  A rule that says if a player shows their cards to anyone at the table they can be asked to show everyone else.</p>
<h3>Showdown</h3><p>
  The point at the end of the final round of betting when all the remaining player's cards are turned up to see which player has won the pot.</p>
<h3>Side Card</h3><p>
  An unmatched card which may determine the winner between two otherwise equal hands.</p>
<h3>Side Pot</h3><p>
  A separate pot contested by other players when one player is all-in.</p>
<h3>Sixth Street</h3><p>
  In Seven-Card Stud, the fourth round of betting on the sixth card.</p>
<h3>Skin</h3><p>
  To fix the cards; cheat.</p>
<h3>Slow Play</h3><p>
  Disguising the value of a strong hand by underbetting, to trick an opponent.</p>
<h3>Slowroll</h3><p>
  To reveal one's hand slowly at showdown, one card at a time, to heighten the drama.</p>
<h3>Small Blind</h3><p>
  The smaller of the two compulsory bets in flop games, made by the player in the first postion to the dealer's left.</p>
<h3>Smooth</h3><p>
  The best possible low hand with a particular high card.</p>
<h3>Smooth Call</h3><p>
  To call rather than raise an opponent's bet.</p>
<h3>Snap Off</h3><p>
  To beat another player, often a bluffer, and usually without a powerful hand.</p>
<h3>Speed</h3><p>
  The level of aggressiveness with which you play. Fast play is more aggressive, slow play is more passive.</p>
<h3>Splash Around</h3><p>
  To play more loosely than you should.</p>
<h3>Splash The Pot</h3><p>
  To throw your chips into the pot, instead of placing them in front of you. This makes it difficult for the dealer to determine the amount you bet.</p>
<h3>Split</h3><p>
  A tie.</p>
<h3>Spread</h3><p>
  When a cardroom starts a table for a particular game, it is said to spread that game. If you want to know what games are played in a particular place, you can ask what they spread.</p>
<h3>Spread Limit</h3><p>
  Betting limits in which there is a fixed minimum and maximum bet for each betting round.</p>
<h3>Squeeze</h3><p>
  To look slowly at the extremities of your hole cards, without removing them from the table, to worry your opponents and heighten the drama.</p>
<h3>Stack</h3><p>
  The pile of chips in front of a player.</p>
<h3>Stand Pat</h3><p>
  To decline an opportunity to draw cards.</p>
<h3>Stand-Off</h3><p>
  A tie, in which the players divide the pot equally.</p>
<h3>Stay</h3><p>
  To remain in a hand with a call rather than a raise.</p>
<h3>Steal</h3><p>
  A bluff in late position, attempting to steal the pot from a table of apparently weak hands.</p>
<h3>Steaming</h3><p>
  Playing poorly and wildly, often because the player is emotionally upset.</p>
<h3>Steel Wheel</h3><p>
  In lowball, a straight flush, five high (Ace-2-3-4-5).</p>
<h3>Straddle</h3><p>
  To make a blind raise before the deal; big blind.</p>
<h3>Straight</h3><p>
  Five consecutive cards of mixed suits.</p>
<h3>Straight Flush</h3><p>
  Five consecutive cards of the same suit.</p>
<h3>Streak</h3><p>
  A run of good or bad cards.</p>
<h3>String Bet</h3><p>
  An illegal bet in which a player puts some chips in the pot, then reaches back to his stack for more, without having first verbally stated the full amount of his bet.</p>
<h3>Structure</h3><p>
  The limits set upon the ante, forced bets and subsequent bets and raises in any given game.</p>
<h3>Stuck</h3><p>
  Slang for losing, often a substantial amount of money.</p>
<h3>Stud</h3><p>
  Any form of poker in which the first card or cards are dealt down, or in the hole, followed by several open, or face up, cards.</p>
<h3>Suck Out</h3><p>
  To win a hand by hitting a very weak draw, often with poor pot odds.</p>
<h3>Suited</h3><p>
  Cards of the same suit.</p>
<h3>Sweat</h3><p>
  To watch a player from the rail.</p>
<h3>Sweeten The Pot</h3><p>
  Slang for raise.</p>

 </td>
  </tr>
</table>
</body>
</html>
