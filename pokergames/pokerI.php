<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Poker - Poker Rules Odds Information</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link href="pokerstyle.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#FFFFFF" background="interface/back.jpg" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- iTwister Responsive -->
<ins class="adsbygoogle"
     style="display:inline-block;width:970px;height:90px"
     data-ad-client="ca-pub-8716721475579600"
     data-ad-slot="3850918332"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2" background="interface/left.jpg"><img src="interface/left.jpg" width="48" height="36"></td>
    <td colspan="2" bgcolor="#FFFFFF" align="center"><img src="interface/header.jpg" width="445" height="109"></td>
    <td rowspan="2" background="interface/right.jpg"><img src="interface/right.jpg" width="48" height="36"></td>
  </tr>
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><?php include("menu.php"); ?>
    </td>
    <td bgcolor="#FFFFFF" valign="top">

<?php include("termsINC.php"); ?>
<h3>Ignorant End</h3><p>
  The low end straight.</p>
<h3>Implied Odds</h3><p>
  The amount of money you expect to win if you make your hand versus the amount of money it will cost you to continue playing.</p>
<h3>In</h3><p>
  A player is &quot;in&quot; if he or she has called all bets.</p>
<h3>In the Air</h3><p>
  When the tournament director instructs the dealers to get the cards in the air, it means to start dealing.</p>
<h3>In The Dark</h3><p>
  To check or bet blind, without looking at your cards.</p>
<h3>Inside Straight</h3><p>
  Four cards requiring one in the middle to fill a straight.</p>
<h3>Insurance</h3><p>
  Selling the actual outcome of the hand for its mathematical equity.</p>
<h3>Isolate</h3><p>
  To raise with the intention of reaching a heads up between yourself and a single other player.</p>
</td>
  </tr>
</table>
</body>
</html>
