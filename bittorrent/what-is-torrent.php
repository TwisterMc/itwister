<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>What is BitTorrent? : Torrent Information : BitTorrent</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">What is Bit Torrent</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2"> <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>What is BitTorrent?<br />
                  <br />
                  BitTorrent is a protocol designed for transferring files. It is peer-to-peer in nature, as users connect to each other directly to send and receive portions of the file. However, there is a central server (called a tracker) which coordinates the action of all such peers. The tracker only manages connections, it does not have any knowledge of the contents of the files being distributed, and therefore a large number of users can be supported with relatively limited tracker bandwidth. The key philosophy of BitTorrent is that users should upload (transmit outbound) at the same time they are downloading (receiving inbound.) In this manner, network bandwidth is utilized as efficiently as possible. BitTorrent is designed to work better as the number of people interested in a certain file increases, in contrast to other file transfer protocols.<br />
                  <br />
                  One analogy to describe this process might be to visualize a group of people sitting at a table. Each person at the table can both talk and listen to any other person at the table. These people are each trying to get a complete copy of a book. Person A announces that he has pages 1-10, 23, 42-50, and 75. Persons C, D, and E are each missing some of those pages that A has, and so they coordinate such that A gives them each copies of the pages he has that they are missing. Person B then announces that she has pages 11-22, 31-37, and 63-70. Persons A, D, and E tell B they would like some of her pages, so she gives them copies of the pages that she has. The process continues around the table until everyone has announced what they have (and hence what they are missing.) The people at the table coordinate to swap parts of this book until everyone has everything. There is also another person at the table, who we'll call 'S'. This person has a complete copy of the book, and so doesn't need anything sent to him. He responds with pages that no one else in the group has. At first, when everyone has just arrived, they all must talk to him to get their first set of pages. However, the people are smart enough to not all get the same pages from him. After a short while they all have most of the book amongst themselves, even if no one person has the whole thing. In this manner, this one person can share a book that he has with many other people, without having to give a full copy to everyone that's interested. He can instead give out different parts to different people, and they will be able to share it amongst themselves. This person who we've referred to as 'S' is called a seed in the terminology of BitTorrent. There's more about the various terms in a later section.</p>
                
              </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
