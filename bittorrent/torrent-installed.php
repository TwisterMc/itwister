<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Installed Torrent. Now What? : Torrent Information : BitTorrent</title>
<meta name="description" content="Torrent information, uninstall torrents, firewalls, proxys, errors, how to install and more.">
<meta name="keywords" content="torrent, sharing, file, share, bit, bitorrent, errors, uninstall, install, firewall, proxy, port">
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table width="359" border="0" cellpadding="0" cellspacing="3">
                <tr height="26">
                  <td width="356" height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Installed Torrent Now What</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
		   <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top">
                <p>I've installed BitTorrent, now what? There's no program to run!<br />
                  <br />
                  BitTorrent is not like other peer-to-peer applications (such as Winmx, Kazaa, Gnutella, etc.) in that it does not have its own "universe." Put another way, BT lives on top of the Web, which means that all of the searching/listing of available files is done on the web. When you find a file you want to download, you click on it and the BitTorrent client program will run and ask you where to put it, and then start downloading. See the links section for some starting points on the web if you're new.<br />
                  I just installed BitTorrent but whenever I click on a link I just get a small file and nothing happens?<br />
                  <br />
                  Try closing and restarting your web browser. When BitTorrent installs, it registers a new MIME-Type (application/x-bittorrent) and this change will not take effect until the next time the web browser is opened.<br />
                  What is this Mandrake thing I'm invited to download after BitTorrent?<br />
                  <br />
                  You don't need it. Mandrake is a full Linux distribution, a replacement Operating System. It is an example of the kind of thing that BitTorrent was designed to distribute -- large programs which, upon release, cause a flood of eager downloaders. In other words, it's not related to BitTorrent in any way, it's just something a lot of people might be interested in.</p>
               
                </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
