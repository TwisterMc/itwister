<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Torrent Terms : Torrent Information : BitTorrent</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Torrent Terms</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
		   <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>

                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>What do all these words mean? (seeding, uploading, share rating, etc.)<br />
                  <br />
                  Here is a brief list of words associated with BitTorrent and their meanings.<br />
                  <br />
                  torrent<br />
                  Usually this refers to the small metadata file you receive from the web server (the one that ends in .torrent.) Metadata here means that the file contains information about the data you want to download, not the data itself. This is what is sent to your computer when you click on a download link on a website. You can also save the torrent file to your local system, and then click on it to open the BitTorrent download. This is useful if you want to be able to re-open the torrent later on without having to find the link again.<br />
                  In some uses, it can also refer to everything associated with a certain file available with BitTorrent. For example, someone might say "I downloaded that torrent" or "that server has a lot of good torrents", meaning there are lots of good files available via BitTorrent on that server.<br />
                  peer<br />
                  A peer is another computer on the internet that you connect to and transfer data. Generally a peer does not have the complete file, otherwise it would be called a seed. Some people also refer to peers as leeches, to distinguish them from those generous folks who have completed their download and continue to leave the client running and act as a seed.<br />
                  seed<br />
                  A computer that has a complete copy of a certain torrent. Once your client finishes downloading, it will remain open until you click the Finish button (or otherwise close it.) This is known as being a seed or seeding. You can also start a BT client with a complete file, and once BT has checked the file it will connect and seed the file to others. Generally, it's considered good manners to continue seeding a file after you have finished downloading, to help out others. Also, when a new torrent is posted to a tracker, someone must seed it in order for it to be available to others. Remember, the tracker doesn't know anything of the actual contents of a file, so it's important to follow through and seed a file if you upload the torrent to a tracker.<br />
                  reseed<br />
                  When there are zero seeds for a given torrent (and not enough peers to have a distributed copy), then eventually all the peers will get stuck with an incomplete file, since no one in the swarm has the missing pieces. When this happens, someone with a complete file (a seed) must connect to the swarm so that those missing pieces can be transferred. This is called reseeding. Usually a request for a reseed comes with an implicit promise that the requester will leave his or her client open for some time period after finishing (to add longevity to the torrent) in return for the kind soul reseeding the file.<br />
                  swarm<br />
                  The group of machines that are collectively connected for a particular file. For example, if you start a BitTorrent client and it tells you that you're connected to 10 peers and 3 seeds, then the swarm consists of you and those 13 other people.<br />
                  tracker<br />
                  A server on the Internet that acts to coordinate the action of BitTorrent clients. When you open a torrent, your machine contacts the tracker and asks for a list of peers to contact. Periodically throughout the transfer, your machine will check in with the tracker, telling it how much you've downloaded and uploaded, how much you have left before finishing, and the state you're in (starting, finished download, stopping.) If a tracker is down and you try to open a torrent, you will be unable to connect. If a tracker goes down during a torrent (i.e., you have already connected at some point and are already talking to peers), you will be able to continue transferring with those peers, but no new peers will be able to contact you. Often tracker errors are temporary, so the best thing to do is just wait and leave the client open to continue trying.<br />
                  downloading<br />
                  Receiving data FROM another computer.<br />
                  uploading<br />
                  Sending data TO another computer.<br />
                  share rating<br />
                  If you are using the experimental client with the stats-patch, you will see a share rating displayed on the GUI panel. This is simply the ratio of your amount uploaded divided by your amount downloaded. The amounts used are for the current session only, not over the history of the file. If you achieve a share ratio of 1.0, that would mean you've uploaded as much as you've downloaded. The higher the number, the more you have contributed. If you see a share ratio of "oo", this means infinity, which will happen if you open a BT client with a complete file (i.e., you seed the file.) In this case you download nothing since you have the full file, and so anything you send will cause the ratio to reach infinity. Note: The share rating is just a number that is displayed for your convenience. It does not directly affect any aspect of the client at all. In general, out of courtesy to others you should strive to keep this ratio as high as possible, of course.<br />
                  distributed copies<br />
                  In some versions of the client, you will see the text "Connected to n seeds; also seeing n.nnn distributed copies." A seed is a machine with the complete file. However, the swarm can collectively have a complete copy (or copies) of the file, and that is what this is telling you. Referring again to the "people at a table" analogy, consider the case where the book has 10 pages, and person A has pp.1-5 and B has pp.6-10. Collectively, A and B have a complete copy of the book, even though no one person has the whole thing. In other words, even if there are no seeds, as long as there is at least one distributed copy of the file everyone can eventually get a complete file. Meditate on this, the Zen of BitTorrent, grasshopper.<br />
                  choked<br />
                  This is a term used in the description of the BitTorrent protocol. It refers to the state of an uploader, i.e. the thread that sends data to another peer. When a connection is choked, it means that the transmitter doesn't currently want to send anything on that link. A BT client signals that it's choked to other clients for a number of reasons, but the most common is that by default a client will only maintain --max_uploads active simultaneous uploads, the rest will be marked choked. (The default value is 4 and this is the same setting that experimental client GUI lets you adjust.) A connection can also be choked for other reasons, for example a peer downloading from a seed will mark his connection as choked since the seed is not interested in receiving anything. Note that since each connection is bidirectional and symmetrical, there are two choked flags for each connection, one for each Tx endpoint.<br />
                  interested<br />
                  Another term used in the protocol specification. This is the corollary to the choked flag, in that interested refers to the state of a downloader with respect to a connection. A downloader is marked as interested if the other end of the link has any pieces that the client wants, otherwise the connection is marked as not interested.<br />
                  snubbed<br />
                  If the client has not received anything after a certain period (default: 60 seconds), it marks a connection as snubbed, in that the peer on the other end has chosen not to send in a while. See the definition of choked for reasons why an uploader might mark a connection as choked. The real function of keeping track of this variable is to improve download speeds. Occasionally the client will find itself in a state where even though it is connected to many peers, it is choked by all of them. The client uses the snubbed flag in an attempt to prevent this situation. It notes that a peer with whom it would like to trade pieces with has not sent anything in a while, and rather than leaving it up to the optimistic choking to eventuall select that peer, it instead reserves one of its upload slots for sending to that peer. (Reference)<br />
                  optimistic unchoking<br />
                  Periodically, the client shakes up the list of uploaders and tries sending on different connections that were previously choked, and choking the connections it was just using. You can observe this action every 10 or 20 seconds or so, by watching the "Advanced" panel of one of the experimental clients.</p>
                
              </span> </td>
            </tr><tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
	 
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
