<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Torrent Errors</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Torrent Errors</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
            <tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
              <td valign="top" width="125"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">

                <p>I'm getting an error message, what does it mean?<br />
                  <br />
                  The best thing to do in general if you're having connectivity problems is just wait. Often trackers are unavailable or slow to respond, usually due to high load or sometimes DDoS attacks. Some torrents can take a while to get up to speed, so patience is a virtue. That being said, below are some common error messages with explanations and what to do about them.<br />
                  <br />
                  Note: Often you will get a red error message when there's a problem connecting to the tracker, but the client will keep on retrying. This is normal. It can result in the download progressing normally and successfully, even with an error message displayed on the screen. Make sure to note the time-stamp on the error, and if it's more than 5 to 10 minutes old, you can ignore it. The newer versions of the experimental client "age away" the error messages after 5 minutes to deal with this situation.<br />
                  <br />
                  Problem getting response info - [Errno 2] No such file or directory: "C:\\Documents and Sett..."<br />
                  For some reason, Internet Explorer sometimes doesn't save the torrent file in the Temporary Internet Files directory properly. The solution seems to be to right click on the link and choose Save As..., and save the torrent file to disk, and then double-click the file to launch the client. Clearing the IE cache seems to help if this problem is recurring. It seems to be related to torrent files with square brackets (']' and '[') in the filename.<br />
                  Too many args - 0 max.<br />
                  This error in indicative of a bad command line. See the section on setting the command line and ensure that the arguments to the BitTorrent program include --responsefile "%1".<br />
                  A piece failed hash check, re-downloading it<br />
                  This is a benign message, you can safely ignore it. It means that you received a piece of the file that didn't check out as being correct, so it will be downloaded again. Probable cause of this might be someone incorrectly using the "skip hash check" option.<br />
                  bad data from tracker -<br />
                  Usually you can ignore this, it seems to happen when the tracker is overloaded or otherwise flaky.<br />
                  Problem connecting to tracker - timeout exceeded<br />
                  Problem connecting to tracker - HTTP Error 503: Connect failed<br />
                  Problem connecting to tracker - [Errno socket error] (10061, "Connection refused")<br />
                  Problem connecting to tracker - (111, 'Connection refused')<br />
                  There was a problem contacting the tracker. Trackers tend to be heavily loaded, and connections sometimes fail. The best thing to do is just be patient and leave the client open. If you find you're getting this a lot, you can try increasing the HTTP request timeout by adding the parameter "--http_timeout 120" (the default is 60, unit is seconds.) See the section on changing the command line if you need help doing this.<br />
                  Problem connecting to tracker - HTTP Error 400: Not Authorized<br />
                  This indicates that the administrators of this tracker are not allowing it to be used for this torrent. Some trackers will only track torrents that are also posted in their forums/website, for example. Usually this indicates a stale torrent -- try going to the web site associated with the tracker and see if you can find an updated torrent.<br />
                  Problem connecting to tracker - HTTP Error 404: Not Found<br />
                  Probably a stale torrent. Try to find a new link to the torrent.<br />
                  Problem connecting to tracker - HTTP Error 407: Proxy Authentication Required<br />
                  You may need to configure a username and password for your proxy server setting in order to contact the tracker. See this section for details.<br />
                  <br />
                  What can I do if I get a blue screen error, spontaneous reboot, or lockup?<br />
                  <br />
                  Some network cards and DSL modems have buggy drivers. Common symptoms include a blue screen (with a DRIVER_IRQL_NOT_LESS_OR_EQUAL error) or a spontaneous reboot. Here are some common culprits:<br />
                  <br />
                  * Linksys LNE100TX model 5, Linksys NC100, Skymaster SK1207E, Planex FNW-9803-T, or any other network card based on the AN983B chipset by ADMtek, sometimes also sold under the no-name "Asound" or "Fast" brands. Note that this includes some motherboards' built-in Ethernet controllers, such as: MSI (Microstar) MS-6378, DFI NS70-EL & AZ30-EL, USI PM-845, Fujitsu D1451. The solution seems to be to install one of the following drivers from ADMtek: Windows XP, Windows ME/2000, Windows 98. These are drivers from the chipset manufacturer and are Microsoft certified. Use these drivers in place of any other driver for those cards, including the latest version from Linksys.<br />
                  * Netgear FA311 - Try this version (1.30) of the drivers from Netgear.<br />
                  * Netgear FA312 - Seems to have the same problem as the FA311, but try this version (1.8) of the drivers instead. (Note that this driver should work with both the FA311 and FA312, so also try it if you have the FA311 and the above driver doesn't work.)<br />
                  * Alcatel Speed Touch USB DSL modem - Install these drivers.<br />
                  <br />
                  If your network interface card (NIC) or DSL/cable modem were not listed above, then check with the manufacturer's website and make sure you have the latest drivers.<br />
                  My internet connection drops, often during very fast downloads. What can I do?<br />
                  <br />
                  This issue is still unresolved, but my guess is that it's due to buggy firmware in the xDSL/cable modem or router. Reports on the mailing list seem to indicate that transfers complete without issue if the download rates are low. It seems that some people have come up with some very creative workarounds to deal with this, such as stopping the transfer if it gets too fast and restarting.<br />
                  <br />
                  Limiting the download rate is much harder than limiting the upload rate, because one can only really control the rate at which packets leave the system. The rate at which they arrive is determined by the originating systems and any routers, gateways, or traffic shapers along the path. However, there are several ways that software can achieve the effect of limiting the download rate -- they amount to basically dropping some packets if they're coming in too fast, which will cause the TCP/IP stack of the sender to back off somewhat.<br />
                  <br />
                  For those having these sorts of problems, here are the methods of which I'm aware to limit the download rate:<br />
                  <br />
                  * For Windows, the Netlimiter program claims to be able to limit both the download and upload rate of individual programs or connections. The program is still in beta-testing, and it shows. My initial trial of it was not positive: it reported vastly wrong figures of its estimation of the BitTorrent program upload rate, its sockets wrapper broke Apache and rsync-over-ssh on my cygwin system, and its installer was ill- behaved. Finally, it is not free software, in both the "free speech" and "free beer" senses of the word. Not recommended at all.<br />
                  * For Linux, *BSD, and Solaris, there is trickle, which is a userspace traffic shaper that uses the dynamic library preload facility. This has the advantages of requiring neither root privileges nor a recompiled kernel, and a simple command line e.g. trickle -u 20 -d 80 btdownloadgui.py --responsefile "%1".<br />
                  * For Linux, you can try using the QoS features of the kernel to set an ingress filter. A recent mailing list post gives an example script that uses the tc (traffic control) command. Note that for this to work you will need root access and you may have to recompile the kernel (for netlink sockets and QoS.) See this page for information about configuring the kernel, this page about the ingress queueing discipline, and the Linux Advanced Routing & Traffic Control HOWTO as well as the Advanced Networking Overview for more information. The BSDs surely have traffic shaping as well, if anyone would like to contribute some links I will update this section.<br />
                  * For Mac OS X (10.2.x only), Carrafix claims the capability to limit both upload and download rate for each port.<br />
                  * Finally, another option is to limit the number of connections that BitTorrent makes. The number of connections does not directly correlate with download speed, however, since a client may be connected to a number of slow peers or just one or two very fast peers. The command line parameters that affect the download rate are: (see this section for details on modifying the command line in Windows)<br />
                  <br />
                  --min_peers n<br />
                  determines the minimum number of peers that the BT client should be connected to before it will stop asking the tracker for more. The default is 20.<br />
                  --max_initiate n<br />
                  determines when the client should stop initiating new peer connections. The default is 40. Note: I believe, but am not positive, that this only affects outgoing, i.e. initiated connections. In other words the client will still continue to accept new connections past the number specified here, but will not initiate any further outbound connections.<br />
                  --request_backlog n<br />
                  determines the number of download requests to keep queued, defaults to 5. Setting this to a lower value should decrease the overall download efficiency and speed.<br />
                  <br />
                  So, you might try --min_peers 5 --max_initiate 10 --request_backlog 3 (for example) and see if it helps alleviate the sporadic disconnects or other related problems.<br />
                  <br />
                  Note: Please let me know of any successes or failures at using these methods to cure the disconnect issue. I have no personal experience with the issue so I don't know what's worth trying.</p>
           
                </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
