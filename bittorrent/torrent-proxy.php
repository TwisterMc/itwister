<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Torrent Proxy : Torrent Information : BitTorrent</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Torrent Proxy</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
		   <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>

                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>Can I use a proxy server with BitTorrent?<br />
                  <br />
                  First, note that there are two types of connections that the BitTorrent program must make:<br />
                  <br />
                  * Outbound HTTP connections to the tracker, usually on port 6969.<br />
                  * Inbound and outbound connections to the peer machines, usually on port 6881 and up.<br />
                  <br />
                  A web proxy can only be used for the first type of connection, since the second type is not HTTP. Theoretically, you could use the HTTP CONNECT command to tunnel them through an HTTP proxy, but this would require additional code support in the client. There is a possible workaround for this scenario, however; see the final point below.<br />
                  <br />
                  That having been said, here is how to configure an HTTP proxy for the tracker connections:<br />
                  <br />
                  * If the proxy does not require authorization, the system-wide proxy configuration should work. For Windows, open the Control Panel select Internet Options, click on the Connections tab, select your connection and click Settings... (or Lan Settings... if you have a direct connection.) Make sure the Use a proxy server option is selected and enter the proxy's address and port.<br />
                  * If your proxy requires basic authorization, set the http_proxy environment variable to http://username:password@hostname:port, where username and password are your login and password, and hostname:port is the address and port of the proxy server. If you don't know how to set environment variables, there are instructions for Windows on this page by Mike Ravkine (krypt).<br />
                  * If your proxy requires NTLM authorization (a Microsoft-proprietary scheme), you may have to use a third party program. Fortunately there is a utility called NTLM Authorization Proxy Server. It is a program you run on your local machine that acts as a proxy for your proxy. In other words, it takes the (unauthenticated) proxy requests from the application (BitTorrent) and forwards them on to your organization's proxy, adding the necessary NTLM authorization. It is written in Python and available as source, so you must install Python on your computer before running it. Refer to the home page for more information.<br />
                  * If you are in a firewalled environment where NO outbound connections are allowed except those through an HTTP proxy, you will have difficulty using BitTorrent. One method which might work is as follows: The desproxy program can serve as a SOCKS 4 or 5 server than tunnels requests through the proxy server. Then use SocksCap to "socksify" (intercept the network calls and redirect them to the SOCKS server) the BitTorrent program. See below for a few notes about SocksCap. Note: If you get this method to work, please report your steps and results so that I can improve this part of the FAQ. Thanks.</p>
             
              </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
