<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Torrent FAQ</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Torrent FAQ</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
		   <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>What if I need to use SOCKS to access the Internet?<br />
                  <br />
                  Look into a program called SocksCap. It can be used to socksify any normal program. The complication here is that you have to give SocksCap a command line to run, and the btdownloadgui command line will be different for each torrent. One suggestion would be to setup a command line in SocksCap of btdownloadgui.exe --responsefile "c:\downloads\file.torrent". (Substitute any suitable directory in the command.) Now, when you want to open a torrent, save it as "file.torrent" in "c:\downloads" (or whatever you used) and then run the command in SocksCap.<br />
                  BitTorrent says I'm uploading, what files am I sharing? What's being sent?<br />
                  <br />
                  Don't worry. When you are downloading a particular torrent, you are also uploading that torrent at the same time. The parts of the file(s) that you have already downloaded are uploaded to other peers. This is normal, and it's how the protocol works. There is no "shared directory" setting as with other peer-to-peer applications. If you have a certain file (or files) that you want to make available to others, you must first create a .torrent file and upload it to a server, and then seed the file. See the section creating a new torrent for the detailed procedure.<br />
                  What happens if I cancel a download? How can I resume?<br />
                  <br />
                  BitTorrent fully supports stopping and later resuming a partial download. You don't have to do anything special. If you cancel a download before it's finished, the partial download remains on your hard drive. To resume the transfer, just click on the same torrent link again and when asked where to save the file, select the same location as last time. BitTorrent will see that the file exists and check it to see how much has already been downloaded. It will then pick up where it left off the last time. See also the section regarding file size.<br />
                  <br />
                  Note: To resume properly, you must make the same selection when prompted as you made the first time. For torrents consisting of a single file, this is rather straight-forward: simply select the file. However, torrents that consist of a folder of multiple files can be a bit more confusing. To resume, you must select the folder that contains the BitTorrent folder.<br />
                  <br />
                  Here's an example of resuming a folder-type torrent. Let's suppose that you downloaded a torrent called SomeCoolBand, and selected to put it in the folder Downloads. So your directory structure resembles something like \Downloads\SomeCoolBand\file1, \Downloads\SomeCoolBand\file2, and so on. The important part of this example is that should you resume this transfer, when asked to select a destination folder you must select the \Downloads folder and NOT \Downloads\SomeCoolBand. It may seem a bit counter-intuitive, but just remember to always make the same selection as the original choice. When you first started the transfer there was no SomeCoolBand folder; you instead selected \Downloads and BT created the SomeCoolBand folder.<br />
                  Why is my downloaded file huge even though I only downloaded a small bit?<br />
                  <br />
                  When BitTorrent starts, it allocates space for the entire file(s). That is what you see at startup as the progress bar moves across the screen and the disk drive goes crazy. The reason it does this is because it downloads the file in pieces, and those pieces arrive in an arbitrary order. Unlike http or ftp, which download the file from start to finish, BT downloads it in random order.<br />
                  Why does my hard drive go crazy at the beginning of a resumed download?<br />
                  <br />
                  When you open a torrent and give BitTorrent a filename/directory that already exists, it must check the file to see how much of it is useful data and how much is junk. (Recall that BT allocates space for the entire file when you first start a torrent.) To do this it must read the entire contents of the file, and generate what's known as a hash for each piece of the file. A hash is a cryptographic function that creates a small summary or digest of a large amount of data. BitTorrent uses the SHA hash function to determine which parts of the file are good and which are bad.<br />
                  What is seeding? How do I do it? Why should I leave the client open after it finishes downloading?<br />
                  <br />
                  First, you may want to review the answers to the question on terminology. A seed is a client which has a complete file. Seeding is the process of connecting to a torrent when you have a complete file. There are two ways to do this:<br />
                  <br />
                  * ...by leaving your client open after the download completes. Once you have the entire file you become a seed, and the BitTorrent client remains connected to the swarm, sending to other users until you close it.<br />
                  * ...by clicking on a torrent link (or opening a saved .torrent file) and selecting a filename of a file that has already completed. BitTorrent will check over the file and realize that it's already complete, and continue to connect to the tracker and serve as a seed.<br />
                  <br />
                  It's generally considered a good idea to leave your client open as long as possible, since it helps other users. Some communities have guidelines on when it's permissible to disconnect, typically after the ratio of bytes received to bytes sent reaches 1:1, or 24 hours after the download completes. Please be nice, and do what you can to contribute to other users.<br />
                  <br />
                  My download speed seems slow, what can I do to increase it?<br />
                  <br />
                  Here are some general guidelines to getting fast connections with BitTorrent.<br />
                  <br />
                  * Give it time! Be patient! This is the most important suggestion for most speed-related problems. Sometimes it can take a while to contact a slow tracker. The beginning of a download will be especially slow since you don't have any pieces of the file to share with others. Recently, trackers have been quite overloaded and it's common to get timeout-related errors. Leave the client open and eventually it will connect.<br />
                  * If your network uses NAT, make sure the BitTorrent ports are forwarded to the machine that runs the client. This will allow inbound connections from peers. Otherwise, only outbound connections will succeed. See this section of the FAQ for more details.<br />
                  * If you have a software firewall, make sure the BitTorrent client has the proper access.<br />
                  * Make sure the torrent is "live." Use an experimental build of the client that shows the number of peers and seeds to which you are connected. Or, check the status of the torrent (using the website's statistics or TorrentSpy) and make sure there are other people connected. To get decent speed, a torrent must have at least a few other people connected. The more peers, the faster the transfer will be in general.<br />
                  * Sometimes, limiting your upload rate will increase your download rate. This is especially true for asymmetric connections such as cable and ADSL, where the outbound bandwidth is much smaller than the inbound bandwidth. If you are seeing very high upload rates and low download rates, this is probably the case. The reason this happens is due to the nature of TCP/IP -- every packet received must be acknowledged with a small outbound packet. If the outbound link is saturated with BitTorrent data, the latency of these TCP/IP ACKs will rise, causing poor efficiency.<br />
                  <br />
                  Use a client that allows limiting of the upload rate, and set it to around 80% of the maximum rate observed. It can be tempting to limit the rate to very small values. On very healthy torrents, this will not adversely affect the download rate. However, when there are fewer peers you will generally get higher download rates by allowing the highest upload rate possible before saturating the link -- the (approx.) 80% sweet spot.<br />
                  <br />
                  To limit the upload rate with Mac OS X, try Carrafix. You'll want to set an individual cap for each BitTorrent port (6881 and up.)<br />
                  * Ensure that your network allows the outgoing connections necessary for BitTorrent to work. Some networks (usually at schools, workplaces, etc.) are firewalled and all connections must go through a proxy server. In other cases, only well-known ports are available. There are too many different situations to list every possible scenario, but if you are trying to download a torrent that you know to be "live" yet the client still reports zero peers and seeds, then this is probably the case. See also the question about proxy settings.<br />
                  <br />
                  How can I get a list of the people to whom I'm connected?<br />
                  <br />
                  Try the command "netstat -an" from a command prompt. This should work for Windows, Linux/Unix, and Mac OS X. It will give you a list of all the network connections on your machine. Generally, you are only interested in connections where the state is ESTABLISHED. Also, connections with the local address of "127.0.0.1" are usually not interesting, as they indicate local-only connections.<br />
                  <br />
                  For Windows, there is a much better tool for this that gives a more informative output with a GUI instead of a command line interface. Download TCPView, a free program. It will show an output similar to the netstat command, but it will also list the program name and PID (process ID) of which program "owns" each connection. Try the Show Unconnected Endpoints button to hide connections that are not in a state of ESTABLISHED. The Resolve Addresses command will perform a reverse-DNS lookup, which will display the hostname of the machines to which you are connected (if available), instead of their numeric IP addresses. TCPView even has an option to close a specific connection, if you right click on the desired row and choose Close Connection.<br />
                  <br />
                  Using either netstat or TCPView, you can figure out which connections are inbound and which are outbound. Those with a Local Address ending in ":6881" through ":6999" (or any port number in the port range that you have specified) are inbound connections. The rest are probably outbound, especially if it has a Remote Address that ends in ":6881" through ":6999".<br />
                  <br />
                  In the latest build of the experimental client (3.2.1b-2), if you click on the Advanced button you will get a screen showing details about each of the machines you are connected to for the current torrent.<br />
                  Is there a way to preview a file before it's finished?<br />
                  <br />
                  There is no good way to do this. Because the BitTorrent protocol downloads pieces in arbitrary order, there is no guarantee that the part of the file necessary for previewing (usually the beginning of the file) is present. To further complicate matters, some torrents are packaged as an archive, which would be quite difficult to extract until it's complete.<br />
                  <br />
                  Still, if you want to attempt to view the file periodically, you may eventually get lucky. First, make sure the file you are downloading is not an archive. If it's a ZIP or RAR (R00, R01, ...) file, forget it. Next, you'll have to interrupt the download, since BitTorrent locks the file in an exclusive mode until the file is complete. You can now try opening the file in whatever application is meant to be used to view it, but don't be surprised if very strange things happen. Finally, you'll want to resume the transfer, unless you've determined that you no longer want the file.<br />
                  <br />
                  How do I change the command line parameters in Windows?<br />
                  <br />
                  The command line that Windows sends to the BitTorrent program is stored in the registry, with the file-type association for TORRENT files. It can be changed as follows:<br />
                  <br />
                  1. Go to Folder Options in the Control Panel.<br />
                  2. Select the File Types tab at the top of the dialog.<br />
                  3. Wait a moment for Windows to load the list, then scroll down and select the TORRENT extension. Tip: press the 't' key to jump to entries starting with t. Note: The file extension column might be truncated to just TORR...<br />
                  4. Click the Advanced button.<br />
                  5. In the list of Actions, select open, probably at the top of the list, then click the Edit button.<br />
                  6. The contents of Application used to perform action is the command line. By default it should be:<br />
                  <br />
"C:\Program Files\BitTorrent\btdownloadgui.exe" --responsefile "%1"<br />
                  <br />
                  Note the double-quotes around %1, and your Program Files directory might be different. To add more command line parameters, simply change this string. For example, to change the port range, you might use:<br />
                  <br />
"C:\Program Files\BitTorrent\btdownloadgui.exe" --minport 10000 --maxport 10100 --responsefile "%1"<br />
                  <br />
                  See this section of the FAQ for a list of all the available command line arguments and their defaults.<br />
                  7. Finish by clicking OK to close all the dialogs.<br />
                  <br />
                  Also note that if you are using TorrentSpy to launch BitTorrent, the command line used is contained in the TorrentSpy configuration. Use the System tab in TorrentSpy to modify the command line parameters used in this case.<br />
                  How can I make Internet Explorer ask me if I want to save the torrent link rather than automatically opening the download GUI?<br />
                  <br />
                  Some people like to save the .TORRENT files to disk, so that it's easy to restart transfers without finding the original link. It's possible to make Internet Explorer ask you if you want to save the file or open it in place, rather than automatically running the GUI. To do this, follow steps 1 through 4 under the section above about changing the command line parameters. On the dialog now on your screen you should see a check box labeled Confirm open after download. Put a check in this box and IE will ask you what you want to do when you click on a torrent link. Use this as an alternative if you find yourself always right clicking and choosing Save As....<br />
                  What are the command line parameters for BitTorrent?<br />
                  <br />
                  I have moved this section to a seperate page as this one is getting quite large. Click Here for the list of command line parameters.</p>
              
              </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
