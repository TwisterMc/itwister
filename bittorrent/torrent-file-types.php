<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Torrent File Types</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Torrent File Types</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
		   <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>I just downloaded a file ending in .xyz, how do I open it?<br />
                  <br />
                  Below is a list of common file types you will encounter with BitTorrent, and how to handle them.<br />
                  .R00, .R01, .Rnn	If you find a directory with a bunch of files ending in .Rnn, it's a RAR archive split into multiple parts. This is commonly done for posting to Usenet newsgroups. Open the .RAR file and extract the contents with WinRAR (Windows) or UnRarX (OS X.) Either program should automatically see all the parts if they are in the same directory.<br />
                  .CBR, .CBZ	These are comics in a compressed archive. For Windows, download the free program CDisplay. Or simply rename them (CBR to RAR, CBZ to ZIP) and open with your usual archive program, such as WinRAR or WinZIP. For OS X, try Book Image Viewer after extracting with unrar or unzip.<br />
                  .PAR, .P01, .Pnn	These are parity files, used to reconstruct any missing parts of the archive. Ordinarily you will not have to do anything with them -- they are extraneous unless a part is missing or bad, in which case the torrent's creator should have fixed the archive before distributing the torrent. If WinRAR does give you a message about a missing or corrupt part, then get SmartPAR (Windows) and open the .PAR file. The program will then check all the files and recreate any missing or damaged parts. For OS X, UnRarX should also process the PAR file.<br />
                  .NFO	Files that end in .NFO are plain text files that often contain very useful information about the files you have just downloaded. Always read the NFO file if you are having a problem! Unfortunately, the .NFO extention also has another meaning to Windows, so sometimes when you try to open these files you will get an error from MS System Information about a corrupt file. If this is the case you will also probably see the file listed with a type of "MSInfo File" or something similar. You should open the NFO file in Notepad, or any plain-text editor. More info here.<br />
                  .SFV	Simple File Verification file - used to verify the integrity of a set of files, this is a text file containing file names and typically CRC32 checksums. For Windows, try a program such as QuickSFV or fsum to verify the integrity. Mac OS X users should try MacSFV. Normally these files should not be necessary with BitTorrent, since the BT protocol has its own error checking method (on top of TCP's checksumming.) If you find some file that doesn't match the checksum in its SFV file, blame the torrent's creator, since he or she should have fixed it before creating and distributing the torrent.<br />
                  .BIN, .CUE, .ISO	These are images of a CD. If the file is a movie, they are most likely VCDs or SVCDs. There are several ways to deal with these. For Windows:<br />
                  <br />
                  * Use a program such as Nero to burn the images to a CDR and then view them in your standalone DVD player, or your DVD drive with your DVD player software. Some instructions for burning BIN files with common software applications can be found at this link.<br />
                  * If you know it's a VCD/SVCD, you can use a tool such as VCDGear to extract the MPEG data. VCDs will be MPEG-1 type files, and SVCDs will be MPEG-2 type files. Usually the easiest way to view these is with a DVD player such as PowerDVD which can read input from a file.<br />
                  * Use a program such as Daemon Tools or Alcohol 120% to 'mount' the file as a virtual disk. Then you can use PowerDVD or whatever application is appropriate to view the data from that drive.<br />
                  <br />
                  For OS X:<br />
                  <br />
                  * Use FireStarter FX to burn (S)VCDs from CUE or BIN files.<br />
                  * Play (S)VCD files, and most others, with VLC.</p>
              </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
