<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Uninstall Torrent : Torrent Information : BitTorrent</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Uninstall Torrent</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
            <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
              </td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>How do I uninstall BitTorrent?<br />
                  MS Windows<br />
                  <br />
                  Go to Add/Remove Programs in the Control Panel. There should exist an entry for BitTorrent. If it's not there, suspect an incorrect install. You can always reinstall the latest version and then uninstall it.<br />
                  <br />
                  If you know what you are doing, you can manually remove BitTorrent by deleting the directory C:\Program Files\BitTorrent\ (substituting the actual location of your Program Files dir) and removing the following registry keys:<br />
                  <br />
                  * HKEY_CLASSES_ROOT\.torrent<br />
                  * HKEY_CLASSES_ROOT\MIME\Database\Content Type\application/x-bittorrent<br />
                  * HKEY_CLASSES_ROOT\bittorrent<br />
                  * HKEY_LOCAL_MACHINE\Software\Microsoft\Windows<br>\CurrentVersion
                  \Uninstall\BitTorrent<br />
                  <br />
                  Mac OS X<br />
                  <br />
                  Simply drag the application to the trash. If you also wish to delete the preferences, trash the file ~/Library/Preferences/BitTorrent.plist as well.</p>
                </span> </td>
            </tr>
            <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
              </td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
