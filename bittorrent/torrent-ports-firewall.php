<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Torrent Ports & Firewall : Torrent Information : BitTorrent</title>
<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
<link href="styles.css" type="text/css" rel="stylesheet">
</head>
<body background="images/bg_image_blue.png" bgcolor="#0b2b64">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td bgcolor="white" background="images/top_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_top.png" alt="" width="110" height="71" border="0"></td>
            <td valign="middle">
              <table border="0" cellspacing="3" cellpadding="0">
                <tr height="26">
                  <td height="26"></td>
                </tr>
                <tr>
                  <td><span class="TitleStyle">Torrent Ports and Firewall</span></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
  <tr height="32">
    <td height="32" background="images/toolbar_bg.png">
      <div align="left">
        <table border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="images/logo_bottom.png" alt="" width="110" height="31" border="0"><img src="images/toolbar_cap.png" alt="" width="2" height="31" border="0"></td>
            <td>&nbsp;</td>
          </tr>
        </table>
      </div>
    </td>
  </tr>
</table>
<div align="center"> <br>
  <table width="64" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><img src="images/content_top.png" alt="" width="602" height="14" border="0"></td>
    </tr>
    <tr>
      <td background="images/content_middle.png">
        <div align="center">
          <table width="570" border="0" cellspacing="0" cellpadding="2">
		   <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
            <tr>
              <td valign="top" width="150"> <span class="contentstyle1">
                <p>
                  <?php include("menu.php"); ?>
                </p>
                </span></td>
              <td valign="top"><span class="contentstyle1">
                <p>What ports does BitTorrent use? Will it work with a firewall/NAT?<br />
                  <br />
                  The quick summary: You need to forward your ports if you have NAT in order to get the fastest speeds. This is probably the most common thing that people fail to do when using BitTorrent. Read on for more details of what all this entails, and if it's something that you need to do.<br />
                  <br />
                  Prior to version 3.2, BitTorrent by default uses ports in the range of 6881-6889. As of 3.2 and later, the range has been extended to 6881-6999. (These are all TCP ports, BitTorrent does not use UDP.) The client starts with the lowest port in the range and sequentially tries higher ports until it can find one to which it can bind. This means that the first client you open will bind to 6881, the next to 6882, etc. Therefore, you only really need to open as many ports as simultaneous BitTorrent clients you would ever have open. For most people it's sufficient to open 6881-6889.<br />
                  <br />
                  The port range that BitTorrent uses is configurable, see the section on command line parameters, specifically the --minport and --maxport parameters.<br />
                  <br />
                  The trackers to which BitTorrent must connect usually are on port 6969, so the client must have outbound access on this port. Some trackers are on other ports, however.<br />
                  <br />
                  BitTorrent will usually work fine in a NAT (network address translation) environment, since it can function with only outbound connections. Such environments generally include all situations where multiple computers share one publicly-visible IP address, most commonly: computers on a home network sharing a cable or xDSL connection. If you are unsure of whether you have NAT or not, then try this link which will try to determine if you are behind a NAT gateway.<br />
                  <br />
                  However, you will get better speeds if you can accept incoming connections as well. To do this you must use the "port forwarding" feature of whatever is performing the NAT/gateway task. For example, if you have a cable or DSL connection and a router/switch/gateway/firewall, you will need to go into the configuration of this device and forward ports 6881-6889 to the local machine that will be using BitTorrent. If your device makes it hard to enter a range of ports (if you must enter each one separately), then you can just do the first 10 or so ports, or however many simultaneous clients you plan to ever have open. If more than one person behind such a gateway wishes to use BitTorrent, then each machine should use a different port range, and the gateway should be configured to forward each port range to the corresponding local machine.<br />
                  <br />
                  If you have one of these broadband router/NAT devices (such as the Linksys BEFSR41, D-Link DI-701/704, Netgear RT311, SMC Barricade, 3Com Home Ethernet Gateway, etc.) you will usually need to enter the web configuration of the device. If you're not sure, try http://192.168.1.1 or sometimes http://192.168.0.1. If you can't figure it out, try the manual for the device -- they are often on the manufacturer's web site in PDF form. You can also try the forums at places like Broadband Reports or Practically Networked. To see an example of what you're looking for, this is a link to the Linksys BEFSR41 manual. Look at page 55, under the section "Port Range Forwarding."<br />
                  <br />
                  If you are using Microsoft's ICS (Internet Connection Sharing), this article on mapping ports might be useful.<br />
                  <br />
                  If you are using a software firewall, then you must also enable incoming connections to be answered by the BitTorrent client program. Note that Windows XP includes a primitive firewall ("Internet Connection Firewall" or ICF) which you may have to configure for BitTorrent. Here are the directions for opening ports in the Windows XP firewall:<br />
                  <br />
                  1. Open the 'Network Connections' folder (click Start, then Control Panel, then Network and Internet Connections, then Network Connections.)<br />
                  2. Click the shared connection or the Internet connection that is protected by Internet Connection Firewall, and then, under Tasks, click Change settings of this connection.<br />
                  3. On the Advanced tab, click Settings.<br />
                  4. For each port you wish to forward, (i.e. 6881, 6882, ... 6889) do the following:<br />
                  <br />
                  1. On the Services tab, click Add and enter all of the following information:<br />
                  2. In Description of service, type an easily recognized name for the service, such as "BitTorrent".<br />
                  3. In Name or IP address of the computer hosting this service on your network, enter 127.0.0.1 (this means "the local machine.")<br />
                  4. In both External and Internal port number for this service, enter the port number, e.g. 6881.<br />
                  5. Select TCP, then OK.<br />
                  <br />
                  See this link or this link for more information about the XP firewall.<br />
                  <br />
                  If you are running another type of software firewall (such as Zone Alarm Pro, Norton Firewall, McAfee Firewall, BlackICE Defender, etc.), you may have to do something similar to allow inbound access on ports 688x to the BitTorrent client (usually btdownloadgui.exe.)<br />
                  <br />
                  For example, in Zone Alarm Pro, in the Program Listings, click on the program's name (btdownloadgui.exe) and then click the Options button and then enter the ports to use. If you're having trouble connecting, you might try giving BitTorrent access to all ports.<br />
                  <br />
                  To open ports in the Mac OS X firewall, do the following:<br />
                  <br />
                  1. Open System Preferences.<br />
                  2. Click Sharing.<br />
                  3. Select the Firewall tab.<br />
                  4. Click the New... button.<br />
                  5. Click the popup menu in the dialog that appears, and choose Other....<br />
                  6. In the Port Number, Range, or Series field, enter 6881-6999.<br />
                  7. In the Name field, enter BitTorrent (or any other identifying string.)<br />
                  8. Click OK. </p>
                
              </span> </td>
            </tr>
			 <tr>
              <td colspan="2" valign="top" align="center"><script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 468;
google_ad_height = 60;
google_ad_format = "468x60_as";
google_ad_channel ="5676545456";
google_color_border = "0b2b64";
google_color_bg = "0b2b64";
google_color_link = "0b2b64";
google_color_url = "0b2b64";
google_color_text = "FFFFFF";
//--></script>
                <script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></td>
            </tr>
          </table>
        </div>
      </td>
    </tr>
    <tr>
      <td><img src="images/content_bottom.png" alt="" width="602" height="14" border="0"></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <table width="600" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="19"><img src="images/footer_left.png" alt="" width="19" height="38" border="0"></td>
        <td background="images/footer_bg.png">
          <table width="561" border="0" cellspacing="0" cellpadding="3">
            <tr>
              <td> <span class="FooterStyle">
                
                </span> </td>
            </tr>
            <tr height="2">
              <td height="2"></td>
            </tr>
          </table>
        </td>
        <td width="19"><img src="images/footer_right.png" alt="" width="19" height="38" border="0"></td>
      </tr>
    </table>
  </div>
</div>
</body>
</html>
