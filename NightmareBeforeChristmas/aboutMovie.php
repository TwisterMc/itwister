<html>
<head>
<title>About The Nightmare Before Christmas</title>
<meta name="keywords" content="nightmare before christmas tim burton">
<meta name="description" content="Nightmare Before Christmas Goods.">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="style.css" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#000000">
<table width="750" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" align="center">
  <tr><td>
<table border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
  <td>
    <? include("menu.php"); ?>
  </td>
  </tr>
</table>
<blockquote>
<p><i>The following is Tim Burton&#8217;s foreword of the book &quot;<a href="http://www.amazon.co.uk/exec/obidos/ASIN/0786853786/qid=1050837965/sr=2-1/ref=sr_2_3_1/026-6043453-4408430" target="_blank">The Nightmare Before Christmas: the Film, the Art, the Vision</a>&quot;. All copyright goes to Tim Burton.</i> </p>
<p> &quot;Nightmare Before Christmas is a movie I&#8217;ve wanted to make for over a decade, since I worked as an animator at Walt Disney Studios in the early eighties. It started as a poem I wrote, influenced by the style of my favorite children&#8217;s author, Dr. Seuss. I made several drawings of the characters and the settings and began planning it as a film. </p>
<p> I thought at first that Nightmare Before Christmas would make a good holiday special for television, although I also considered other forms, including a children&#8217;s book. At the time, I think, it was too weird for Disney. I moved on to other things, but I never forgot it. </p>
<p> Although the title makes the film sound a little scary, I see Nightmare Before Christmas as a positive story, without any truly bad characters. The characters are trying to do something good and just get mixed up. </p>
<p> Like a lot of people, I grew up loving the animated specials like Rudolph the Red-Nosed Reindeer and How the Grinch Stole Christmas that appeared on TV every year. I wanted to create something with the same kind of feeling and warmth. </p>
<p> Nightmare is the story of Jack Skellington, the Pumpkin King of HalloweenTown, who discovers Christmas and immediately wants to celebrate this strange holiday himself. I love Jack. He has a lot of passion and energy; he&#8217;s always looking for a feeling. That&#8217;s what he finds in Christmas Town. He is a bit misguided and his emotions take over, but he gets everybody excited. The setting may be odd and a little unsettling, but there are no real villains in the film. It&#8217;s a celebration of Halloween and Christmas--my two favorite holidays. </p>
<p> I decided early on that I wanted to tell this story through stopmotion animation. I have always loved this medium, but it is challenging. One danger is that the audience may become overwhelmed by the technique and get distracted from the emotion. </p>
<p> Luckily, I was able to entrust Nightmare Before Christmas to my friend Henry Selick, the most brilliant stop-motion director around. We have a similar sensibility, and he was able to take my original drawings and bring them to life. He gathered together an amazing crew in San Francisco � a wonderful group of artists, all working toward the same vision. Everybody put their heart into it � Henry, the composer Danny Elfman, the screenwriter Caroline Thompson, the animators, everybody down the line � making it an incredibly challenging, and rewarding experience. </p>
<p> Nightmare Before Christmas is deeper in my heart than any other film. It is more beautiful than I imagined it would be. Thanks to Henry and his talented crew of artists, animators, and designers. As I watch it, I know I will never have this feeling again. Nightmare Before Christmas is special. It is a film that I have always known I had to make. More important, it is a film I have always wanted to see. Now I can. It has been worth the wait. I think there are few projects like that in your life.&quot; </p>
<p align="right"> - Tim Burton </p>
</blockquote>
<script type="text/javascript"><!--
google_ad_client = "pub-8716721475579600";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_channel ="2906846293";
google_ad_type = "text";
google_color_border = "FFFFFF";
google_color_bg = "FFEBCD";
google_color_link = "DE7008";
google_color_url = "E0AD12";
google_color_text = "8B4513";
//--></script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br>
</td></tr><tr>
<td background="images/fence.gif"><img src="images/fence.gif" alt="Nightmare Before Christmas" width="90" height="50"></td>
</tr></table>
</body>
</html>

