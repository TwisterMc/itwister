CBZ Auto Create Posts
===================

A WordPress plugin that auto generates loriem ipsum posts in cat, bacon or zombie.

To Install:
1. Download the zip file and upload the whole folder to your plugins directory.
2. Activate.
3. CBZ Auto Create Posts will then show up under the Tools menu in WordPress.

This is a work in progress and has been working fine for me.

If you want to help make it better, let me know.

If you like to draw cute zombie cats eating bacon let me know that too.

Thanks
Thomas
@TwisterMc